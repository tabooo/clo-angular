export interface OrganizationAddDto {
    organizationCode: string;
    organizationName: string;
    organizationNameEn: string;
    address: string;
    addressFact: string;
}
