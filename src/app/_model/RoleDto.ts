export interface RoleDto {
    id: number;
    name: string;
    displayName: string;
    description: string;
}
