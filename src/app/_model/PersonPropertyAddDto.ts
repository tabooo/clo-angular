export interface PersonPropertyAddDto {
    personId: string;
    propertyName: string;
    comment: string;
}
