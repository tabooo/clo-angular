import {LibDto} from './LibDto';
import {PersonDto} from './PersonDto';

export interface PersonConnectionDto {
    personDto: PersonDto;
    personConnectionType: LibDto;
}
