export interface OrganizationDto {
    organizationId: number;
    organizationCode: string;
    organizationName: string;
    organizationNameEn: string;
    address: string;
    addressFact: string;
    isLoaner: boolean;
}
