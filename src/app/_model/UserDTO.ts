import {RoleDto} from './RoleDto';

export interface UserDTO {
    userId: number;
    userName: string;
    firstName: string;
    lastName: string;
    personalNo: string;
    birthDate: Date;
    address: string;
    position: string;
    mobileNo: string;
    email: string;
    accountNumber: string;
    startDate: Date;
    authorities: string[];
    roles: RoleDto[];
    bonus: number;
    online: number;
}
