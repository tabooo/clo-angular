export interface DebtComplexityTypeDto {
    id: number;
    name: string;
    nameEn: string;
    percent: number;
}
