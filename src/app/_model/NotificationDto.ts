export interface NotificationDto {
    userBonusAmount: number;
    unconfirmedPaidDebtCount: number;
}
