import {LibDto} from './LibDto';
import {UserDTO} from './UserDTO';
import {File} from './File';

export interface DebtBillingDto {
    id: number;
    debtId: number;
    billingType: LibDto;
    amount: number;
    paymentDate: Date;
    file: File;
    insertDate: Date;
    userDto: UserDTO;
    comment: string;
    payByCheck: number;
}
