export interface DebtCommentDto {
    id: number;
    debtId: number;
    commentDate: Date;
    comment: string;
    promiseDate: Date;
    nextContactDate: Date;
}
