export interface PrivilegeDto {
    id: number;
    name: string;
    displayName: string;
    description: string;
}
