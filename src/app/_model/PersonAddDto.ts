export interface PersonAddDto {
    firstName: string;
    lastName: string;
    personalNo: string;
    birthDate: Date;
    midName: string;
    address: string;
    addressFact: string;
}
