import {PersonDto} from './PersonDto';
import {OrganizationDto} from './OrganizationDto';
import {LibDto} from './LibDto';

export interface DebtBorrower {
    personType: LibDto;
    person: PersonDto;
    organization: OrganizationDto;
}

export interface DebtLoaner {
    personType: LibDto;
    person: PersonDto;
    organization: OrganizationDto;
}

export interface DebtUserDto {
    id: number;
    name: string;
}

class DebtToConfirmPaidDto {
    id: number;
    userId: number;
    confirmedUserId: number;
    insertDate: Date;
    confirmedDate: Date;
    confirmed: number;
}

class AssigneeDebtRequestResponseDto {
    id: number;
    debtId: number;
    assigneeRequestDate: Date;
    comment: string;
    requestUserId: DebtUserDto;
    status: LibDto;
    assigneeTypeId: number;
}

export interface DebtDto {
    id: number;
    debtDate: Date;
    amount: number;
    borrower: DebtBorrower;
    loaner: DebtLoaner;
    debtComplexityType: LibDto;
    expert: DebtUserDto;
    incomingDate: Date;
    percent: number;
    expertAssigneeDate: Date;
    lastCommentDate: Date;
    lastPromiseDate: Date;
    lastNextContactDate: Date;
    courtFeeAmount: number;
    fineAmount: number;
    fullyPaid: boolean;
    refusesToPay: boolean;
    notInContact: boolean;
    processed: boolean;
    cannSeeExpert: boolean;
    debtToConfirmPaidDto: DebtToConfirmPaidDto[];
    assigneeDebtRequestDto: AssigneeDebtRequestResponseDto[];
    debtTotalAmount: number;
    paymentAmount: number;
    leftToPayAmount: number;
    state: number;
    statuteOfLimitationDate: Date;
    lawyerStatus: LibDto;
}
