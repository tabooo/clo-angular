export interface PersonContactAddDto {
    personId: string;
    contactInfo: string;
    comment: string;
}
