export interface File {
    id: number;
    name: string;
    fileTypeId: number;
    path: string;
    mimeType: number;
    ext: number;
    insertDate: Date;
    state: number;
}
