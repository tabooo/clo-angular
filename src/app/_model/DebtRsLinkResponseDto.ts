export interface DebtRsLinkResponseDto {
    id: number;
    debtId: number;
    link: string;
    comment: string;
}
