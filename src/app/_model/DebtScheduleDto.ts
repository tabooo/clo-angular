import {LibDto} from './LibDto';
import {UserDTO} from './UserDTO';

export interface DebtSchedulePaymentDto {
    id: number;
    scheduleId: number;
    requiredAmount: number;
    paidAmount: number;
    payDate: Date;
    fullyPaid: number;
}

export interface DebtScheduleDto {
    id: number;
    debtId: number;
    scheduleType: LibDto;
    month: number;
    paymentStartDate: Date;
    fullyPaid: number;
    delegatePercent: number;
    comment: string;
    debtSchedulePayments: DebtSchedulePaymentDto[];
    debtScheduleStatus: LibDto;
    statusComment: string;
    statusUpdateUser: UserDTO;
    statusUpdateDate: Date;
}
