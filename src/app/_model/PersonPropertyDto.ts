import {LibDto} from './LibDto';

export interface PersonPropertyDto {
    id: number;
    personId: number;
    propertyName: string;
    comment: string;
    propertyType: LibDto;
}
