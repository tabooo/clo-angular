import {LibDto} from './LibDto';

export interface PersonContactDto {
    id: number;
    personId: number;
    contactInfo: string;
    comment: string;
    contactStatus: LibDto;
}
