export interface PersonBankDetailDto {
    id: number;
    personId: number;
    bankDetail: string;
    comment: string;
}
