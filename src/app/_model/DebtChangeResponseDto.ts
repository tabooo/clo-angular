import {DebtBorrower, DebtLoaner, DebtUserDto} from './DebtDto';
import {DebtComplexityTypeDto} from './DebtComplexityTypeDto';

export interface DebtChangeResponseDto {
    id: number;
    borrower: DebtBorrower;
    loaner: DebtLoaner;
    debtDate: Date;
    incomingDate: Date;
    debtComplexityType: DebtComplexityTypeDto;
    percent: number;
    amount: number;
    comment: string;
    user: DebtUserDto;
}
