import {OrganizationAddDto} from './OrganizationAddDto';
import {PersonAddDto} from './PersonAddDto';

export interface PersonCreateDto {
    personTypeId: number;
    person: PersonAddDto;
    organization: OrganizationAddDto;
}
