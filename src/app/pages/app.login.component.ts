import {Component} from '@angular/core';
import {AuthenticationService} from '../_services/authentication.service';

@Component({
    selector: 'app-login',
    templateUrl: './app.login.component.html',
})
export class AppLoginComponent {
    username;
    password;

    constructor(private authenticationService: AuthenticationService) {
    }

    login() {
        this.authenticationService.login(this.username, this.password);
    }
}
