export const DebtStates = [
    {
        id: 1,
        name: 'აქტიური'
    },
    {
        id: 2,
        name: 'წაშლილი (შეცდომით გამოგზავნილი)'
    },
    {
        id: 3,
        name: 'წაშლილი (უშედეგო)'
    }
];
