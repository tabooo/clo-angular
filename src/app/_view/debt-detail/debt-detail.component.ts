import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {DebtBorrower, DebtDto} from '../../_model/DebtDto';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {GlobalService} from '../../_services/global.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ApiService} from '../../_services/api.service';
import {DebtCommentsComponent} from '../_forms/debt-comments/debt-comments.component';
import {DebtScheduleComponent} from '../_forms/debt-schedule/debt-schedule.component';
import {DebtBillingsComponent} from '../_forms/debt-billings/debt-billings.component';
import {UserDTO} from '../../_model/UserDTO';
import {decode} from '../../_helper/json-util';
import {AuthenticationService} from '../../_services/authentication.service';
import {hasRight, hasRole} from '../../_helper/user-util';
import {DateFormatPipe} from '../../_helper/dates/pipe/date-format.pipe';
import {DebtSchedulesComponent} from '../_forms/debt-schedules/debt-schedules.component';
import {setDefaultTimeToDate} from '../../_helper/dates/date-util';
import {DebtChangesComponent} from '../_forms/debt-changes/debt-changes.component';
import {DebtRsLinksComponent} from '../_forms/debt-rs-links/debt-rs-links.component';
import {ConfirmationService} from 'primeng/api';
import {DebtFilesComponent} from '../_forms/debt-files/debt-files.component';
import {DebtLawyerComponent} from '../_forms/debt-lawyer/debt-lawyer.component';

@Component({
    selector: 'app-debt-detail',
    templateUrl: './debt-detail.component.html',
    styleUrls: ['./debt-detail.component.scss'],
})
export class DebtDetailComponent implements OnInit {
    @Input() debtId;
    debt: DebtDto;
    debtForm: FormGroup;
    complexityTypes;
    user: UserDTO;

    libs = 2;

    @ViewChild('commentsComponent', {static: false}) commentsComponent: DebtCommentsComponent;
    debtCommentsLoaded = false;

    @ViewChild('scheduleComponent', {static: false}) scheduleComponent: DebtScheduleComponent;
    debtScheduleLoaded = false;

    @ViewChild('debtFilesComponent', {static: false}) debtFilesComponent: DebtFilesComponent;
    debtFilesLoaded = false;

    @ViewChild('debtBillingsComponent', {static: false}) debtBillingsComponent: DebtBillingsComponent;
    debtBillingsLoaded = false;

    @ViewChild('debtSchedulesComponent', {static: false}) debtSchedulesComponent: DebtSchedulesComponent;
    debtSchedulesLoaded = false;

    @ViewChild('debtChangesComponent', {static: false}) debtChangesComponent: DebtChangesComponent;
    debtChangesLoaded = false;

    @ViewChild('debtRsLinksComponent', {static: false}) debtRsLinksComponent: DebtRsLinksComponent;
    debtRsLinksLoaded = false;

    @ViewChild('debtLawyerComponent', {static: false}) debtLawyerComponent: DebtLawyerComponent;
    debtLawyerLoaded = false;

    displayDebtChangeModal = false;
    updateDebtComment = '';
    modalBlocked = false;

    enableDiscussDebtToConfirmFullyPaid = false;
    enableDiscussDebtToConfirmResult = false;

    enableRequestAssigneeDebtToAdministration = false;
    enableMakeAssigneeAdministrationDecisionButton = false;
    enableRequestAssigneeDebtToLawyer = false;
    enableMakeAssigneeLawyerDecisionButton = false;
    requestAssigneeDebtToAdministrationDebtComment = '';
    requestAssigneeDebtToLawyerDebtComment = '';
    displayRequestAssigneeDebtToAdministrationModal = false;
    displayRequestAssigneeDebtToLawyerModal = false;

    refusesToPay = false;
    notInContact = false;
    isProcessed = false;
    cannSeeExpert = false;

    displaySearchUserAssigneeLawyer = false;

    constructor(private globalService: GlobalService,
                private activatedRoute: ActivatedRoute,
                private apiService: ApiService,
                private fb: FormBuilder,
                private router: Router,
                private authenticationService: AuthenticationService,
                private dateFormatPipe: DateFormatPipe,
                private confirmationService: ConfirmationService) {
        this.user = decode(localStorage.getItem(this.authenticationService.LOCAL_STORAGE_KEY));
    }

    ngOnInit(): void {
        this.debtForm = this.fb.group({
            debtDate: new FormControl(),
            amount: new FormControl(),
            courtFeeAmount: new FormControl(),
            fineAmount: new FormControl(),
            borrowerPersonId: new FormControl(),
            borrowerPerson: new FormControl(),
            loanPersonId: new FormControl(),
            loanPerson: new FormControl(),
            user: new FormControl(),
            userId: new FormControl(),
            lawyer: new FormControl(),
            complexityType: new FormControl(),
            percent: new FormControl(),
            incomingDate: new FormControl(),
            statuteOfLimitationDate: new FormControl(),
            expertAssigneeDate: new FormControl(),
            lastCommentDate: new FormControl(),
            lastPromiseDate: new FormControl(),
            lastNextContactDate: new FormControl(),
            refusesToPay: new FormControl(),
            notInContact: new FormControl(),
            isProcessed: new FormControl(),
            cannSeeExpert: new FormControl(),
        });

        if (!this.debtId) {
            this.activatedRoute.params.subscribe(params => {
                this.debtId = params.debtId;
                this.loadLib();
            });
        } else {
            this.loadLib();
        }

        this.apiService.getDebtComplexityTypes().subscribe(response => {
            this.complexityTypes = response;
            this.loadLib();
        });
    }

    loadLib() {
        this.libs -= 1;
        if (this.libs === 0) {
            this.getDebt();
        }
    }

    getDebt() {
        this.globalService.onBlockUI(true);
        this.apiService.getDebt(this.debtId).subscribe(response => {
            this.debt = response;
            this.debtForm.patchValue({
                debtDate: response.debtDate ? new Date(response.debtDate) : null,
                amount: response.amount,
                courtFeeAmount: response.courtFeeAmount,
                fineAmount: response.fineAmount,
                refusesToPay: response.refusesToPay,
                notInContact: response.notInContact,
                isProcessed: response.processed,
                cannSeeExpert: response.cannSeeExpert,
                borrowerPersonId: response.borrower ?
                    response.borrower.personType.id === 1 ?
                        response.borrower.person.personId : response.borrower.organization.organizationId
                    : null,
                borrowerPerson: response.borrower ?
                    response.borrower.personType.id === 1 ?
                        response.borrower.person.firstName + ' ' + response.borrower.person.lastName +
                        ' (' + response.borrower.person.personalNo + ')'
                        : response.borrower.organization.organizationName + ' (' + response.borrower.organization.organizationCode + ')'
                    : null,
                loanPersonId: response.loaner ?
                    response.loaner.personType.id === 1 ?
                        response.loaner.person.personId : response.loaner.organization.organizationId
                    : null,
                loanPerson: response.loaner ?
                    response.loaner.personType.id === 1 ?
                        response.loaner.person.firstName + ' ' + response.loaner.person.lastName +
                        ' (' + response.loaner.person.personalNo + ')'
                        : response.loaner.organization.organizationName + ' (' + response.loaner.organization.organizationCode + ')'
                    : null,
                user: response.expert?.fullName,
                userId: response.expert?.id,
                lawyer: response.lawyer?.fullName,
                complexityType: response.debtComplexityType,
                percent: response.percent,
                incomingDate: response.incomingDate ? new Date(response.incomingDate) : null,
                statuteOfLimitationDate: response.statuteOfLimitationDate ? new Date(response.statuteOfLimitationDate) : null,
                expertAssigneeDate: response.expertAssigneeDate ? new Date(response.expertAssigneeDate) : null,
                lastCommentDate: response.lastCommentDate ? new Date(response.lastCommentDate) : null,
                lastPromiseDate: response.lastPromiseDate ? new Date(response.lastPromiseDate) : null,
                lastNextContactDate: response.lastNextContactDate ? new Date(response.lastNextContactDate) : null,
            });

            if (!this.debt.fullyPaid) {
                if (this.debt.expert && this.debt.expert.id === this.user.userId) {
                    if ((this.debt.debtToConfirmPaidDto && this.debt.debtToConfirmPaidDto.length > 0
                            && this.debt.debtToConfirmPaidDto[0].confirmed === 2)
                        || !this.debt.debtToConfirmPaidDto || this.debt.debtToConfirmPaidDto.length < 1) {
                        this.enableDiscussDebtToConfirmFullyPaid = true;
                    } else {
                        this.enableDiscussDebtToConfirmFullyPaid = false;
                    }
                } else {
                    this.enableDiscussDebtToConfirmFullyPaid = false;
                }

                if (this.hasRight('ROLE_ვალის განულების დადასტურება')
                    && this.debt.debtToConfirmPaidDto && this.debt.debtToConfirmPaidDto.length > 0
                    && this.debt.debtToConfirmPaidDto[0].confirmed === 0) {
                    this.enableDiscussDebtToConfirmResult = true;
                } else {
                    this.enableDiscussDebtToConfirmResult = false;
                }
            }

            if (this.debt.expert && this.debt.expert.id === this.user.userId) {
                if ((this.debt.assigneeDebtRequestDto && this.debt.assigneeDebtRequestDto.length > 0
                        && this.debt.assigneeDebtRequestDto[0].status.id !== 1)
                    || !this.debt.assigneeDebtRequestDto || this.debt.assigneeDebtRequestDto.length < 1
                ) {
                    if (this.hasRight('ROLE_ვალის ადმინისტრაციაზე გადაწერა')) {
                        this.enableRequestAssigneeDebtToAdministration = true;
                    }
                    if (this.hasRight('ROLE_იურისტზე გადაწერის მოთხოვნა')) {
                        this.enableRequestAssigneeDebtToAdministration = true;
                    }
                } else {
                    this.enableRequestAssigneeDebtToAdministration = false;
                    this.enableRequestAssigneeDebtToAdministration = false;
                }
            } else {
                this.enableRequestAssigneeDebtToAdministration = false;
                this.enableRequestAssigneeDebtToAdministration = false;
            }

            if (this.debt.assigneeDebtRequestDto && this.debt.assigneeDebtRequestDto.length > 0
                && this.debt.assigneeDebtRequestDto[0].status.id === 1
                && this.hasRight('ROLE_ვალის ადმინისტრაციაზე გადაწერის დადასტურება')) {
                if (this.debt.assigneeDebtRequestDto[0].assigneeTypeId === 1) {
                    this.enableMakeAssigneeAdministrationDecisionButton = true;
                } else if (this.debt.assigneeDebtRequestDto[0].assigneeTypeId === 2) {
                    this.enableMakeAssigneeLawyerDecisionButton = true;
                }

            } else {
                this.enableMakeAssigneeAdministrationDecisionButton = false;
                this.enableMakeAssigneeLawyerDecisionButton = false;
            }

            this.globalService.onBlockUI(false);
        });
    }

    onTabChange(event) {
        if (event.index === 1) {
            if (!this.debtCommentsLoaded) {
                this.commentsComponent.setDebt(this.debt);
                this.commentsComponent.getDebtComments();
                this.debtCommentsLoaded = true;
            }
        } else if (event.index === 2) {
            if (!this.debtFilesLoaded) {
                this.debtFilesComponent.setDebtId(this.debtId);
                this.debtFilesComponent.getDebtFiles();
                this.debtFilesLoaded = true;
            }
        } else if (event.index === 3) {
            if (!this.debtScheduleLoaded) {
                this.scheduleComponent.setDebt(this.debt);
                // this.scheduleComponent.getDebtSchedule();
                this.debtScheduleLoaded = true;
            }
        } else if (event.index === 4) {
            if (!this.debtBillingsLoaded) {
                this.debtBillingsComponent.setDebtId(this.debt.id);
                this.debtBillingsComponent.getDebtBillings();
                this.debtBillingsLoaded = true;
            }
        } else if (event.index === 5) {
            if (!this.debtSchedulesLoaded) {
                this.debtSchedulesComponent.setDebtId(this.debt.id);
                this.debtSchedulesComponent.getDebtSchedules();
                this.debtSchedulesLoaded = true;
            }
        } else if (event.index === 6) {
            if (!this.debtChangesLoaded) {
                this.debtChangesComponent.setDebtId(this.debt.id);
                this.debtChangesComponent.getDebtChanges();
                this.debtChangesLoaded = true;
            }
        } else if (event.index === 7) {
            if (!this.debtRsLinksLoaded) {
                this.debtRsLinksComponent.setDebtId(this.debt.id);
                this.debtRsLinksComponent.getDebtRsLinks();
                this.debtRsLinksLoaded = true;
            }
        } else if (event.index === 8) {
            if (!this.debtLawyerLoaded) {
                this.debtLawyerComponent.setDebt(this.debt);
                this.debtLawyerLoaded = true;
            }
        }
    }

    openPerson(borrower: DebtBorrower) {
        if (borrower) {
            if (borrower.personType.id === 1) {
                this.router.navigate(['/main/person/' + borrower.person.personId]);
            } else {
                this.router.navigate(['/main/organization/' + borrower.organization.organizationId]);
            }
        }
    }

    updateDebtDebt() {
        this.modalBlocked = true;
        const formValues = this.debtForm.getRawValue();
        const request = {
            debtId: this.debtId,
            borrowerPersonId: formValues.borrowerPersonId,
            loanPersonId: formValues.loanPersonId,
            debtDate: formValues.debtDate ? setDefaultTimeToDate(formValues.debtDate) : null,
            incomingDate: formValues.incomingDate ? setDefaultTimeToDate(formValues.incomingDate) : null,
            debtComplexityTypeId: formValues.complexityType?.id,
            percent: formValues.percent ? formValues.percent : 0,
            comment: this.updateDebtComment,
            amount: formValues.amount,
            // expertUserId: formValues.userId,
            courtFeeAmount: formValues.courtFeeAmount ? formValues.courtFeeAmount : 0,
            fineAmount: formValues.fineAmount ? formValues.fineAmount : 0,
        };
        this.globalService.onBlockUI(true);
        this.apiService.updateDebt(request).subscribe(response => {
            this.modalBlocked = false;
            this.displayDebtChangeModal = false;
            this.globalService.showSuccess('ოპერაცია წარმატებთ დასრულდა');
            this.globalService.onBlockUI(false);
            this.getDebt();
        }, error => {
            this.modalBlocked = false;
            this.globalService.showError('დაფიქსირდა შეცდომა');
            this.globalService.onBlockUI(false);
        });
    }

    hasRight(right) {
        return hasRight(this.user, right);
    }

    discussDebtToConfirmFullyPaid() {
        this.confirmationService.confirm({
            message: 'დარწმუნებული ხართ რომ გსურთ მონიშნული ვალის განულების მოთხოვნა?',
            header: 'დადასტურება',
            icon: 'pi pi-info-circle',
            acceptLabel: 'კი',
            rejectLabel: 'არა',
            key: 'debtChangeConfirm',
            accept: () => {
                this.globalService.onBlockUI(true);
                this.apiService.discussDebtToConfirmFullyPaid(this.debtId).subscribe(response => {
                    this.globalService.onBlockUI(false);
                    if (response.valid) {
                        this.globalService.showSuccess('ოპერაცია წარმატებთ დასრულდა');
                        this.getDebt();
                    } else {
                        this.globalService.showError(response.description);
                    }
                }, error => {
                    this.globalService.showError('დაფიქსირდა შეცდომა');
                    this.globalService.onBlockUI(false);
                });
            }
        });
    }

    discussDebtToConfirmResult(confirmedStatus) {
        this.confirmationService.confirm({
            message: 'დარწმუნებული ხართ რომ გსურთ ვალის განულებაზე გადაწყვეტილების მიღება?',
            header: 'დადასტურება',
            icon: 'pi pi-info-circle',
            acceptLabel: 'კი',
            rejectLabel: 'არა',
            key: 'debtChangeConfirm',
            accept: () => {
                this.globalService.onBlockUI(true);
                this.apiService.discussDebtToConfirmResult(this.debtId, this.debt.debtToConfirmPaidDto[0].id, confirmedStatus)
                    .subscribe(response => {
                        this.globalService.onBlockUI(false);
                        if (response.valid) {
                            this.globalService.showSuccess('ოპერაცია წარმატებთ დასრულდა');
                            this.getDebt();
                        } else {
                            this.globalService.showError(response.description);
                        }
                    }, error => {
                        this.globalService.showError('დაფიქსირდა შეცდომა');
                        this.globalService.onBlockUI(false);
                    });
            }
        });
    }

    openUpdateDebtDebtModal() {
        this.updateDebtComment = '';
        this.displayDebtChangeModal = true;
    }

    openRequestAssigneeDebtToAdministrationModal() {
        this.requestAssigneeDebtToAdministrationDebtComment = '';
        this.displayRequestAssigneeDebtToAdministrationModal = true;
    }

    openRequestAssigneeDebtToLawyerModal() {
        this.requestAssigneeDebtToLawyerDebtComment = '';
        this.displayRequestAssigneeDebtToLawyerModal = true;
    }

    requestAssigneeDebt(assigneeTypeId, comment) {
        this.modalBlocked = true;
        const request = {
            debtId: this.debtId,
            assigneeTypeId,
            comment
        };

        this.apiService.requestAssigneeDebt(request)
            .subscribe(response => {
                this.modalBlocked = false;
                this.displayRequestAssigneeDebtToAdministrationModal = false;
                this.displayRequestAssigneeDebtToLawyerModal = false;
                if (response.valid) {
                    this.globalService.showSuccess('ოპერაცია წარმატებთ დასრულდა');
                    this.getDebt();
                } else {
                    this.globalService.showError(response.description);
                }
            }, error => {
                this.globalService.showError('დაფიქსირდა შეცდომა');
                this.modalBlocked = false;
            });
    }

    makeAssigneeDecision(confirmedStatus) {
        this.confirmationService.confirm({
            message: 'დარწმუნებული ხართ რომ გსურთ არჩეული ქმედების დადასტურება?',
            header: 'დადასტურება',
            icon: 'pi pi-info-circle',
            acceptLabel: 'კი',
            rejectLabel: 'არა',
            key: 'makeAssigneeDecisionConfirm',
            accept: () => {
                this.globalService.onBlockUI(true);
                const request = {
                    assigneeDebtRequestId: this.debt.assigneeDebtRequestDto[0].id,
                    status: {
                        id: confirmedStatus
                    }
                };
                this.apiService.makeAssigneeDecision(request)
                    .subscribe(response => {
                        this.globalService.onBlockUI(false);
                        if (response.valid) {
                            this.globalService.showSuccess('ოპერაცია წარმატებთ დასრულდა');
                            this.getDebt();
                        } else {
                            this.globalService.showError(response.description);
                        }
                    }, error => {
                        this.globalService.showError('დაფიქსირდა შეცდომა');
                        this.globalService.onBlockUI(false);
                    });
            }
        });
    }

    makeAssigneeDecisionForLawyer(confirmedStatus, userId) {
        this.globalService.onBlockUI(true);
        const request = {
            assigneeDebtRequestId: this.debt.assigneeDebtRequestDto[0].id,
            status: {
                id: confirmedStatus
            },
            lawyerId: userId
        };
        this.apiService.makeAssigneeDecisionForLawyer(request)
            .subscribe(response => {
                this.globalService.onBlockUI(false);
                if (response.valid) {
                    this.globalService.showSuccess('ოპერაცია წარმატებთ დასრულდა');
                    this.getDebt();
                } else {
                    this.globalService.showError(response.description);
                }
            }, error => {
                this.globalService.showError('დაფიქსირდა შეცდომა');
                this.globalService.onBlockUI(false);
            });
    }

    setRefusesToPay($event: any) {
        if (this.debt.refusesToPay !== $event.checked) {
            this.globalService.onBlockUI(true);
            this.apiService.refusesToPay({debtId: this.debtId, refuses: $event.checked}).subscribe(response => {
                this.globalService.onBlockUI(false);
                if (response.valid) {
                    this.globalService.showSuccess('ოპერაცია წარმატებთ დასრულდა');
                    this.getDebt();
                } else {
                    this.globalService.showError(response.description);
                }
            }, error => {
                this.globalService.showError('დაფიქსირდა შეცდომა');
                this.globalService.onBlockUI(false);
            });
        }
    }

    setNotInContact($event: any) {
        if (this.debt.notInContact !== $event.checked) {
            this.globalService.onBlockUI(true);
            this.apiService.contact({debtId: this.debtId, contact: $event.checked}).subscribe(response => {
                this.globalService.onBlockUI(false);
                if (response.valid) {
                    this.globalService.showSuccess('ოპერაცია წარმატებთ დასრულდა');
                    this.getDebt();
                } else {
                    this.globalService.showError(response.description);
                }
            }, error => {
                this.globalService.showError('დაფიქსირდა შეცდომა');
                this.globalService.onBlockUI(false);
            });
        }
    }

    setIsProcessed($event: any) {
        if (this.debt.processed !== $event.checked) {
            this.globalService.onBlockUI(true);
            this.apiService.isProcessed({
                debtId: this.debtId,
                isProcessed: $event.checked
            }).subscribe(response => {
                this.globalService.onBlockUI(false);
                if (response.valid) {
                    this.globalService.showSuccess('ოპერაცია წარმატებთ დასრულდა');
                    this.getDebt();
                } else {
                    this.globalService.showError(response.description);
                }
            }, error => {
                this.globalService.showError('დაფიქსირდა შეცდომა');
                this.globalService.onBlockUI(false);
            });
        }
    }

    setCanSeeExpert($event: any) {
        if (this.debt.cannSeeExpert !== $event.checked) {
            this.globalService.onBlockUI(true);
            this.apiService.canSeeExpert({
                debtId: this.debtId,
                canSee: $event.checked
            }).subscribe(response => {
                this.globalService.onBlockUI(false);
                if (response.valid) {
                    this.globalService.showSuccess('ოპერაცია წარმატებთ დასრულდა');
                    this.getDebt();
                } else {
                    this.globalService.showError(response.description);
                }
            }, error => {
                this.globalService.showError('დაფიქსირდა შეცდომა');
                this.globalService.onBlockUI(false);
            });
        }
    }

    openSearchUserForAssigneeLawyer() {
        this.displaySearchUserAssigneeLawyer = true;
    }

    selectUserAsigneeLawyer(event) {
        this.displaySearchUserAssigneeLawyer = false;
        this.makeAssigneeDecisionForLawyer(2, event.userId);
    }

    hasRole(roleId) {
        return hasRole(this.user, roleId);
    }
}
