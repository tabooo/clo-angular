import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '../../_services/api.service';
import {GlobalService} from '../../_services/global.service';
import {SearchPersonsFormComponent} from '../search-persons-form/search-persons-form.component';
import {DateFormatPipe} from '../../_helper/dates/pipe/date-format.pipe';
import {DebtComplexityTypeDto} from '../../_model/DebtComplexityTypeDto';
import {resetTimeToDateToISOString, setDefaultTimeToDate} from "../../_helper/dates/date-util";

@Component({
    selector: 'app-add-debt',
    templateUrl: './add-debt.component.html',
    styleUrls: ['./add-debt.component.scss']
})
export class AddDebtComponent implements OnInit {
    addForm: FormGroup;
    displaySearchModal: boolean;
    displaySearchUser: boolean;
    @ViewChild('SearchPersonsFormComponent', {static: false}) appSearchPersonsForm: SearchPersonsFormComponent;

    lastSearchPersonField;
    complexityTypes: DebtComplexityTypeDto[];

    constructor(private apiService: ApiService,
                private globalService: GlobalService,
                private fb: FormBuilder,
                private dateFormatPipe: DateFormatPipe) {
    }

    ngOnInit(): void {
        this.addForm = this.fb.group({
            debtDate: new FormControl('', [Validators.required]),
            amount: new FormControl('', [Validators.required]),
            borrowerPersonId: new FormControl('', [Validators.required]),
            borrowerPerson: new FormControl('', [Validators.required]),
            loanPersonId: new FormControl('', [Validators.required]),
            loanPerson: new FormControl('', [Validators.required]),
            user: new FormControl(),
            userId: new FormControl(),
            complexityType: new FormControl('', [Validators.required]),
            percent: new FormControl('', [Validators.required]),
            incomingDate: new FormControl('', [Validators.required]),
            courtFeeAmount: new FormControl(),
            fineAmount: new FormControl(),
        });

        this.globalService.onBlockUI(true);
        this.apiService.getDebtComplexityTypes().subscribe(response => {
            this.complexityTypes = response;
            this.globalService.onBlockUI(false);
        });
    }

    openSearchPersonModal(searchPersonType, field) {
        this.lastSearchPersonField = field;
        this.appSearchPersonsForm.setPersonType(searchPersonType);
        this.displaySearchModal = true;
    }

    selectPerson(obj) {
        if (this.lastSearchPersonField === 'borrowerPersonId') {
            this.addForm.patchValue({
                borrowerPersonId: obj.personType === 1 ? obj.person.personId : obj.person.organizationId,
                borrowerPerson: obj.personType === 1 ? obj.person.firstName + ' ' + obj.person.lastName : obj.person.organizationName
            });
        } else if (this.lastSearchPersonField === 'loanPersonId') {
            this.addForm.patchValue({
                loanPersonId: obj.personType === 1 ? obj.person.personId : obj.person.organizationId,
                loanPerson: obj.personType === 1 ? obj.person.firstName + ' ' + obj.person.lastName : obj.person.organizationName
            });
        }

        this.displaySearchModal = false;
    }

    openSearchUser() {
        this.displaySearchUser = true;
    }

    selectUser(event) {
        this.addForm.patchValue({
            userId: event.userId,
            user: event.userName
        });
        this.displaySearchUser = false;
    }

    saveDebt() {
        const valid = this.checkValidity(this.addForm);
        if (!valid) {
            this.globalService.showError('შეავსეთ აუცილებელი ველები');
            return;
        }

        const formValues = this.addForm.getRawValue();
        const request = {
            borrowerPersonId: formValues.borrowerPersonId,
            loanPersonId: formValues.loanPersonId,
            expertUserId: formValues.userId,
            amount: formValues.amount,
            courtFeeAmount: formValues.courtFeeAmount ? formValues.courtFeeAmount : 0,
            fineAmount: formValues.fineAmount ? formValues.fineAmount : 0,
            percent: formValues.percent,
            debtComplexityTypeId: formValues.complexityType?.id,
            debtDate: formValues.debtDate ? setDefaultTimeToDate(formValues.debtDate) : null,
            incomingDate: formValues.incomingDate ? setDefaultTimeToDate(formValues.incomingDate) : null,
        };
        this.globalService.onBlockUI(true);
        this.apiService.addDebt(request).subscribe(response => {
            this.globalService.showSuccess('ოპერაცია წარმატებთ დასრულდა');
            this.globalService.onBlockUI(false);
            this.addForm.reset();
        }, error => {
            this.globalService.showError('დაფიქსირდა შეცდომა');
            this.globalService.onBlockUI(false);
        });
    }

    getDebtComplexityTypeByDate(event) {
        const debtDate = resetTimeToDateToISOString(event);

        this.globalService.onBlockUI(true);
        this.apiService.getDebtComplexityTypeByDate(debtDate).subscribe(response => {
            this.globalService.onBlockUI(false);
            if (response) {
                this.addForm.patchValue({
                    complexityType: response,
                    percent: response.percent
                });
            }
        }, error => {
            this.globalService.showError('დაფიქსირდა შეცდომა');
            this.globalService.onBlockUI(false);
        });
    }

    public checkValidity(form): boolean {
        let valid = true;
        Object.keys(form.controls).forEach((key) => {
            form.controls[key].markAsDirty();
            if (valid) {
                valid = form.controls[key].valid;
            }
        });
        return valid;
    }
}
