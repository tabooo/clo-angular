import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {UserDTO} from '../../_model/UserDTO';
import {Table} from 'primeng/table';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {ApiService} from '../../_services/api.service';
import {GlobalService} from '../../_services/global.service';
import {ActivatedRoute, Router} from '@angular/router';
import {DateFormatPipe} from '../../_helper/dates/pipe/date-format.pipe';
import {AuthenticationService} from '../../_services/authentication.service';
import {LazyLoadEvent} from 'primeng/api';
import {decode} from '../../_helper/json-util';
import {hasRight} from '../../_helper/user-util';
import {DebtComplexityTypeDto} from '../../_model/DebtComplexityTypeDto';
import {LibDto} from '../../_model/LibDto';
import {SearchPersonsFormComponent} from '../search-persons-form/search-persons-form.component';
import {getLibFromLibsById} from '../../_helper/utils';
import {DebtStates} from "../../_libs/debtStates";

@Component({
    selector: 'app-debts-search-simple',
    templateUrl: './debts-search-simple.component.html',
    styleUrls: ['./debts-search-simple.component.scss']
})
export class DebtsSearchSimpleComponent implements OnInit {
    @Output() onSelectDebt: EventEmitter<any> = new EventEmitter<any>();
    user: UserDTO;
    @ViewChild('table', {static: false}) table: Table;
    searchForm: FormGroup;
    libs = 2;
    complexityTypes: DebtComplexityTypeDto[];
    scheduleStatuses: LibDto[];
    debts: any = {count: 0, debts: [], amount: 0};
    searchRequest: any = {
        firstResult: 0,
        limit: 10,
        borrowerPersonId: '',
        loanPersonId: '',
        userId: '',
        fullyPaid: false,
        debtToConfirmPaid: false,
    };
    now;
    displaySearchUserForSearch = false;
    lastSearchPersonField: any;
    displaySearchModal = false;
    @ViewChild('SearchPersonsFormComponent', {static: false}) appSearchPersonsForm: SearchPersonsFormComponent;

    statesOptions = DebtStates;

    constructor(private apiService: ApiService,
                private globalService: GlobalService,
                private fb: FormBuilder,
                private router: Router,
                private activatedRoute: ActivatedRoute,
                private dateFormatPipe: DateFormatPipe,
                private authenticationService: AuthenticationService) {
    }

    ngOnInit(): void {
        this.user = decode(localStorage.getItem(this.authenticationService.LOCAL_STORAGE_KEY));

        this.searchForm = this.fb.group({
            borrowerPerson: new FormControl(),
            borrowerPersonId: new FormControl(),
            loanPerson: new FormControl(),
            loanPersonId: new FormControl(),
            userId: new FormControl(),
            user: new FormControl(),
        });

        if (hasRight(this.user, 'ROLE_მხოლოდ საკუთარი ვალების ნახვა')) {
            this.searchForm.patchValue({
                userId: this.user.userId,
                user: this.user.firstName + ' ' + this.user.lastName
            });
            this.searchRequest.userId = this.user.userId;
        }

        this.globalService.onBlockUI(true);
        this.apiService.getDebtComplexityTypes().subscribe(response => {
            this.complexityTypes = response;
            this.reloadLib();
        });

        this.apiService.getDebtScheduleStatuses().subscribe(response => {
            this.scheduleStatuses = response;
            this.reloadLib();
        });
    }

    reloadLib() {
        this.libs -= 1;
        if (this.libs === 0) {
            this.globalService.onBlockUI(false);
        }
    }

    search(event: LazyLoadEvent, req) {
        this.globalService.onBlockUI(true);
        const formValues = this.searchForm.getRawValue();
        this.now = new Date();
        if (event == null) {
            this.table._first = this.searchRequest.firstResult;
            if (req == null) {
                this.table._first = 0;
                this.searchRequest.firstResult = 0;

                this.searchRequest.borrowerPersonId = formValues.borrowerPersonId ? formValues.borrowerPersonId : '';
                this.searchRequest.borrowerPerson = formValues.borrowerPerson ? formValues.borrowerPerson : '';
                this.searchRequest.loanPersonId = formValues.loanPersonId ? formValues.loanPersonId : '';
                this.searchRequest.loanPerson = formValues.loanPerson ? formValues.loanPerson : '';
                this.searchRequest.userId = formValues.userId ? formValues.userId : '';
                this.searchRequest.user = formValues.user ? formValues.user : '';
            }
        } else {
            this.searchRequest.firstResult = event.first;
        }

        this.apiService.searchDebts(this.searchRequest).subscribe(response => {
            this.debts = response;
            this.globalService.onBlockUI(false);
        }, error => {
            this.debts.count = 0;
            this.debts.debts = [];
            this.globalService.onBlockUI(false);
            this.globalService.showError(error.error.message);
        });
    }

    openSearchUserForSearch() {
        this.displaySearchUserForSearch = true;
    }

    selectUserForSearch(event) {
        this.displaySearchUserForSearch = false;
        this.searchForm.patchValue({
            user: event.firstName + ' ' + event.lastName,
            userId: event.userId
        });
    }

    openSearchPersonModal(searchPersonType, field) {
        this.lastSearchPersonField = field;
        this.appSearchPersonsForm.setPersonType(searchPersonType);
        this.displaySearchModal = true;
    }

    selectPerson(obj) {
        if (this.lastSearchPersonField === 'search-borrowerPersonId') {
            this.searchForm.patchValue({
                borrowerPersonId: obj.personType === 1 ? obj.person.personId : obj.person.organizationId,
                borrowerPerson: obj.personType === 1 ? obj.person.firstName + ' '
                    + obj.person.lastName : obj.person.organizationName
            });
        } else if (this.lastSearchPersonField === 'search-loanPersonId') {
            this.searchForm.patchValue({
                loanPersonId: obj.personType === 1 ? obj.person.personId : obj.person.organizationId,
                loanPerson: obj.personType === 1 ? obj.person.firstName + ' '
                    + obj.person.lastName : obj.person.organizationName
            });
        }

        this.displaySearchModal = false;
    }

    resetSearchForm() {
        this.searchForm.reset();
        // this.fullyPaid = false;
        if (hasRight(this.user, 'ROLE_მხოლოდ საკუთარი ვალების ნახვა')) {
            this.searchForm.patchValue({
                userId: this.user.userId,
                user: this.user.firstName + ' ' + this.user.lastName
            });
            this.searchRequest.userId = this.user.userId;
        }
    }

    hasRight(right) {
        return hasRight(this.user, right);
    }

    stringToDate(lastNextContactDate) {
        return new Date(lastNextContactDate);
    }

    getLibFromLibs(libId, libs) {
        return getLibFromLibsById(libId, libs);
    }

    selectDebt(debt: any) {
        this.onSelectDebt.emit(debt);
    }
}
