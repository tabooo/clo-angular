import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {GlobalService} from '../../../_services/global.service';
import {ApiService} from '../../../_services/api.service';
import {DebtDto} from '../../../_model/DebtDto';
import {LibDto} from '../../../_model/LibDto';
import {DebtScheduleDto, DebtSchedulePaymentDto} from '../../../_model/DebtScheduleDto';
import {DateFormatPipe} from '../../../_helper/dates/pipe/date-format.pipe';
import {TableService} from 'primeng/table';
import {UserDTO} from '../../../_model/UserDTO';
import {decode} from '../../../_helper/json-util';
import {AuthenticationService} from '../../../_services/authentication.service';
import {hasRight} from '../../../_helper/user-util';
import {setDefaultTimeToDate} from '../../../_helper/dates/date-util';

@Component({
    selector: 'app-debt-schedule',
    templateUrl: './debt-schedule.component.html',
    styleUrls: ['./debt-schedule.component.scss'],
    providers: [TableService],
    styles: [`
        :host ::ng-deep .p-cell-editing {
            padding-top: 0 !important;
            padding-bottom: 0 !important;
        }
    `]
})
export class DebtScheduleComponent implements OnInit {
    @Input() debt: DebtDto;
    user: UserDTO;
    libs = 2;

    debtSchedulePaymentsWholePrice = 0;
    debtSchedulePaymentsPaidAmount = 0;
    debtSchedulePaymentsLeftAmount = 0;
    debtSchedule: DebtScheduleDto;
    previewDebtSchedule: DebtScheduleDto;
    previewDebtScheduleOriginal: DebtScheduleDto;
    displayStatusChangeModal = false;
    displayStatusChangeModalTitle = '';
    displayModal = false;
    blockedPanelStatusChangeModal = false;
    blockedPanel = false;
    addForm: FormGroup;
    viewForm: FormGroup;
    statusChangeForm: FormGroup;
    scheduleTypes: LibDto[];
    debtScheduleStatuses: LibDto[];
    addButtonVisible = false;
    needPaidDebtExecutionAmount = false;
    isStandard = null;
    delegatePercentShow = false;

    clonedPayments: { [s: string]: DebtSchedulePaymentDto; } = {};
    now = new Date();

    paymentSchedule = [];
    generatedWholePrice = 0;
    monthMaxValue = 12;

    constructor(private globalService: GlobalService,
                private apiService: ApiService,
                private fb: FormBuilder,
                private dateFormatPipe: DateFormatPipe,
                private authenticationService: AuthenticationService) {
        this.user = decode(localStorage.getItem(this.authenticationService.LOCAL_STORAGE_KEY));
    }

    ngOnInit(): void {
        this.addForm = this.fb.group({
            debtAmount: new FormControl(),
            scheduleType: new FormControl('', [Validators.required]),
            delegatePercent: new FormControl(),
            month: new FormControl('', [Validators.required]),
            paymentStartDate: new FormControl('', [Validators.required]),
            comment: new FormControl(),
        });
        this.viewForm = this.fb.group({
            debtAmount: new FormControl(),
            scheduleType: new FormControl(),
            delegatePercent: new FormControl(),
            month: new FormControl(),
            paymentStartDate: new FormControl(),
            comment: new FormControl(),
        });
        this.statusChangeForm = this.fb.group({
            comment: new FormControl(),
            statusId: new FormControl(),
        });
    }

    reloadLib() {
        this.libs -= 1;
        if (this.libs === 0) {
            this.globalService.onBlockUI(false);
            this.getDebtSchedule();
        }
    }

    setDebt(debt) {
        this.debt = debt;
        this.addForm.patchValue({
            debtAmount: this.debt.amount,
        });

        this.globalService.onBlockUI(true);
        this.apiService.getDebtScheduleTypes().subscribe(response => {
            this.scheduleTypes = response;
            this.reloadLib();
        });

        this.globalService.onBlockUI(true);
        this.apiService.getDebtScheduleStatuses().subscribe(response => {
            this.debtScheduleStatuses = response;
            this.reloadLib();
        });
    }

    getDebtSchedule() {
        this.debtSchedule = null;
        this.debtSchedulePaymentsWholePrice = 0;
        this.debtSchedulePaymentsLeftAmount = 0;
        this.debtSchedulePaymentsPaidAmount = 0;
        this.globalService.onBlockUI(true);
        this.apiService.getActiveDebtSchedule(this.debt.id).subscribe(response => {
            if (response) {
                this.debtSchedule = response;
                this.addButtonVisible = false;

                this.viewForm.patchValue({
                    debtAmount: this.debt.amount,
                    scheduleType: this.debtSchedule?.scheduleType?.name,
                    delegatePercent: this.debtSchedule.delegatePercent,
                    month: this.debtSchedule.month,
                    paymentStartDate: this.debtSchedule.paymentStartDate,
                    comment: this.debtSchedule.comment,
                });

                this.debtSchedulePaymentsWholePrice = 0;
                this.debtSchedule.debtSchedulePayments.forEach(payment => {
                    this.debtSchedulePaymentsWholePrice += payment.requiredAmount;

                    if (payment.fullyPaid !== 1) {
                        this.debtSchedulePaymentsLeftAmount += payment.requiredAmount - payment.paidAmount;
                    }

                    this.debtSchedulePaymentsPaidAmount += payment.paidAmount;
                });
            } else {
                this.addButtonVisible = true;
            }
            this.globalService.onBlockUI(false);
        }, error => {
            this.globalService.onBlockUI(false);
            this.globalService.showError(error.error.message);
        });
    }

    addNewSchedule() {
        this.globalService.onBlockUI(true);
        this.apiService.checkIfNeedPaidDebtExecutionAmount(this.debt.id).subscribe(response => {
            this.needPaidDebtExecutionAmount = response;
            if (!this.needPaidDebtExecutionAmount) {
                this.displayModal = true;
            } else {
                this.globalService.showError('ამ ვალზე გადასახდელია სასამართლო ბაჟი');
            }
            this.globalService.onBlockUI(false);
        });
    }

    save() {
        const valid = this.checkValidity(this.addForm);
        if (!valid) {
            this.globalService.showError('შეავსეთ აუცილებელი ველები');
            return;
        }

        this.blockedPanel = true;
        const formValues = this.addForm.getRawValue();

        if (this.previewDebtSchedule && this.previewDebtSchedule.debtSchedulePayments) {
            this.previewDebtSchedule.debtSchedulePayments.forEach((item, index) => {
                if (this.previewDebtScheduleOriginal.debtSchedulePayments[index].requiredAmount !== item.requiredAmount) {
                    this.paymentSchedule[index] = item.requiredAmount;
                } else {
                    // this.paymentSchedule[index] = -1;
                }
            });
        }

        const request = {
            debtId: this.debt.id,
            scheduleTypeId: formValues.scheduleType?.id,
            delegatePercent: formValues.delegatePercent,
            month: formValues.month,
            comment: formValues.comment,
            paymentStartDate: formValues.paymentStartDate ? setDefaultTimeToDate(formValues.paymentStartDate) : null,
            paymentSchedule: this.paymentSchedule
        };
        this.apiService.addDebtSchedule(request).subscribe(response => {
            this.blockedPanel = false;
            if (response.valid) {
                this.displayModal = false;
                this.getDebtSchedule();
            } else {
                this.globalService.showError(response.description);
            }

        }, error => {
            this.blockedPanel = false;
            this.globalService.showError(error.error.message);
        });
    }

    generateDebtSchedule() {
        const valid = this.checkValidity(this.addForm);
        if (!valid) {
            this.globalService.showError('შეავსეთ აუცილებელი ველები');
            return;
        }

        this.blockedPanel = true;
        const formValues = this.addForm.getRawValue();

        if (this.previewDebtSchedule && this.previewDebtSchedule.debtSchedulePayments) {
            this.previewDebtSchedule.debtSchedulePayments.forEach((item, index) => {
                if (this.previewDebtScheduleOriginal.debtSchedulePayments[index].requiredAmount !== item.requiredAmount) {
                    this.paymentSchedule[index] = item.requiredAmount;
                } else {
                    // this.paymentSchedule[index] = -1;
                }
            });
        }

        const request = {
            debtId: this.debt.id,
            scheduleTypeId: formValues.scheduleType?.id,
            delegatePercent: formValues.delegatePercent,
            month: formValues.month,
            comment: formValues.comment,
            paymentStartDate: formValues.paymentStartDate ? setDefaultTimeToDate(formValues.paymentStartDate) : null,
            paymentSchedule: this.paymentSchedule
        };
        this.apiService.generateDebtScheduleForPreview(request).subscribe(response => {
            this.blockedPanel = false;
            if (response.valid) {
                this.previewDebtSchedule = response.added.debtSchedule;
                this.previewDebtScheduleOriginal = (JSON.parse(JSON.stringify(response.added.debtSchedule)));
                /*this.addForm.patchValue({
                    month: this.previewDebtSchedule.month
                });*/
                if (this.paymentSchedule.length < 1) {
                    for (let i = 0; i < this.previewDebtSchedule.month; i++) {
                        this.paymentSchedule.push(-1);
                    }
                }
                this.generatedWholePrice = 0;
                this.previewDebtSchedule.debtSchedulePayments.forEach(payment => {
                    this.generatedWholePrice += payment.requiredAmount;
                });
            } else {
                this.globalService.showError(response.description);
            }

        }, error => {
            this.blockedPanel = false;
            this.globalService.showError(error.error.message);
        });
    }

    onScheduleTypeChange($event) {
        this.isStandard = $event?.value?.id;
        this.monthMaxValue = 12;
        if (this.isStandard === 2) {
            this.delegatePercentShow = true;
            this.monthMaxValue = null;
        }
    }

    onRowEditInit(payment: DebtSchedulePaymentDto) {
        this.clonedPayments[new Date(payment.payDate).getTime()] = {...payment};
    }

    onRowEditSave(payment: DebtSchedulePaymentDto) {
        if (payment.requiredAmount > 0) {
            delete this.clonedPayments[new Date(payment.payDate).getTime()];
        } else {
            this.globalService.showError('არასწორი თანხა');
        }
    }

    onRowEditCancel(payment: DebtSchedulePaymentDto, index: number) {
        this.previewDebtSchedule.debtSchedulePayments[index] = this.clonedPayments[new Date(payment.payDate).getTime()];
        delete this.clonedPayments[new Date(payment.payDate).getTime()];
    }

    openStatusChangeModal(statusId, title) {
        this.statusChangeForm.patchValue({
            statusId,
            comment: ''
        });
        this.displayStatusChangeModal = true;
        this.displayStatusChangeModalTitle = title;
    }

    changeDebtScheduleStatus() {
        const formData = this.statusChangeForm.getRawValue();

        this.globalService.onBlockUI(true);
        const request = {
            scheduleId: this.debtSchedule.id,
            statusId: formData.statusId,
            comment: formData.comment
        };
        this.apiService.changeDebtScheduleStatus(request).subscribe(response => {
            this.globalService.onBlockUI(false);
            if (response.valid) {
                this.getDebtSchedule();
                this.displayStatusChangeModal = false;
            } else {
                this.globalService.showError(response.description);
            }

        }, error => {
            this.globalService.onBlockUI(false);
            this.globalService.showError(error.error.message);
        });
    }

    hasRight(right) {
        return hasRight(this.user, right);
    }

    public checkValidity(form): boolean {
        let valid = true;
        Object.keys(form.controls).forEach((key) => {
            form.controls[key].markAsDirty();
            if (valid) {
                valid = form.controls[key].valid;
            }
        });
        return valid;
    }
}
