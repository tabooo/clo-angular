import {Component, Input, OnInit} from '@angular/core';
import {GlobalService} from '../../../_services/global.service';
import {ApiService} from '../../../_services/api.service';
import {PersonContactDto} from '../../../_model/PersonContactDto';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ConfirmationService} from 'primeng/api';
import {hasRight} from '../../../_helper/user-util';
import {UserDTO} from '../../../_model/UserDTO';
import {decode} from '../../../_helper/json-util';
import {AuthenticationService} from '../../../_services/authentication.service';
import {LibDto} from '../../../_model/LibDto';

@Component({
    selector: 'app-person-contacts',
    templateUrl: './person-contacts.component.html',
    styleUrls: ['./person-contacts.component.scss']
})
export class PersonContactsComponent implements OnInit {
    user: UserDTO;
    @Input() personId;
    personContacts: PersonContactDto[];
    contactStatus: LibDto[];
    libs = 1;

    displayModal: boolean;
    blockedPanel = false;
    addForm: FormGroup;

    constructor(private globalService: GlobalService,
                private apiService: ApiService,
                private fb: FormBuilder,
                private confirmationService: ConfirmationService,
                private authenticationService: AuthenticationService) {
        this.user = decode(localStorage.getItem(this.authenticationService.LOCAL_STORAGE_KEY));
    }

    ngOnInit(): void {
        this.addForm = this.fb.group({
            contactInfo: new FormControl('', [Validators.required]),
            comment: new FormControl(),
            contactStatus: new FormControl(),
            id: new FormControl(),
        });
    }

    loadPersonContacts() {
        this.globalService.onBlockUI(true);
        this.apiService.getContactStatuses().subscribe(response => {
            this.contactStatus = response;
            this.globalService.onBlockUI(false);
            this.reloadLib();
        });
    }

    reloadLib() {
        this.libs -= 1;
        if (this.libs === 0) {
            this.getPersonContacts();
        }
    }

    getPersonContacts() {
        this.globalService.onBlockUI(true);
        this.apiService.getPersonContacts(this.personId).subscribe(response => {
            this.personContacts = response;
            this.globalService.onBlockUI(false);
        });

    }

    addNewContact(contact) {
        if (contact) {
            this.addForm.patchValue({
                id: contact.id,
                contactInfo: contact.contactInfo,
                comment: contact.comment,
                contactStatus: contact.contactStatus
            });
        } else {
            this.addForm.patchValue({
                id: null,
                contactInfo: null,
                comment: null,
                contactStatus: null
            });
        }
        this.displayModal = true;
    }

    setPersonId(personId) {
        this.personId = personId;
    }

    save() {
        const valid = this.checkValidity(this.addForm);
        if (!valid) {
            this.globalService.showError('შეავსეთ აუცილებელი ველები');
            return;
        }

        const formValues = this.addForm.getRawValue();
        const request = {
            personId: this.personId,
            contactInfo: formValues.contactInfo,
            comment: formValues.comment,
            contactStatusId: formValues.contactStatus?.id,
            id: formValues.id,
        };

        this.blockedPanel = true;
        if (request.id) {
            this.apiService.updatePersonContact(request).subscribe(response => {
                this.blockedPanel = false;
                if (response.valid) {
                    this.getPersonContacts();
                    this.displayModal = false;
                } else {
                    this.globalService.showError(response?.description);
                }
            }, error => {
                this.blockedPanel = false;
                this.globalService.showError('დაფიქსირდა შეცდომა');
            });
        } else {
            this.apiService.addPersonContact(request).subscribe(response => {
                this.blockedPanel = false;
                if (response.valid) {
                    this.getPersonContacts();
                    this.displayModal = false;
                } else {
                    this.globalService.showError(response?.description);
                }
            }, error => {
                this.blockedPanel = false;
                this.globalService.showError('დაფიქსირდა შეცდომა');
            });
        }
    }

    removePersonContact(contact) {
        this.confirmationService.confirm({
            message: 'დარწმუნებული ხართ რომ გსურთ წაშლა?',
            header: 'დადასტურება',
            icon: 'pi pi-info-circle',
            acceptLabel: 'კი',
            rejectLabel: 'არა',
            accept: () => {
                this.globalService.onBlockUI(true);
                this.apiService.removePersonContact(contact.id).subscribe(response => {
                    this.globalService.onBlockUI(false);
                    if (response.valid) {
                        this.getPersonContacts();
                    } else {
                        this.globalService.showError(response?.description);
                    }
                }, error => {
                    this.globalService.onBlockUI(false);
                    this.globalService.showError('დაფიქსირდა შეცდომა');
                });
            }
        });
    }

    hasRight(right) {
        return hasRight(this.user, right);
    }

    public checkValidity(form): boolean {
        let valid = true;
        Object.keys(form.controls).forEach((key) => {
            form.controls[key].markAsDirty();
            if (valid) {
                valid = form.controls[key].valid;
            }
        });
        return valid;
    }
}
