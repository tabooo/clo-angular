import {Component, Input, OnInit} from '@angular/core';
import {GlobalService} from '../../../_services/global.service';
import {ApiService} from '../../../_services/api.service';
import {DebtScheduleDto} from '../../../_model/DebtScheduleDto';

@Component({
    selector: 'app-debt-schedules',
    templateUrl: './debt-schedules.component.html',
    styleUrls: ['./debt-schedules.component.scss']
})
export class DebtSchedulesComponent implements OnInit {
    @Input() debtId;
    debtSchedules: DebtScheduleDto[];

    constructor(private globalService: GlobalService,
                private apiService: ApiService) {
    }

    ngOnInit(): void {
    }

    setDebtId(debtId) {
        this.debtId = debtId;
    }

    getDebtSchedules() {
        this.globalService.onBlockUI(true);
        this.apiService.getDebtSchedules(this.debtId).subscribe(response => {
            this.debtSchedules = response;
            this.globalService.onBlockUI(false);
        }, error => {
            this.globalService.onBlockUI(false);
            this.globalService.showError(error.error.message);
        });
    }
}
