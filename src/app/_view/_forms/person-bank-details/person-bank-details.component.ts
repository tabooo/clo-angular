import {Component, Input, OnInit} from '@angular/core';
import {UserDTO} from '../../../_model/UserDTO';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {GlobalService} from '../../../_services/global.service';
import {ApiService} from '../../../_services/api.service';
import {ConfirmationService} from 'primeng/api';
import {AuthenticationService} from '../../../_services/authentication.service';
import {decode} from '../../../_helper/json-util';
import {PersonBankDetailDto} from '../../../_model/PersonBankDetailDto';
import {hasRight} from '../../../_helper/user-util';

@Component({
    selector: 'app-person-bank-details',
    templateUrl: './person-bank-details.component.html',
    styleUrls: ['./person-bank-details.component.scss']
})
export class PersonBankDetailsComponent implements OnInit {
    user: UserDTO;
    @Input() personId;
    personBankDetails: PersonBankDetailDto[];

    displayModal: boolean;
    blockedPanel = false;
    addForm: FormGroup;

    constructor(private globalService: GlobalService,
                private apiService: ApiService,
                private fb: FormBuilder,
                private confirmationService: ConfirmationService,
                private authenticationService: AuthenticationService) {
        this.user = decode(localStorage.getItem(this.authenticationService.LOCAL_STORAGE_KEY));
    }

    ngOnInit(): void {
        this.addForm = this.fb.group({
            bankDetail: new FormControl('', [Validators.required]),
            comment: new FormControl(),
            id: new FormControl(),
        });
    }

    getPersonBankDetails() {
        this.globalService.onBlockUI(true);
        this.apiService.getPersonBankDetails(this.personId).subscribe(response => {
            this.personBankDetails = response;
            this.globalService.onBlockUI(false);
        });
    }

    addNewBankDetail(bankDetail) {
        if (bankDetail) {
            this.addForm.patchValue({
                bankDetail: bankDetail.bankDetail,
                comment: bankDetail.comment,
                id: bankDetail.id
            });
        } else {
            this.addForm.patchValue({
                bankDetail: null,
                comment: null,
                id: null
            });
        }
        this.displayModal = true;
    }

    setPersonId(personId) {
        this.personId = personId;
    }

    save() {
        const valid = this.checkValidity(this.addForm);
        if (!valid) {
            this.globalService.showError('შეავსეთ აუცილებელი ველები');
            return;
        }

        const formValues = this.addForm.getRawValue();
        const request = {
            personId: this.personId,
            bankDetail: formValues.bankDetail,
            comment: formValues.comment,
            id: formValues.id
        };

        this.blockedPanel = true;
        this.apiService.savePersonBankDetail(request).subscribe(response => {
            this.blockedPanel = false;
            if (response.valid) {
                this.getPersonBankDetails();
                this.displayModal = false;
            } else {
                this.globalService.showError(response?.description);
            }
        }, error => {
            this.blockedPanel = false;
            this.globalService.showError('დაფიქსირდა შეცდომა');
        });
    }


    removePersonBankDetail(bankDetail) {
        this.confirmationService.confirm({
            message: 'დარწმუნებული ხართ რომ გსურთ წაშლა?',
            header: 'დადასტურება',
            icon: 'pi pi-info-circle',
            acceptLabel: 'კი',
            rejectLabel: 'არა',
            accept: () => {
                this.globalService.onBlockUI(true);
                this.apiService.removePersonBankDetail(bankDetail.id).subscribe(response => {
                    this.globalService.onBlockUI(false);
                    if (response.valid) {
                        this.getPersonBankDetails();
                    } else {
                        this.globalService.showError(response?.description);
                    }
                }, error => {
                    this.globalService.onBlockUI(false);
                    this.globalService.showError('დაფიქსირდა შეცდომა');
                });
            }
        });
    }

    hasRight(right) {
        return hasRight(this.user, right);
    }

    public checkValidity(form): boolean {
        let valid = true;
        Object.keys(form.controls).forEach((key) => {
            form.controls[key].markAsDirty();
            if (valid) {
                valid = form.controls[key].valid;
            }
        });
        return valid;
    }
}
