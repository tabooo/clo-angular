import {Component, Input, OnInit} from '@angular/core';
import {GlobalService} from '../../../_services/global.service';
import {ApiService} from '../../../_services/api.service';
import {DebtChangeResponseDto} from '../../../_model/DebtChangeResponseDto';
import {Router} from '@angular/router';

@Component({
    selector: 'app-debt-changes',
    templateUrl: './debt-changes.component.html',
    styleUrls: ['./debt-changes.component.scss']
})
export class DebtChangesComponent implements OnInit {
    @Input() debtId;
    debtChanges: DebtChangeResponseDto[];

    constructor(private globalService: GlobalService,
                private apiService: ApiService,
                private router: Router) {
    }

    ngOnInit(): void {
    }

    setDebtId(debtId) {
        this.debtId = debtId;
    }

    getDebtChanges() {
        this.globalService.onBlockUI(true);
        this.apiService.getDebtChanges(this.debtId).subscribe(response => {
            this.debtChanges = response;
            this.globalService.onBlockUI(false);
        }, error => {
            this.globalService.onBlockUI(false);
            this.globalService.showError(error.error.message);
        });
    }

    openPerson(personId) {
        this.router.navigate(['/main/person/' + personId]);
    }

    openOrganization(organizationId) {
        this.router.navigate(['/main/organization/' + organizationId]);
    }

}
