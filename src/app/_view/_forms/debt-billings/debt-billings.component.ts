import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {DebtBillingDto} from '../../../_model/DebtBillingDto';
import {GlobalService} from '../../../_services/global.service';
import {ApiService} from '../../../_services/api.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {DateFormatPipe} from '../../../_helper/dates/pipe/date-format.pipe';
import {FileUpload} from 'primeng/fileupload';
import {ConfirmationService} from 'primeng/api';
import {setDefaultTimeToDate, setMaxTimeToDateToISOString, setTimeToDate} from '../../../_helper/dates/date-util';

@Component({
    selector: 'app-debt-billings',
    templateUrl: './debt-billings.component.html',
    styleUrls: ['./debt-billings.component.scss']
})
export class DebtBillingsComponent implements OnInit {
    @Input() debtId;
    @Output() onAddDebtBilling: EventEmitter<any> = new EventEmitter<any>();
    debtBillings: DebtBillingDto[];
    addForm: FormGroup;
    displayModal = false;
    blockedPanel = false;
    now = new Date();
    uploadedFiles: any[] = [];
    @ViewChild('fileUploadButton', {static: false}) fileUploadButton: FileUpload;
    currentBilling: DebtBillingDto;

    constructor(private globalService: GlobalService,
                private apiService: ApiService,
                private fb: FormBuilder,
                private dateFormatPipe: DateFormatPipe,
                private confirmationService: ConfirmationService) {
        this.now = setMaxTimeToDateToISOString(new Date());
    }

    ngOnInit(): void {
        this.addForm = this.fb.group({
            paymentDate: new FormControl('', [Validators.required]),
            amount: new FormControl('', [Validators.required]),
            comment: new FormControl(),
            payByCheck: new FormControl(),
        });
    }

    setDebtId(debtId) {
        this.debtId = debtId;
    }

    getDebtBillings() {
        this.globalService.onBlockUI(true);
        this.apiService.getDebtBillings(this.debtId).subscribe(response => {
            this.debtBillings = response;
            this.globalService.onBlockUI(false);
        }, error => {
            this.globalService.onBlockUI(false);
            this.globalService.showError(error.error.message);
        });
    }

    addDebtBilling() {
        const valid = this.checkValidity(this.addForm);
        if (!valid) {
            this.globalService.showError('შეავსეთ აუცილებელი ველები');
            return;
        }

        this.blockedPanel = true;
        const formValues = this.addForm.getRawValue();

        const request = {
            id: this.currentBilling ? this.currentBilling.id : null,
            debtId: this.debtId,
            amount: formValues.amount,
            comment: formValues.comment,
            payByCheck: formValues.payByCheck ? 1 : 0,
            paymentDate: formValues.paymentDate ? setTimeToDate(formValues.paymentDate) : null,
            fileId: this.uploadedFiles.length > 0 ? this.uploadedFiles[0].id : null,
        };
        this.apiService.addDebtBilling(request).subscribe(response => {
            this.blockedPanel = false;
            if (response.valid) {
                this.displayModal = false;
                this.getDebtBillings();
                this.onAddDebtBilling.emit();
            } else {
                this.globalService.showError(response.description);
            }

        }, error => {
            this.blockedPanel = false;
            this.globalService.showError(error.error.message);
        });
    }

    addNewBilling(billing) {
        this.addForm.reset();
        this.currentBilling = null;
        this.uploadedFiles = [];
        if (billing) {
            this.addForm.patchValue({
                paymentDate: new Date(billing.paymentDate),
                amount: billing.amount,
                comment: billing.comment,
                payByCheck: billing.payByCheck
            });
            if (billing.file) {
                this.uploadedFiles.push(billing.file);
            }
            this.currentBilling = billing;
        }
        this.displayModal = true;
    }

    uploadFile(event: any, fileUploadButton: FileUpload): void {
        if (this.uploadedFiles.length > 0) {
            return;
        }
        const file = event.files[0];

        const formData: FormData = new FormData();
        formData.append('file', file, file.name);
        formData.append('fileTypeId', '2');

        this.apiService.fileUpload(formData).subscribe(response => {
            if (!response.valid) {
                this.globalService.showError(response.description);
            } else {
                this.uploadedFiles.push(response.added);
                fileUploadButton.clear();
                fileUploadButton.disabled = true;
            }
        }, error => {
            fileUploadButton.clear();
            fileUploadButton.disabled = false;
            this.globalService.showError('ფაილის ატვირთვის დროს მოხდა შეცდომა');
        });
    }

    onModalClose() {
        this.addForm.reset();
        this.uploadedFiles = [];
        this.fileUploadButton.clear();
        this.fileUploadButton.disabled = false;
    }

    downloadFile(file: any) {
        window.open('/clo-java/rest/api/file/files/' + file.id);
    }

    removeDebtBilling(billingId) {
        this.confirmationService.confirm({
            message: 'დარწმუნებული ხართ რომ გსურთ წაშლა?',
            header: 'დადასტურება',
            icon: 'pi pi-info-circle',
            acceptLabel: 'კი',
            rejectLabel: 'არა',
            key: 'removeDebtBilling',
            accept: () => {
                this.globalService.onBlockUI(true);
                this.apiService.removeDebtBilling(billingId).subscribe(response => {
                    this.globalService.onBlockUI(false);
                    this.getDebtBillings();
                }, error => {
                    this.globalService.onBlockUI(false);
                    this.globalService.showError('დაფიქსირდა შეცდომა');
                });
            }
        });
    }

    removeDebtBillingFile(file: any) {
        this.uploadedFiles.forEach((item, index) => {
            if (item.id === file.id) {
                this.uploadedFiles.splice(index, 1);
            }
        });
    }

    public checkValidity(form): boolean {
        let valid = true;
        Object.keys(form.controls).forEach((key) => {
            form.controls[key].markAsDirty();
            if (valid) {
                valid = form.controls[key].valid;
            }
        });
        return valid;
    }
}
