import {Component, Input, OnInit} from '@angular/core';
import {GlobalService} from '../../../../_services/global.service';
import {ApiService} from '../../../../_services/api.service';
import {LibDto} from '../../../../_model/LibDto';

@Component({
    selector: 'app-debt-lawyer-status',
    templateUrl: './debt-lawyer-status.component.html',
    styleUrls: ['./debt-lawyer-status.component.scss']
})
export class DebtLawyerStatusComponent implements OnInit {
    @Input() debt;
    statuses: LibDto[];
    selectedStatus;

    constructor(private globalService: GlobalService,
                private apiService: ApiService) {
    }

    ngOnInit(): void {

    }

    setDebt(debt) {
        this.debt = debt;
    }

    getDebtLawyerStatuses() {
        this.globalService.onBlockUI(true);
        this.apiService.getDebtLawyerStatus().subscribe(response => {
            this.statuses = response;
            if (this.debt.lawyerStatus) {
                this.statuses.forEach(status => {
                    if (status.id === this.debt.lawyerStatus.id) {
                        this.selectedStatus = status;
                    }
                });
            }
            this.globalService.onBlockUI(false);
        });
    }

    addDebtLawyerStatus(newStatus: LibDto) {
        this.globalService.onBlockUI(true);
        this.apiService.addDebtLawyerStatus({debtId: this.debt.id, statusId: newStatus.id}).subscribe(response => {
            if (response.valid) {
                this.selectedStatus = newStatus;
                this.debt.lawyerStatus = newStatus;
                this.globalService.showSuccess('სტატუსი წარმატებით განახლდა');
            } else {
                if (this.debt.lawyerStatus) {
                    this.statuses.forEach(status => {
                        if (status.id === this.debt.lawyerStatus.id) {
                            this.selectedStatus = status;
                        }
                    });
                } else {
                    this.selectedStatus = null;
                }
                this.globalService.showError(response.description);
            }
            this.globalService.onBlockUI(false);
        }, error => {
            this.globalService.showError(error.error.message);
            this.globalService.onBlockUI(false);
        });
    }
}
