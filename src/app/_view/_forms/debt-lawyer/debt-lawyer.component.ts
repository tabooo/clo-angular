import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {UserDTO} from '../../../_model/UserDTO';
import {GlobalService} from '../../../_services/global.service';
import {ApiService} from '../../../_services/api.service';
import {ConfirmationService} from 'primeng/api';
import {AuthenticationService} from '../../../_services/authentication.service';
import {decode} from '../../../_helper/json-util';
import {DebtLawyerFilesComponent} from './debt-lawyer-files/debt-lawyer-files.component';
import {DebtLawyerScheduleComponent} from './debt-lawyer-schedule/debt-lawyer-schedule.component';
import {DebtLawyerStatusComponent} from './debt-lawyer-status/debt-lawyer-status.component';

@Component({
    selector: 'app-debt-lawyer',
    templateUrl: './debt-lawyer.component.html',
    styleUrls: ['./debt-lawyer.component.scss'],
    styles: [`
        :host ::ng-deep .redBackground a {
            background-color: red !important;
        }
        :host ::ng-deep .redBackground a span{
            color: black;
            font-weight: bold;
        }
    `
    ]
})
export class DebtLawyerComponent implements OnInit {
    @Input() debt;

    displayModal: boolean;
    blockedPanel = false;
    addForm: FormGroup;

    user: UserDTO;

    @ViewChild('debtLawyerFilesSarcheliShesagebeli', {static: false}) debtLawyerFilesSarcheliShesagebeli: DebtLawyerFilesComponent;
    debtLawyerFilesSarcheliShesagebeliLoaded = false;

    @ViewChild('debtLawyerFilesDamatebitiDokumentacia', {static: false}) debtLawyerFilesDamatebitiDokumentacia: DebtLawyerFilesComponent;
    debtLawyerFilesDamatebitiDokumentaciaLoaded = false;

    @ViewChild('debtLawyerFilesSasamartloAqtebi', {static: false}) debtLawyerFilesSasamartloAqtebi: DebtLawyerFilesComponent;
    debtLawyerFilesSasamartloAqtebiLoaded = false;

    @ViewChild('debtLawyerFilesShesrulebuliDokumentacia', {static: false}) debtLawyerFilesShesrulebuliDokumentacia: DebtLawyerFilesComponent;
    debtLawyerFilesShesrulebuliDokumentaciaLoaded = false;

    @ViewChild('debtLawyerFilesMopasuxe', {static: false}) debtLawyerFilesMopasuxe: DebtLawyerFilesComponent;
    debtLawyerFilesMopasuxeLoaded = false;

    @ViewChild('debtLawyerScheduleComponent', {static: false}) debtLawyerScheduleComponent: DebtLawyerScheduleComponent;
    debtLawyerScheduleComponentLoaded = false;

    @ViewChild('debtLawyerStatusComponent', {static: false}) debtLawyerStatusComponent: DebtLawyerStatusComponent;
    debtLawyerStatusComponentLoaded = false;

    constructor(private globalService: GlobalService,
                private apiService: ApiService,
                private fb: FormBuilder,
                private confirmationService: ConfirmationService,
                private authenticationService: AuthenticationService) {
        this.user = decode(localStorage.getItem(this.authenticationService.LOCAL_STORAGE_KEY));
    }

    ngOnInit(): void {
        this.addForm = this.fb.group({
            comment: new FormControl(),
            link: new FormControl('', [Validators.required]),
            id: new FormControl(),
        });
    }

    setDebt(debt) {
        this.debt = debt;
        if (!this.debtLawyerFilesSarcheliShesagebeliLoaded) {
            this.debtLawyerFilesSarcheliShesagebeli.setDebtId(this.debt.id);
            this.debtLawyerFilesSarcheliShesagebeli.getDebtFiles();
            this.debtLawyerFilesSarcheliShesagebeliLoaded = true;
        }
    }

    onTabChange(event) {
        if (event.index === 0) {
            if (!this.debtLawyerFilesSarcheliShesagebeliLoaded) {
                this.debtLawyerFilesSarcheliShesagebeli.setDebtId(this.debt.id);
                this.debtLawyerFilesSarcheliShesagebeli.getDebtFiles();
                this.debtLawyerFilesSarcheliShesagebeliLoaded = true;
            }
        } else if (event.index === 1) {
            if (!this.debtLawyerFilesDamatebitiDokumentaciaLoaded) {
                this.debtLawyerFilesDamatebitiDokumentacia.setDebtId(this.debt.id);
                this.debtLawyerFilesDamatebitiDokumentacia.getDebtFiles();
                this.debtLawyerFilesDamatebitiDokumentaciaLoaded = true;
            }
        } else if (event.index === 2) {
            if (!this.debtLawyerFilesSasamartloAqtebiLoaded) {
                this.debtLawyerFilesSasamartloAqtebi.setDebtId(this.debt.id);
                this.debtLawyerFilesSasamartloAqtebi.getDebtFiles();
                this.debtLawyerFilesSasamartloAqtebiLoaded = true;
            }
        } else if (event.index === 3) {
            if (!this.debtLawyerFilesShesrulebuliDokumentaciaLoaded) {
                this.debtLawyerFilesShesrulebuliDokumentacia.setDebtId(this.debt.id);
                this.debtLawyerFilesShesrulebuliDokumentacia.getDebtFiles();
                this.debtLawyerFilesShesrulebuliDokumentaciaLoaded = true;
            }
        } else if (event.index === 4) {
            if (!this.debtLawyerFilesMopasuxeLoaded) {
                this.debtLawyerFilesMopasuxe.setDebtId(this.debt.id);
                this.debtLawyerFilesMopasuxe.getDebtFiles();
                this.debtLawyerFilesMopasuxeLoaded = true;
            }
        } else if (event.index === 5) {
            if (!this.debtLawyerScheduleComponentLoaded) {
                this.debtLawyerScheduleComponent.setDebtId(this.debt.id);
                this.debtLawyerScheduleComponent.getLawyerSchedules();
                this.debtLawyerScheduleComponentLoaded = true;
            }
        } else if (event.index === 6) {
            if (!this.debtLawyerStatusComponentLoaded) {
                this.debtLawyerStatusComponent.setDebt(this.debt);
                this.debtLawyerStatusComponent.getDebtLawyerStatuses();
                this.debtLawyerStatusComponentLoaded = true;
            }
        }
    }
}
