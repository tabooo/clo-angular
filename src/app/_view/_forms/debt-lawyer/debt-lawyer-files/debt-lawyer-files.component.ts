import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {File} from '../../../../_model/File';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {FileUpload} from 'primeng/fileupload';
import {GlobalService} from '../../../../_services/global.service';
import {ApiService} from '../../../../_services/api.service';
import {DateFormatPipe} from '../../../../_helper/dates/pipe/date-format.pipe';
import {ConfirmationService} from 'primeng/api';

@Component({
    selector: 'app-debt-lawyer-files',
    templateUrl: './debt-lawyer-files.component.html',
    styleUrls: ['./debt-lawyer-files.component.scss']
})
export class DebtLawyerFilesComponent implements OnInit {
    @Input() debtId;
    @Input() fileTypeId;
    files: File[];
    addForm: FormGroup;
    displayModal = false;
    blockedPanel = false;
    uploadedFiles: any[] = [];
    @ViewChild('fileUploadButton', {static: false}) fileUploadButton: FileUpload;
    currentDebtFile;

    constructor(private globalService: GlobalService,
                private apiService: ApiService,
                private fb: FormBuilder,
                private dateFormatPipe: DateFormatPipe,
                private confirmationService: ConfirmationService) {
    }

    ngOnInit(): void {
        this.addForm = this.fb.group({
            comment: new FormControl(),
        });
    }

    setDebtId(debtId) {
        this.debtId = debtId;
    }

    getDebtFiles() {
        this.globalService.onBlockUI(true);
        this.apiService.getDebtFilesByTypeId(this.debtId, this.fileTypeId).subscribe(response => {
            this.files = response;
            this.globalService.onBlockUI(false);
        }, error => {
            this.globalService.onBlockUI(false);
            this.globalService.showError(error.error.message);
        });
    }

    addDebtFile() {
        const valid = this.checkValidity(this.addForm);
        if (!valid) {
            this.globalService.showError('შეავსეთ აუცილებელი ველები');
            return;
        }

        this.blockedPanel = true;
        const formValues = this.addForm.getRawValue();

        const request = {
            debtId: this.debtId,
            fileId: this.uploadedFiles.length > 0 ? this.uploadedFiles[0].id : null,
            comment: formValues.comment,
        };
        this.apiService.addDebtFile(request).subscribe(response => {
            this.blockedPanel = false;
            if (response.valid) {
                this.displayModal = false;
                this.getDebtFiles();
            } else {
                this.globalService.showError(response.description);
            }

        }, error => {
            this.blockedPanel = false;
            this.globalService.showError(error.error.message);
        });
    }

    addNewFile(debtFile) {
        this.addForm.reset();
        this.currentDebtFile = null;
        this.uploadedFiles = [];
        if (debtFile) {
            this.addForm.patchValue({
                comment: debtFile.comment
            });
            if (debtFile.file) {
                this.uploadedFiles.push(debtFile.file);
            }
            this.currentDebtFile = debtFile;
        }
        this.displayModal = true;
    }

    uploadFile(event: any, fileUploadButton: FileUpload): void {
        if (this.uploadedFiles.length > 0) {
            return;
        }
        const file = event.files[0];

        const formData: FormData = new FormData();
        formData.append('file', file, file.name);
        formData.append('fileTypeId', this.fileTypeId);

        this.apiService.fileUpload(formData).subscribe(response => {
            if (!response.valid) {
                this.globalService.showError(response.description);
            } else {
                this.uploadedFiles.push(response.added);
                fileUploadButton.clear();
                fileUploadButton.disabled = true;
            }
        }, error => {
            fileUploadButton.clear();
            fileUploadButton.disabled = false;
            this.globalService.showError('ფაილის ატვირთვის დროს მოხდა შეცდომა');
        });
    }

    onModalClose() {
        this.addForm.reset();
        this.uploadedFiles = [];
        this.fileUploadButton.clear();
        this.fileUploadButton.disabled = false;
    }

    downloadFile(file: any) {
        window.open('/clo-java/rest/api/file/files/' + file.id);
    }

    removeDebtFile(fileId) {
        this.confirmationService.confirm({
            message: 'დარწმუნებული ხართ რომ გსურთ წაშლა?',
            header: 'დადასტურება',
            icon: 'pi pi-info-circle',
            acceptLabel: 'კი',
            rejectLabel: 'არა',
            key: 'removeDebtFile',
            accept: () => {
                this.globalService.onBlockUI(true);
                this.apiService.removeDebtFile(fileId).subscribe(response => {
                    this.globalService.onBlockUI(false);
                    this.getDebtFiles();
                }, error => {
                    this.globalService.onBlockUI(false);
                    this.globalService.showError('დაფიქსირდა შეცდომა');
                });
            }
        });
    }

    removeDebtFileFile(file: any) {
        this.uploadedFiles.forEach((item, index) => {
            if (item.id === file.id) {
                this.uploadedFiles.splice(index, 1);
            }
        });
    }

    public checkValidity(form): boolean {
        let valid = true;
        Object.keys(form.controls).forEach((key) => {
            form.controls[key].markAsDirty();
            if (valid) {
                valid = form.controls[key].valid;
            }
        });
        return valid;
    }

}
