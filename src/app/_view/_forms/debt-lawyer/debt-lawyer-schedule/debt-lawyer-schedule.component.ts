import {Component, Input, OnInit} from '@angular/core';
import {GlobalService} from '../../../../_services/global.service';
import {ApiService} from '../../../../_services/api.service';

@Component({
    selector: 'app-debt-lawyer-schedule',
    templateUrl: './debt-lawyer-schedule.component.html',
    styleUrls: ['./debt-lawyer-schedule.component.scss']
})
export class DebtLawyerScheduleComponent implements OnInit {
    @Input() debtId;
    plan = '';
    schedule = '';
    lawyerScheduleId = null;

    constructor(private globalService: GlobalService,
                private apiService: ApiService) {
    }

    ngOnInit(): void {
    }

    setDebtId(debtId) {
        this.debtId = debtId;
    }

    getLawyerSchedules() {
        this.globalService.onBlockUI(true);
        this.apiService.getLawyerSchedules(this.debtId).subscribe(response => {
            if (response && response.length > 0) {
                this.plan = response[0].plan;
                this.schedule = response[0].schedule;
                this.lawyerScheduleId = response[0].id;
            }

            this.globalService.onBlockUI(false);
        }, error => {
            this.globalService.onBlockUI(false);
            this.globalService.showError(error.error.message);
        });
    }

    saveDebtSchedule() {
        this.globalService.onBlockUI(true);
        const request = {
            debtId: this.debtId,
            plan: this.plan,
            schedule: this.schedule,
            id: this.lawyerScheduleId
        };

        this.apiService.saveLawyerSchedule(request).subscribe(response => {
            if (response.valid) {
                this.lawyerScheduleId = response.added.id;
            } else {
                this.globalService.showError(response.description);
            }

            this.globalService.onBlockUI(false);
        }, error => {
            this.globalService.onBlockUI(false);
            this.globalService.showError(error.error.message);
        });
    }
}
