import {Component, Input, OnInit} from '@angular/core';
import {PersonPropertyDto} from '../../../_model/PersonPropertyDto';
import {GlobalService} from '../../../_services/global.service';
import {ApiService} from '../../../_services/api.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ConfirmationService} from 'primeng/api';
import {LibDto} from '../../../_model/LibDto';
import {hasRight} from '../../../_helper/user-util';
import {UserDTO} from '../../../_model/UserDTO';
import {AuthenticationService} from '../../../_services/authentication.service';
import {decode} from '../../../_helper/json-util';

@Component({
    selector: 'app-person-properties',
    templateUrl: './person-properties.component.html',
    styleUrls: ['./person-properties.component.scss']
})
export class PersonPropertiesComponent implements OnInit {
    user: UserDTO;
    @Input() personId;
    personProperties: PersonPropertyDto[];

    displayModal: boolean;
    blockedPanel = false;
    addForm: FormGroup;
    propertyTypes: LibDto[];

    constructor(private globalService: GlobalService,
                private apiService: ApiService,
                private fb: FormBuilder,
                private confirmationService: ConfirmationService,
                private authenticationService: AuthenticationService) {
        this.user = decode(localStorage.getItem(this.authenticationService.LOCAL_STORAGE_KEY));
    }

    ngOnInit(): void {
        this.addForm = this.fb.group({
            propertyName: new FormControl('', [Validators.required]),
            comment: new FormControl(),
            id: new FormControl(),
            propertyType: new FormControl('', [Validators.required]),
        });

        this.globalService.onBlockUI(true);
        this.apiService.getDebtPropertyTypes().subscribe(response => {
            this.propertyTypes = response;
            this.globalService.onBlockUI(false);
        });
    }

    getPersonProperties() {
        this.globalService.onBlockUI(true);
        this.apiService.getPersonProperties(this.personId).subscribe(response => {
            this.personProperties = response;
            this.globalService.onBlockUI(false);
        });
    }

    addNewProperty(property) {
        if (property) {
            this.addForm.patchValue({
                propertyName: property.propertyName,
                comment: property.comment,
                propertyType: property.propertyType,
                id: property.id
            });
        } else {
            this.addForm.patchValue({
                propertyName: null,
                propertyType: null,
                comment: null,
                id: null
            });
        }
        this.displayModal = true;
    }

    setPersonId(personId) {
        this.personId = personId;
    }

    save() {
        const valid = this.checkValidity(this.addForm);
        if (!valid) {
            this.globalService.showError('შეავსეთ აუცილებელი ველები');
            return;
        }

        const formValues = this.addForm.getRawValue();
        const request = {
            personId: this.personId,
            propertyName: formValues.propertyName,
            propertyTypeId: formValues.propertyType?.id,
            comment: formValues.comment,
            id: formValues.id
        };

        if (request.id) {
            this.blockedPanel = true;
            this.apiService.updatePersonProperty(request).subscribe(response => {
                this.blockedPanel = false;
                if (response.valid) {
                    this.getPersonProperties();
                    this.displayModal = false;
                } else {
                    this.globalService.showError(response?.description);
                }
            }, error => {
                this.blockedPanel = false;
                this.globalService.showError('დაფიქსირდა შეცდომა');
            });
        } else {
            this.blockedPanel = true;
            this.apiService.addPersonProperty(request).subscribe(response => {
                this.blockedPanel = false;
                if (response.valid) {
                    this.getPersonProperties();
                    this.displayModal = false;
                } else {
                    this.globalService.showError(response?.description);
                }
            }, error => {
                this.blockedPanel = false;
                this.globalService.showError('დაფიქსირდა შეცდომა');
            });
        }
    }


    removePersonProperty(property) {
        this.confirmationService.confirm({
            message: 'დარწმუნებული ხართ რომ გსურთ წაშლა?',
            header: 'დადასტურება',
            icon: 'pi pi-info-circle',
            acceptLabel: 'კი',
            rejectLabel: 'არა',
            accept: () => {
                this.globalService.onBlockUI(true);
                this.apiService.removePersonProperty(property.id).subscribe(response => {
                    this.globalService.onBlockUI(false);
                    if (response.valid) {
                        this.getPersonProperties();
                    } else {
                        this.globalService.showError(response?.description);
                    }
                }, error => {
                    this.globalService.onBlockUI(false);
                    this.globalService.showError('დაფიქსირდა შეცდომა');
                });
            }
        });
    }

    hasRight(right) {
        return hasRight(this.user, right);
    }

    public checkValidity(form): boolean {
        let valid = true;
        Object.keys(form.controls).forEach((key) => {
            form.controls[key].markAsDirty();
            if (valid) {
                valid = form.controls[key].valid;
            }
        });
        return valid;
    }
}
