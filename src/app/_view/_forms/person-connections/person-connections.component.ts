import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {UserDTO} from '../../../_model/UserDTO';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {LibDto} from '../../../_model/LibDto';
import {GlobalService} from '../../../_services/global.service';
import {ApiService} from '../../../_services/api.service';
import {ConfirmationService} from 'primeng/api';
import {AuthenticationService} from '../../../_services/authentication.service';
import {decode} from '../../../_helper/json-util';
import {hasRight} from '../../../_helper/user-util';
import {SearchPersonsFormComponent} from '../../search-persons-form/search-persons-form.component';
import {Router} from '@angular/router';

@Component({
    selector: 'app-person-connections',
    templateUrl: './person-connections.component.html',
    styleUrls: ['./person-connections.component.scss']
})
export class PersonConnectionsComponent implements OnInit {
    user: UserDTO;
    @Input() personId;
    personConnections: any[] = [];
    personConnectionsTmp: any[] = [];

    displayModal: boolean;
    blockedPanel = false;
    addForm: FormGroup;
    connectionTypes: LibDto[];

    displaySearchModal = false;
    lastSearchPersonField: any;
    @ViewChild('SearchPersonsFormComponent', {static: false}) appSearchPersonsForm: SearchPersonsFormComponent;

    constructor(private globalService: GlobalService,
                private apiService: ApiService,
                private fb: FormBuilder,
                private confirmationService: ConfirmationService,
                private router: Router,
                private authenticationService: AuthenticationService) {
        this.user = decode(localStorage.getItem(this.authenticationService.LOCAL_STORAGE_KEY));
    }

    ngOnInit(): void {
        this.addForm = this.fb.group({
            connectionType: new FormControl('', [Validators.required]),
            secondPersonId: new FormControl(),
            secondPerson: new FormControl(),
        });

        this.globalService.onBlockUI(true);
        this.apiService.getPersonConnectionTypes().subscribe(response => {
            this.connectionTypes = response;
            this.globalService.onBlockUI(false);
        });
    }

    getPersonConnections() {
        this.personConnectionsTmp = [];
        this.personConnections = [];
        this.globalService.onBlockUI(true);
        this.apiService.getPersonConnections(this.personId).subscribe(response => {
            if (response && response.length > 0) {
                this.personConnectionsTmp = response;
                response.forEach(item => {
                    this.personConnections.push(item);
                });
            }
            this.globalService.onBlockUI(false);
        });
    }

    addNewConnection(property) {
        if (property) {
            this.addForm.patchValue({
                propertyName: property.propertyName,
                comment: property.comment,
                propertyType: property.propertyType,
                id: property.id
            });
        } else {
            this.addForm.patchValue({
                propertyName: null,
                propertyType: null,
                comment: null,
                id: null
            });
        }
        this.displayModal = true;
    }

    setPersonId(personId) {
        this.personId = personId;
    }

    save() {
        const valid = this.checkValidity(this.addForm);
        if (!valid) {
            this.globalService.showError('შეავსეთ აუცილებელი ველები');
            return;
        }

        const formValues = this.addForm.getRawValue();
        const request = {
            firstPersonId: this.personId,
            secondPersonId: formValues.secondPersonId,
            personConnectionTypeId: formValues.connectionType?.id,
        };

        this.blockedPanel = true;
        this.apiService.addPersonConnection(request).subscribe(response => {
            this.blockedPanel = false;
            if (response.valid) {
                this.getPersonConnections();
                this.displayModal = false;
            } else {
                this.globalService.showError(response?.description);
            }
        }, error => {
            this.blockedPanel = false;
            this.globalService.showError('დაფიქსირდა შეცდომა');
        });
    }


    removePersonConnection(connectionId) {
        this.confirmationService.confirm({
            message: 'დარწმუნებული ხართ რომ გსურთ წაშლა?',
            header: 'დადასტურება',
            icon: 'pi pi-info-circle',
            acceptLabel: 'კი',
            rejectLabel: 'არა',
            accept: () => {
                this.globalService.onBlockUI(true);
                this.apiService.removePersonConnection(connectionId).subscribe(response => {
                    this.globalService.onBlockUI(false);
                    if (response.valid) {
                        this.getPersonConnections();
                    } else {
                        this.globalService.showError(response?.description);
                    }
                }, error => {
                    this.globalService.onBlockUI(false);
                    this.globalService.showError('დაფიქსირდა შეცდომა');
                });
            }
        });
    }

    hasRight(right) {
        return hasRight(this.user, right);
    }

    public checkValidity(form): boolean {
        let valid = true;
        Object.keys(form.controls).forEach((key) => {
            form.controls[key].markAsDirty();
            if (valid) {
                valid = form.controls[key].valid;
            }
        });
        return valid;
    }

    onConnectionTypeChange($event: any) {
        this.personConnectionsTmp = [];
        if ($event.value === null) {
            this.personConnectionsTmp = this.personConnections;
            return;
        }

        const tmp = [];
        this.personConnections.forEach(item => {
            if (item.personConnectionType.id === $event.value.id) {
                tmp.push(item);
            }
        });
        this.personConnectionsTmp = tmp;
    }

    openSearchPersonModal(searchPersonType, field) {
        this.lastSearchPersonField = field;
        this.appSearchPersonsForm.setPersonType(searchPersonType);
        this.displaySearchModal = true;
    }

    selectPerson(obj) {
        if (obj.personType !== 1) {
            this.globalService.showError('აირჩიეთ პიროვნება');
            return;
        }
        this.addForm.patchValue({
            secondPersonId: obj.person.personId,
            secondPerson: obj.person.firstName + ' ' + obj.person.lastName
        });

        this.displaySearchModal = false;
    }

    openPerson(personId) {
        this.globalService.redirectTo('/main/person/' + personId);
    }
}
