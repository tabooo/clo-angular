import {Component, Input, OnInit} from '@angular/core';
import {DebtRsLinkResponseDto} from '../../../_model/DebtRsLinkResponseDto';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {GlobalService} from '../../../_services/global.service';
import {ApiService} from '../../../_services/api.service';
import {ConfirmationService} from 'primeng/api';
import {hasRight} from '../../../_helper/user-util';
import {UserDTO} from '../../../_model/UserDTO';
import {decode} from '../../../_helper/json-util';
import {AuthenticationService} from '../../../_services/authentication.service';

@Component({
    selector: 'app-debt-rs-links',
    templateUrl: './debt-rs-links.component.html',
    styleUrls: ['./debt-rs-links.component.scss']
})
export class DebtRsLinksComponent implements OnInit {
    @Input() debtId;
    debtRSLinks: DebtRsLinkResponseDto[];

    displayModal: boolean;
    blockedPanel = false;
    addForm: FormGroup;

    user: UserDTO;

    constructor(private globalService: GlobalService,
                private apiService: ApiService,
                private fb: FormBuilder,
                private confirmationService: ConfirmationService,
                private authenticationService: AuthenticationService) {
        this.user = decode(localStorage.getItem(this.authenticationService.LOCAL_STORAGE_KEY));
    }

    ngOnInit(): void {
        this.addForm = this.fb.group({
            comment: new FormControl(),
            link: new FormControl('', [Validators.required]),
            id: new FormControl(),
        });
    }

    getDebtRsLinks() {
        this.globalService.onBlockUI(true);
        this.apiService.getDebtRsLinks(this.debtId).subscribe(response => {
            this.debtRSLinks = response;
            this.globalService.onBlockUI(false);
        });
    }

    addNewRsLink(rsLink) {
        if (rsLink) {
            this.addForm.patchValue({
                id: rsLink.id,
                comment: rsLink.comment,
                link: rsLink.link,
            });
        } else {
            this.addForm.patchValue({
                id: null,
                comment: null,
                link: null,
            });
        }
        this.displayModal = true;
    }

    setDebtId(debtId) {
        this.debtId = debtId;
    }

    save() {
        const valid = this.checkValidity(this.addForm);
        if (!valid) {
            this.globalService.showError('შეავსეთ აუცილებელი ველები');
            return;
        }

        const formValues = this.addForm.getRawValue();
        const request = {
            debtId: this.debtId,
            comment: formValues.comment,
            link: formValues.link,
            id: formValues.id,
        };

        this.blockedPanel = true;
        if (request.id) {
            this.apiService.saveDebtRsLink(request).subscribe(response => {
                this.blockedPanel = false;
                this.getDebtRsLinks();
                this.displayModal = false;
            }, error => {
                this.blockedPanel = false;
                this.globalService.showError('დაფიქსირდა შეცდომა');
            });
        } else {
            this.apiService.saveDebtRsLink(request).subscribe(response => {
                this.blockedPanel = false;
                this.getDebtRsLinks();
                this.displayModal = false;
            }, error => {
                this.blockedPanel = false;
                this.globalService.showError('დაფიქსირდა შეცდომა');
            });
        }
    }

    removeDebtRsLink(rsLinkId) {
        this.confirmationService.confirm({
            message: 'დარწმუნებული ხართ რომ გსურთ წაშლა?',
            header: 'დადასტურება',
            icon: 'pi pi-info-circle',
            acceptLabel: 'კი',
            rejectLabel: 'არა',
            key: 'removeDebtRsLink',
            accept: () => {
                this.globalService.onBlockUI(true);
                this.apiService.removeDebtRsLink(rsLinkId).subscribe(response => {
                    this.globalService.onBlockUI(false);
                    if (response.valid) {
                        this.getDebtRsLinks();
                    } else {
                        this.globalService.showError(response?.description);
                    }
                }, error => {
                    this.globalService.onBlockUI(false);
                    this.globalService.showError('დაფიქსირდა შეცდომა');
                });
            }
        });
    }

    public checkValidity(form): boolean {
        let valid = true;
        Object.keys(form.controls).forEach((key) => {
            form.controls[key].markAsDirty();
            if (valid) {
                valid = form.controls[key].valid;
            }
        });
        return valid;
    }

    hasRight(right) {
        return hasRight(this.user, right);
    }
}
