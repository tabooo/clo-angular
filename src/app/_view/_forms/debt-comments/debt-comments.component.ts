import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {GlobalService} from '../../../_services/global.service';
import {ApiService} from '../../../_services/api.service';
import {ConfirmationService} from 'primeng/api';
import {DebtCommentDto} from '../../../_model/DebtCommentDto';
import {DateFormatPipe} from '../../../_helper/dates/pipe/date-format.pipe';
import {setDefaultTimeToDate} from '../../../_helper/dates/date-util';
import {DebtDto} from '../../../_model/DebtDto';

@Component({
    selector: 'app-debt-comments',
    templateUrl: './debt-comments.component.html',
    styleUrls: ['./debt-comments.component.scss']
})
export class DebtCommentsComponent implements OnInit {
    @Input() debt: DebtDto;
    debtComments: DebtCommentDto[];
    selectedComment: DebtCommentDto;

    displayModal: boolean;
    blockedPanel = false;
    addForm: FormGroup;

    promiseDateMin: Date;
    promiseDateMax: Date;
    nextContactMin: Date;
    nextContactMax: Date;

    constructor(private globalService: GlobalService,
                private apiService: ApiService,
                private fb: FormBuilder,
                private confirmationService: ConfirmationService,
                private dateFormatPipe: DateFormatPipe) {
    }

    ngOnInit(): void {
        this.addForm = this.fb.group({
            promiseDate: new FormControl(),
            nextContactDate: new FormControl('', [Validators.required]),
            comment: new FormControl('', [Validators.required]),
            id: new FormControl(),
        });


        const today = new Date();
        const month = today.getMonth();
        const year = today.getFullYear();
        const nextMonth = (month === 11) ? 0 : month + 1;
        const nextYear = (nextMonth === 0) ? year + 1 : year;

        /*this.promiseDateMin = new Date();
        this.promiseDateMax = new Date();
        this.promiseDateMax.setMonth(nextMonth);
        this.promiseDateMax.setFullYear(nextYear);*/

        this.nextContactMin = new Date();
        this.nextContactMax = new Date();
        this.nextContactMax.setDate(today.getDate() + 5);
    }

    getDebtComments() {
        this.globalService.onBlockUI(true);
        this.apiService.getDebtComments(this.debt.id).subscribe(response => {
            this.debtComments = response;
            this.globalService.onBlockUI(false);
        });
    }

    addNewComment(comment) {
        this.selectedComment = null;
        if (comment) {
            this.selectedComment = comment;
            this.addForm.patchValue({
                id: comment.id,
                comment: comment.comment,
                promiseDate: comment.promiseDate ? new Date(comment.promiseDate) : null,
                nextContactDate: comment.nextContactDate ? new Date(comment.nextContactDate) : null
            });
        } else {
            const now = new Date();
            now.setHours(12, 0, 0, 0);

            this.addForm.patchValue({
                id: null,
                comment: null,
                promiseDate: null,
                nextContactDate: null
            });
        }
        this.displayModal = true;
    }

    setDebt(debt) {
        this.debt = debt;
    }

    save() {
        const valid = this.checkValidity(this.addForm);
        if (!valid) {
            this.globalService.showError('შეავსეთ აუცილებელი ველები');
            return;
        }

        const formValues = this.addForm.getRawValue();
        const request = {
            debtId: this.debt.id,
            promiseDate: formValues.promiseDate ? setDefaultTimeToDate(formValues.promiseDate) : null,
            nextContactDate: formValues.nextContactDate ? setDefaultTimeToDate(formValues.nextContactDate) : null,
            comment: formValues.comment,
            id: formValues.id,
        };

        this.blockedPanel = true;
        if (request.id) {
            this.apiService.updateDebtComment(request).subscribe(response => {
                this.blockedPanel = false;
                this.getDebtComments();
                this.displayModal = false;
            }, error => {
                this.blockedPanel = false;
                this.globalService.showError('დაფიქსირდა შეცდომა');
            });
        } else {
            this.apiService.saveDebtComment(request).subscribe(response => {
                this.blockedPanel = false;
                this.getDebtComments();
                this.displayModal = false;
            }, error => {
                this.blockedPanel = false;
                this.globalService.showError('დაფიქსირდა შეცდომა');
            });
        }
    }

    public checkValidity(form): boolean {
        let valid = true;
        Object.keys(form.controls).forEach((key) => {
            form.controls[key].markAsDirty();
            if (valid) {
                valid = form.controls[key].valid;
            }
        });
        return valid;
    }

    removeDebtComment(comment: any) {
        this.confirmationService.confirm({
            message: 'დარწმუნებული ხართ რომ გსურთ წაშლა?',
            header: 'დადასტურება',
            icon: 'pi pi-info-circle',
            acceptLabel: 'კი',
            rejectLabel: 'არა',
            key: 'removeDebtComment',
            accept: () => {
                this.globalService.onBlockUI(true);
                this.apiService.removeDebtComment(comment.id).subscribe(response => {
                    this.globalService.onBlockUI(false);
                    this.getDebtComments();
                }, error => {
                    this.globalService.onBlockUI(false);
                    this.globalService.showError('დაფიქსირდა შეცდომა');
                });
            }
        });
    }
}
