import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {GlobalService} from '../../_services/global.service';
import {ActivatedRoute} from '@angular/router';
import {ApiService} from '../../_services/api.service';
import {OrganizationDto} from '../../_model/OrganizationDto';
import {PersonContactsComponent} from '../_forms/person-contacts/person-contacts.component';
import {PersonPropertiesComponent} from '../_forms/person-properties/person-properties.component';
import {UserDTO} from '../../_model/UserDTO';
import {AuthenticationService} from '../../_services/authentication.service';
import {decode} from '../../_helper/json-util';
import {hasRight} from '../../_helper/user-util';
import {PersonBankDetailsComponent} from '../_forms/person-bank-details/person-bank-details.component';

@Component({
    selector: 'app-organization-detail',
    templateUrl: './organization-detail.component.html',
    styleUrls: ['./organization-detail.component.scss']
})
export class OrganizationDetailComponent implements OnInit {
    user: UserDTO;
    @Input() organizationId;
    organizationForm: FormGroup;
    organization: OrganizationDto;

    @ViewChild('personContactsComponent', {static: false}) personContactsComponent: PersonContactsComponent;
    personContactsLoaded = false;

    @ViewChild('personPropertiesComponent', {static: false}) personPropertiesComponent: PersonPropertiesComponent;
    personPropertiesLoaded = false;

    @ViewChild('personBankDetailsComponent', {static: false}) personBankDetailsComponent: PersonBankDetailsComponent;
    personBankDetailsLoaded = false;

    constructor(private globalService: GlobalService,
                private activatedRoute: ActivatedRoute,
                private apiService: ApiService,
                private fb: FormBuilder,
                private authenticationService: AuthenticationService) {
        if (!this.organizationId) {
            this.activatedRoute.params.subscribe(params => {
                this.organizationId = params.organizationId;
                this.getOrganization();
            });
        } else {
            this.getOrganization();
        }
    }

    ngOnInit(): void {
        this.user = decode(localStorage.getItem(this.authenticationService.LOCAL_STORAGE_KEY));

        this.organizationForm = this.fb.group({
            organizationCode: new FormControl('', [Validators.required]),
            organizationName: new FormControl('', [Validators.required]),
            organizationNameEn: new FormControl(),
            address: new FormControl(),
            addressFact: new FormControl(),
            isLoaner: new FormControl(),
        });
    }

    getOrganization() {
        this.globalService.onBlockUI(true);
        this.apiService.getOrganization(this.organizationId).subscribe(response => {
            this.organization = response;
            this.organizationForm.setValue({
                organizationCode: response.organizationCode,
                organizationName: response.organizationName,
                organizationNameEn: response.organizationNameEn,
                address: response.address,
                addressFact: response.addressFact,
                isLoaner: response.isLoaner,
            });
            this.globalService.onBlockUI(false);
        });
    }

    onTabChange(event) {
        if (event.index === 1) {
            if (!this.personContactsLoaded) {
                this.personContactsComponent.setPersonId(this.organizationId);
                this.personContactsComponent.loadPersonContacts();
                this.personContactsLoaded = true;
            }
        } else if (event.index === 2) {
            if (!this.personPropertiesLoaded) {
                this.personPropertiesComponent.setPersonId(this.organizationId);
                this.personPropertiesComponent.getPersonProperties();
                this.personPropertiesLoaded = true;
            }
        } else if (event.index === 3) {
            if (!this.personBankDetailsLoaded) {
                this.personBankDetailsComponent.setPersonId(this.organizationId);
                this.personBankDetailsComponent.getPersonBankDetails();
                this.personBankDetailsLoaded = true;
            }
        }
    }

    hasRight(right) {
        return hasRight(this.user, right);
    }

    updateOrganization() {
        const valid = this.checkValidity();
        if (!valid) {
            this.globalService.showError('შეავსეთ აუცილებელი ველები');
            return;
        }

        this.globalService.onBlockUI(true);
        const formValues = this.organizationForm.getRawValue();

        const request = {
            organizationCode: formValues.organizationCode,
            organizationName: formValues.organizationName,
            organizationNameEn: formValues.organizationNameEn,
            address: formValues.address,
            addressFact: formValues.addressFact,
            organizationId: this.organizationId,
            isLoaner: formValues.isLoaner,
        };

        this.apiService.updateOrganization(request).subscribe(response => {
            this.globalService.onBlockUI(false);
            this.globalService.showSuccess('ორგანიზაცია წარმატებით დარედაქტირდა');
        }, error => {
            this.globalService.onBlockUI(false);
            this.globalService.showError(error.error.message);
        });
    }

    public checkValidity(): boolean {
        let valid = true;
        Object.keys(this.organizationForm.controls).forEach((key) => {
            this.organizationForm.controls[key].markAsDirty();
            if (valid) {
                valid = this.organizationForm.controls[key].valid;
            }
        });
        return valid;
    }
}
