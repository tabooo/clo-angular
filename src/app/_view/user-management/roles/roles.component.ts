import {Component, OnInit} from '@angular/core';
import {RoleDto} from '../../../_model/RoleDto';
import {ApiService} from '../../../_services/api.service';
import {GlobalService} from '../../../_services/global.service';
import {PrivilegeDto} from '../../../_model/PrivilegeDto';

@Component({
    selector: 'app-roles',
    templateUrl: './roles.component.html',
    styleUrls: ['./roles.component.scss']
})
export class RolesComponent implements OnInit {
    roles: RoleDto[];
    selectedRole: RoleDto;
    privileges: PrivilegeDto[];
    selectedPrivileges: PrivilegeDto[] = [];

    constructor(private apiService: ApiService,
                private globalService: GlobalService) {
    }

    ngOnInit(): void {
        this.globalService.onBlockUI(true);
        this.apiService.getRoles().subscribe(response => {
            this.roles = response;
            this.globalService.onBlockUI(false);
        });
        this.globalService.onBlockUI(true);
        this.apiService.getPrivileges().subscribe(response => {
            this.privileges = response;
            this.globalService.onBlockUI(false);
        });
    }

    onRoleSelect(event) {
        this.selectedRole = event.data;
        this.selectedPrivileges = [];
        this.globalService.onBlockUI(true);
        this.apiService.getRolePrivileges(event.data.id).subscribe(response => {
            // this.privileges = response;
            this.selectedPrivileges = response;
            this.globalService.onBlockUI(false);
        });
    }

    onRoleUnselect(event) {
        this.selectedPrivileges = [];
        this.selectedRole = null;
    }

    onPrivilegeSelect(event) {
        this.globalService.onBlockUI(true);
        this.apiService.addPrivilegeToRole(this.selectedRole.id, [event.data.id]).subscribe(response => {
            this.onRoleSelect({data: this.selectedRole});
            this.globalService.onBlockUI(false);
        }, error => {
            this.globalService.onBlockUI(false);
            this.globalService.showError(error.message);
        });
    }

    onPrivilegeUnselect(event) {
        this.globalService.onBlockUI(true);
        this.apiService.removePrivilegeFromRole(this.selectedRole.id, [event.data.id]).subscribe(response => {
            this.onRoleSelect({data: this.selectedRole});
            this.globalService.onBlockUI(false);
        }, error => {
            this.globalService.onBlockUI(false);
            this.globalService.showError(error.message);
        });
    }
}
