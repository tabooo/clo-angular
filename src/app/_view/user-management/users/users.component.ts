import {Component, OnInit, ViewChild} from '@angular/core';
import {Table} from 'primeng/table';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {ConfirmationService, LazyLoadEvent} from 'primeng/api';
import {ApiService} from '../../../_services/api.service';
import {GlobalService} from '../../../_services/global.service';
import {ActivatedRoute, Router} from '@angular/router';
import {DateFormatPipe} from '../../../_helper/dates/pipe/date-format.pipe';
import {UserDTO} from '../../../_model/UserDTO';
import {RoleDto} from '../../../_model/RoleDto';

@Component({
    selector: 'app-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.scss'],
    styles: [`
        :host ::ng-deep .row-accessories {
            background-color: #8be88b !important;
        }
    `
    ]
})
export class UsersComponent implements OnInit {
    @ViewChild('table', {static: false}) table: Table;
    searchForm: FormGroup;
    users: any = {
        content: [],
        totalElements: 0,
        totalPages: 0,
        size: 0,
    };
    searchRequest: any = {
        page: 0,
        size: 10,
        userName: '',
        firstName: '',
        lastName: '',
        personalNo: '',
        mobileNo: '',
        email: '',
    };

    roles: RoleDto[];
    curUser: UserDTO;
    displayUserModal = false;
    blockedPanel = false;
    userAddForm: FormGroup;

    actionButtonItems: any;

    displayresetPasswordModal = false;
    resetPasswordForm: FormGroup;
    blockedPanel3 = false;

    constructor(private apiService: ApiService,
                private globalService: GlobalService,
                private fb: FormBuilder,
                private router: Router,
                private activatedRoute: ActivatedRoute,
                private dateFormatPipe: DateFormatPipe,
                private confirmationService: ConfirmationService) {
    }

    ngOnInit(): void {
        this.actionButtonItems = [
            {
                label: 'პაროლის შეცვლა', icon: 'pi pi-lock-open', command: () => {
                    this.resetPasswordModalOpen(this.curUser);
                }
            },
            {
                label: 'წაშლა', icon: 'pi pi-trash', command: () => {
                    this.removeUser(this.curUser);
                }
            },
        ];

        this.searchForm = this.fb.group({
            userName: new FormControl(),
            firstName: new FormControl(),
            lastName: new FormControl(),
            personalNo: new FormControl(),
            mobileNo: new FormControl(),
            email: new FormControl(),
        });

        this.userAddForm = this.fb.group({
            userId: new FormControl(),
            userName: new FormControl(),
            firstName: new FormControl(),
            lastName: new FormControl(),
            personalNo: new FormControl(),
            birthDate: new FormControl(),
            address: new FormControl(),
            position: new FormControl(),
            mobileNo: new FormControl(),
            email: new FormControl(),
            accountNumber: new FormControl(),
            password: new FormControl(),
            role: new FormControl(),
        });

        this.resetPasswordForm = this.fb.group({
            password: new FormControl(),
            repeatPassword: new FormControl(),
        });

        this.activatedRoute.queryParams.subscribe(params => {
            if (params.userName) {
                this.searchRequest.userName = params.userName;
            }
            if (params.firstName) {
                this.searchRequest.firstName = params.firstName;
            }
            if (params.lastName) {
                this.searchRequest.lastName = params.lastName;
            }
            if (params.personalNo) {
                this.searchRequest.personalNo = params.personalNo;
            }
            if (params.mobileNo) {
                this.searchRequest.mobileNo = params.mobileNo;
            }
            if (params.email) {
                this.searchRequest.email = params.email;
            }
        });

        this.globalService.onBlockUI(true);
        this.apiService.getRoles().subscribe(response => {
            this.roles = response;
            this.globalService.onBlockUI(false);
        });

        this.search(null, this.searchRequest);
    }

    search(event: LazyLoadEvent, req) {
        const formValues = this.searchForm.getRawValue();
        if (event == null) {
            if (req == null) {
                this.table._first = 0;
                this.searchRequest.page = 0;

                this.searchRequest.userName = formValues.userName ? formValues.userName : '';
                this.searchRequest.firstName = formValues.firstName ? formValues.firstName : '';
                this.searchRequest.lastName = formValues.lastName ? formValues.lastName : '';
                this.searchRequest.personalNo = formValues.personalNo ? formValues.personalNo : '';
                this.searchRequest.mobileNo = formValues.mobileNo ? formValues.mobileNo : '';
                this.searchRequest.email = formValues.email ? formValues.email : '';
            }
        } else {
            this.searchRequest.page = (event.first / this.searchRequest.size);
        }

        const params = {
            page: this.searchRequest.page,
            size: this.searchRequest.size,
            userName: this.searchRequest.userName,
            firstName: this.searchRequest.firstName,
            lastName: this.searchRequest.lastName,
            personalNo: this.searchRequest.personalNo,
            mobileNo: this.searchRequest.mobileNo,
            email: this.searchRequest.email,
        };
        this.globalService.createUrl(['/main/users'], params);

        this.globalService.onBlockUI(true);
        this.apiService.searchUsers(this.searchRequest).subscribe(response => {
            this.users = response;
            if (this.users && this.users.content.length > 0) {
                this.users.content.forEach(user => {
                    user.roleName = '';
                    if (user.roles && user.roles.length > 0) {
                        user.roles.forEach((role, key, arr) => {
                            user.roleName += role.name;
                            if (Object.is(arr.length - 1, key)) {

                            } else {
                                user.roleName += ', ';
                            }
                        });
                    }

                });
            }
            this.globalService.onBlockUI(false);
        }, error => {
            this.users = {
                content: [],
                totalElements: 0,
                totalPages: 0,
                size: 10,
            };
            this.globalService.onBlockUI(false);
            this.globalService.showError(error.error.message);
        });
    }

    addUser(user) {
        this.userAddForm.reset();
        if (user) {
            this.curUser = user;
            this.blockedPanel = true;
            this.apiService.getUserById(user.userId).subscribe(response => {
                this.curUser = user;
                this.userAddForm.patchValue({
                    userName: user.userName,
                    firstName: user.firstName,
                    lastName: user.lastName,
                    personalNo: user.personalNo,
                    birthDate: user.birthDate ? new Date(user.birthDate) : null,
                    midName: user.midName,
                    address: user.address,
                    addressFact: user.addressFact,
                    position: user.position,
                    mobileNo: user.mobileNo,
                    email: user.email,
                    accountNumber: user.accountNumber,
                    userId: user.userId,
                    role: user.roles ? user.roles[0] : null,
                });
                this.blockedPanel = false;
            });
        } else {
            this.curUser = null;
        }
        this.displayUserModal = true;
    }

    openUser(user: any) {

    }

    saveUser() {
        this.globalService.onBlockUI(true);
        this.blockedPanel = true;
        const formValues = this.userAddForm.getRawValue();
        const request = {
            userName: formValues.userName,
            firstName: formValues.firstName,
            lastName: formValues.lastName,
            personalNo: formValues.personalNo,
            birthDate: formValues.birthDate,
            midName: formValues.midName,
            address: formValues.address,
            addressFact: formValues.addressFact,
            position: formValues.position,
            mobileNo: formValues.mobileNo,
            email: formValues.email,
            accountNumber: formValues.accountNumber,
            password: formValues.password,
            userId: formValues.userId,
            roleIds: [formValues.role?.id],
        };

        this.apiService.addUserWithRole(request).subscribe(response => {
            this.displayUserModal = false;
            this.globalService.onBlockUI(false);
            this.blockedPanel = false;
            this.globalService.showSuccess('პიროვნება წარმატებით დაემატა');
            this.search(null, this.searchRequest);
        }, error => {
            this.globalService.onBlockUI(false);
            this.blockedPanel = false;
            this.globalService.showError(error.error.message);
        });
    }

    removeUser(user) {
        this.confirmationService.confirm({
            message: 'დარწმუნებული ხართ რომ გსურთ წაშლა?',
            header: 'დადასტურება',
            icon: 'pi pi-info-circle',
            acceptLabel: 'კი',
            rejectLabel: 'არა',
            accept: () => {
                this.globalService.onBlockUI(true);
                this.apiService.removeUser(user.userId).subscribe(response => {
                    this.globalService.onBlockUI(false);
                    if (response.valid) {
                        this.search(null, this.searchRequest);
                    } else {
                        this.globalService.showError(response.description);
                    }
                });
            }
        });
    }

    resetPassword() {
        this.blockedPanel3 = true;
        const formValues = this.resetPasswordForm.getRawValue();
        const request = {
            password: formValues.password,
            repeatPassword: formValues.repeatPassword,
            userId: this.curUser.userId,
        };
        this.apiService.resetPassword(request).subscribe(response => {
            this.blockedPanel3 = false;
            this.displayresetPasswordModal = false;
            this.globalService.showSuccess('პაროლი წარმატებით შეიცვალა');
        }, error => {
            this.globalService.showError(error.message);
            this.blockedPanel3 = false;
        });
    }

    private resetPasswordModalOpen(curUser: UserDTO) {
        this.curUser = curUser;
        this.displayresetPasswordModal = true;
    }
}
