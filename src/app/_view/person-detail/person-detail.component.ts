import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {GlobalService} from '../../_services/global.service';
import {ActivatedRoute} from '@angular/router';
import {ApiService} from '../../_services/api.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {PersonDto} from '../../_model/PersonDto';
import {PersonContactsComponent} from '../_forms/person-contacts/person-contacts.component';
import {PersonPropertiesComponent} from '../_forms/person-properties/person-properties.component';
import {UserDTO} from '../../_model/UserDTO';
import {decode} from '../../_helper/json-util';
import {AuthenticationService} from '../../_services/authentication.service';
import {hasRight} from '../../_helper/user-util';
import {PersonBankDetailsComponent} from '../_forms/person-bank-details/person-bank-details.component';
import {PersonConnectionsComponent} from "../_forms/person-connections/person-connections.component";

@Component({
    selector: 'app-person-detail',
    templateUrl: './person-detail.component.html',
    styleUrls: ['./person-detail.component.scss']
})
export class PersonDetailComponent implements OnInit {
    user: UserDTO;
    @Input() personId;
    personForm: FormGroup;
    person: PersonDto;

    @ViewChild('personContactsComponent', {static: false}) personContactsComponent: PersonContactsComponent;
    personContactsLoaded = false;

    @ViewChild('personPropertiesComponent', {static: false}) personPropertiesComponent: PersonPropertiesComponent;
    personPropertiesLoaded = false;

    @ViewChild('personBankDetailsComponent', {static: false}) personBankDetailsComponent: PersonBankDetailsComponent;
    personBankDetailsLoaded = false;

    @ViewChild('personConnectionsComponent', {static: false}) personConnectionsComponent: PersonConnectionsComponent;
    personConnectionsLoaded = false;

    constructor(private globalService: GlobalService,
                private activatedRoute: ActivatedRoute,
                private apiService: ApiService,
                private fb: FormBuilder,
                private authenticationService: AuthenticationService) {
    }

    ngOnInit(): void {
        this.user = decode(localStorage.getItem(this.authenticationService.LOCAL_STORAGE_KEY));

        this.personForm = this.fb.group({
            firstName: new FormControl('', [Validators.required]),
            lastName: new FormControl('', [Validators.required]),
            personalNo: new FormControl('', [Validators.required, Validators.minLength(11), Validators.maxLength(11)]),
            birthDate: new FormControl(),
            midName: new FormControl(),
            address: new FormControl(),
            addressFact: new FormControl(),
        });

        if (!this.personId) {
            this.activatedRoute.params.subscribe(params => {
                this.personId = params.personId;
                this.getPerson();
            });
        } else {
            this.getPerson();
        }
    }

    getPerson() {
        this.globalService.onBlockUI(true);
        this.apiService.getPerson(this.personId).subscribe(response => {
            this.person = response;
            this.personForm.setValue({
                firstName: response.firstName,
                lastName: response.lastName,
                personalNo: response.personalNo,
                birthDate: response.birthDate ? new Date(response.birthDate) : null,
                midName: response.midName,
                address: response.address,
                addressFact: response.addressFact,
            });
            this.globalService.onBlockUI(false);
        });
    }

    onTabChange(event) {
        if (event.index === 1) {
            if (!this.personContactsLoaded) {
                this.personContactsComponent.setPersonId(this.personId);
                this.personContactsComponent.loadPersonContacts();
                this.personContactsLoaded = true;
            }
        } else if (event.index === 2) {
            if (!this.personPropertiesLoaded) {
                this.personPropertiesComponent.setPersonId(this.personId);
                this.personPropertiesComponent.getPersonProperties();
                this.personPropertiesLoaded = true;
            }
        } else if (event.index === 3) {
            if (!this.personBankDetailsLoaded) {
                this.personBankDetailsComponent.setPersonId(this.personId);
                this.personBankDetailsComponent.getPersonBankDetails();
                this.personBankDetailsLoaded = true;
            }
        } else if (event.index === 4) {
            if (!this.personConnectionsLoaded) {
                this.personConnectionsComponent.setPersonId(this.personId);
                this.personConnectionsComponent.getPersonConnections();
                this.personConnectionsLoaded = true;
            }
        }
    }

    hasRight(right) {
        return hasRight(this.user, right);
    }

    updatePerson() {
        const valid = this.checkValidity(this.personForm);
        if (!valid) {
            this.globalService.showError('შეავსეთ აუცილებელი ველები');
            return;
        }
        this.globalService.onBlockUI(true);
        const formValues = this.personForm.getRawValue();
        const request = {
            firstName: formValues.firstName,
            lastName: formValues.lastName,
            personalNo: formValues.personalNo,
            midName: formValues.midName,
            address: formValues.address,
            addressFact: formValues.addressFact,
            birthDate: formValues.birthDate,
            personId: this.personId,
        };

        this.apiService.updatePerson(request).subscribe(response => {
            this.globalService.onBlockUI(false);
            this.globalService.showSuccess('პიროვნება წარმატებით დარედაქტირდა');
        }, error => {
            this.globalService.onBlockUI(false);
            this.globalService.showError(error.error.message);
        });
    }

    public checkValidity(form): boolean {
        let valid = true;
        Object.keys(form.controls).forEach((key) => {
            form.controls[key].markAsDirty();
            if (valid) {
                valid = form.controls[key].valid;
            }
        });
        return valid;
    }
}
