import {Component, OnInit, ViewChild} from '@angular/core';
import {Table} from 'primeng/table';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {ConfirmationService, LazyLoadEvent} from 'primeng/api';
import {resetTimeToDateToISOString} from '../../_helper/dates/date-util';
import {ApiService} from '../../_services/api.service';
import {GlobalService} from '../../_services/global.service';
import {ActivatedRoute, Router} from '@angular/router';
import {DateFormatPipe} from '../../_helper/dates/pipe/date-format.pipe';
import {AuthenticationService} from '../../_services/authentication.service';
import {getLibFromLibsById} from '../../_helper/utils';
import {DebtStates} from '../../_libs/debtStates';

@Component({
    selector: 'app-bonuses',
    templateUrl: './bonuses.component.html',
    styleUrls: ['./bonuses.component.scss']
})
export class BonusesComponent implements OnInit {
    @ViewChild('table', {static: false}) table: Table;
    searchForm: FormGroup;

    debts: any = {
        content: [],
        totalElements: 0,
        totalPages: 0,
        size: 0,
    };

    searchRequest: any = {
        page: 0,
        size: 10,
        expertId: '',
        expert: '',
        debtId: '',
        startDate: '',
        endDate: '',
    };

    displaySearchUserForSearch: boolean;

    statesOptions = DebtStates;

    constructor(private apiService: ApiService,
                private globalService: GlobalService,
                private fb: FormBuilder,
                private router: Router,
                private activatedRoute: ActivatedRoute,
                private dateFormatPipe: DateFormatPipe,
                private authenticationService: AuthenticationService,
                private confirmationService: ConfirmationService) {
    }

    ngOnInit(): void {
        this.searchForm = this.fb.group({
            startDate: new FormControl(),
            endDate: new FormControl(),
            expert: new FormControl(),
            expertId: new FormControl(),
            debtId: new FormControl(),
        });

        this.activatedRoute.queryParams.subscribe(params => {

            if (params.expertId) {
                this.searchRequest.expertId = params.expertId;
                this.searchForm.patchValue({
                    expertId: params.expertId
                });
            }
            if (params.expert) {
                this.searchRequest.expert = params.expert;
                this.searchForm.patchValue({
                    expert: params.expert
                });
            }
            if (params.debtId) {
                this.searchRequest.debtId = params.debtId;
                this.searchForm.patchValue({
                    debtId: params.debtId
                });
            }

            if (params.debtDateTo) {
                this.searchRequest.startDate = params.startDate;
                this.searchForm.patchValue({
                    startDate: new Date(params.startDate)
                });
            }

            if (params.endDate) {
                this.searchRequest.startDate = params.endDate;
                this.searchForm.patchValue({
                    endDate: new Date(params.endDate)
                });
            }

            if (params.page) {
                this.searchRequest.page = params.page;
            }

            if (params.size) {
                this.searchRequest.size = params.size;
            }

            this.search(null, this.searchRequest);
        });
    }

    search(event: LazyLoadEvent, req) {
        this.globalService.onBlockUI(true);
        const formValues = this.searchForm.getRawValue();
        if (event == null) {
            this.table._first = this.searchRequest.page * this.searchRequest.size;
            if (req == null) {
                this.table._first = 0;
                this.searchRequest.page = 0;

                this.searchRequest.expertId = formValues.expertId ? formValues.expertId : '';
                this.searchRequest.expert = formValues.expert ? formValues.expert : '';
                this.searchRequest.debtId = formValues.debtId ? formValues.debtId : '';

                this.searchRequest.startDate = resetTimeToDateToISOString(formValues.startDate);
                this.searchRequest.endDate = resetTimeToDateToISOString(formValues.endDate);
            }
        } else {
            this.searchRequest.page = (event.first / this.searchRequest.size);
        }

        const params = {
            page: this.searchRequest.page,
            size: this.searchRequest.size,
            startDate: this.searchRequest.startDate,
            endDate: this.searchRequest.endDate,
            expertId: this.searchRequest.expertId,
            expert: this.searchRequest.expert,
            debtId: this.searchRequest.debtId,
        };
        this.globalService.createUrl(['/main/bonuses'], params);

        this.apiService.bonusSearch(this.searchRequest).subscribe(response => {
            this.debts = response;
            this.globalService.onBlockUI(false);
        }, error => {
            this.debts = {
                content: [],
                totalElements: 0,
                totalPages: 0,
                size: 10,
            };
            this.globalService.onBlockUI(false);
            this.globalService.showError(error.error.message);
        });
    }

    resetSearchForm() {
        this.searchForm.reset();
    }

    openSearchUserForSearch() {
        this.displaySearchUserForSearch = true;
    }

    selectUserForSearch(event) {
        this.displaySearchUserForSearch = false;
        this.searchForm.patchValue({
            expert: event.firstName + ' ' + event.lastName,
            expertId: event.userId
        });
    }

    openDebt(debt) {
        this.router.navigate(['/main/debt/' + debt.id]);
    }

    openPerson(personId) {
        this.router.navigate(['/main/person/' + personId]);
    }

    openOrganization(organizationId) {
        this.router.navigate(['/main/organization/' + organizationId]);
    }

    getLibFromLibs(libId, libs) {
        return getLibFromLibsById(libId, libs);
    }
}
