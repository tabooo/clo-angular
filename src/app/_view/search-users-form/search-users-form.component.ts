import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {LazyLoadEvent} from 'primeng/api';
import {ApiService} from '../../_services/api.service';
import {GlobalService} from '../../_services/global.service';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {Table} from 'primeng/table';
import {getLibFromLibsById} from "../../_helper/utils";

@Component({
    selector: 'app-search-users-form',
    templateUrl: './search-users-form.component.html',
    styleUrls: ['./search-users-form.component.scss']
})
export class SearchUsersFormComponent implements OnInit {
    @Input() roleId;
    @Output() onSelectUser: EventEmitter<any> = new EventEmitter<any>();
    blockedPanel = false;

    @ViewChild('table', {static: false}) table: Table;
    users: any = [];
    searchForm: FormGroup;
    roles = [];

    constructor(private apiService: ApiService,
                private globalService: GlobalService,
                private fb: FormBuilder) {
    }

    ngOnInit(): void {
        this.searchForm = this.fb.group({
            roleId: new FormControl(),
        });

        this.blockedPanel = true;
        this.apiService.getRoles().subscribe(response => {
            this.roles = response;
            this.blockedPanel = false;
            if (this.roleId) {
                this.searchForm.patchValue({roleId: getLibFromLibsById(this.roleId, this.roles)});
                this.search(null, null);
            }
        }, error => {
            this.blockedPanel = false;
            this.globalService.showError(error.error.message);
        });
    }

    search(event: LazyLoadEvent, req) {
        const formValues = this.searchForm.getRawValue();
        if (!formValues.roleId) {
            this.globalService.showError('აირჩიეთ როლი');
            return;
        }

        const request = {
            roleId: formValues.roleId.id
        };

        this.blockedPanel = true;
        this.apiService.getRoleUsers(request).subscribe(response => {
            this.users = response;
            this.blockedPanel = false;
        }, error => {
            this.blockedPanel = false;
            this.globalService.showError(error.error.message);
        });
    }

    selectUser(user) {
        this.onSelectUser.emit(user);
    }
}
