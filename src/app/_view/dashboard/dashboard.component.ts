import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../_services/api.service';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

    constructor(private apiService: ApiService) {
    }

    ngOnInit(): void {
        /*this.apiService.test().subscribe(response => {
            console.log(response);
        });*/
    }

}
