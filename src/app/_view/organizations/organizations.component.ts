import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Table} from 'primeng/table';
import {ApiService} from '../../_services/api.service';
import {GlobalService} from '../../_services/global.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {LazyLoadEvent} from 'primeng/api';
import {OrganizationDto} from '../../_model/OrganizationDto';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-organizations',
    templateUrl: './organizations.component.html',
    styleUrls: ['./organizations.component.scss']
})
export class OrganizationsComponent implements OnInit, AfterViewInit {
    @ViewChild('table', {static: false}) table: Table;
    searchForm: FormGroup;
    organizations: any = {count: 0, organizations: []};
    searchRequest: any = {
        firstResult: 0,
        limit: 10,
        organizationCode: '',
        organizationName: '',
        isLoaner: ''
    };
    displayOrganizationModal: boolean;
    curOrganization: OrganizationDto;
    personAddForm: FormGroup;
    blockedPanel = false;

    constructor(private apiService: ApiService,
                private globalService: GlobalService,
                private fb: FormBuilder,
                private router: Router,
                private activatedRoute: ActivatedRoute) {

    }

    ngOnInit(): void {
        this.searchForm = this.fb.group({
            organizationCode: new FormControl(),
            organizationName: new FormControl(),
            isLoaner: new FormControl(),
        });

        this.personAddForm = this.fb.group({
            organizationCode: new FormControl('', [Validators.required]),
            organizationName: new FormControl('', [Validators.required]),
            organizationNameEn: new FormControl(),
            address: new FormControl(),
            addressFact: new FormControl(),
            isLoaner: new FormControl(),
        });

        this.activatedRoute.queryParams.subscribe(params => {
            if (params.organizationCode) {
                this.searchRequest.organizationCode = params.organizationCode;
                this.searchForm.patchValue({
                    organizationCode: params.organizationCode
                });
            }
            if (params.organizationName) {
                this.searchRequest.organizationName = params.organizationName;
                this.searchForm.patchValue({
                    organizationName: params.organizationName
                });
            }
            if (params.isLoaner) {
                this.searchRequest.isLoaner = params.isLoaner;
                this.searchForm.patchValue({
                    isLoaner: params.isLoaner
                });
            }
            if (params.firstResult) {
                this.searchRequest.firstResult = params.firstResult;
            }
            if (params.limit) {
                this.searchRequest.limit = params.limit;
            }
        });
    }

    ngAfterViewInit() {
        this.search(null, this.searchRequest);
    }

    search(event: LazyLoadEvent, req) {
        const formValues = this.searchForm.getRawValue();
        if (event == null) {
            this.table._first = this.searchRequest.firstResult;
            if (req == null) {
                this.table._first = 0;
                this.searchRequest.firstResult = 0;

                this.searchRequest.organizationCode = formValues.organizationCode ? formValues.organizationCode : '';
                this.searchRequest.organizationName = formValues.organizationName ? formValues.organizationName : '';
                this.searchRequest.isLoaner = formValues.isLoaner ? formValues.isLoaner : '';
            }
        } else {
            this.searchRequest.firstResult = event.first;
        }

        const params = {
            firstResult: this.searchRequest.firstResult,
            limit: this.searchRequest.limit,
            organizationCode: this.searchRequest.organizationCode,
            organizationName: this.searchRequest.organizationName,
            isLoaner: this.searchRequest.isLoaner
        };
        this.globalService.createUrl(['/main/organizations'], params);

        this.globalService.onBlockUI(true);
        this.apiService.searchOrganizations(this.searchRequest).subscribe(response => {
            this.organizations = response;
            this.globalService.onBlockUI(false);
        }, error => {
            this.globalService.onBlockUI(false);
            this.globalService.showError(error.error.message);
        });
    }

    addNewOrganization(organization) {
        if (organization) {
            this.curOrganization = organization;
        }
        this.displayOrganizationModal = true;
    }

    saveOrganization() {
        const valid = this.checkValidity();
        if (!valid) {
            this.globalService.showError('შეავსეთ აუცილებელი ველები');
            return;
        }

        this.globalService.onBlockUI(true);
        this.blockedPanel = true;
        const formValues = this.personAddForm.getRawValue();
        const request = {
            personTypeId: 2,
            organization: {
                organizationCode: formValues.organizationCode,
                organizationName: formValues.organizationName,
                organizationNameEn: formValues.organizationNameEn,
                isLoaner: formValues.isLoaner,
                address: formValues.address,
                addressFact: formValues.addressFact,
            }
        };

        this.apiService.savePerson(request).subscribe(response => {
            this.displayOrganizationModal = false;
            this.globalService.onBlockUI(false);
            this.blockedPanel = false;
            this.globalService.showSuccess('პიროვნება წარმატებით დაემატა');
            this.search(null, this.searchRequest);
        }, error => {
            this.globalService.onBlockUI(false);
            this.blockedPanel = false;
            this.globalService.showError(error.error.message);
        });
    }

    openOrganization(organization: any) {
        this.router.navigate(['/main/organization/' + organization.organizationId]);
    }

    public checkValidity(): boolean {
        let valid = true;
        Object.keys(this.personAddForm.controls).forEach((key) => {
            this.personAddForm.controls[key].markAsDirty();
            if (valid) {
                valid = this.personAddForm.controls[key].valid;
            }
        });
        return valid;
    }
}
