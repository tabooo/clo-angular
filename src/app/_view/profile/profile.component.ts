import {Component, OnInit} from '@angular/core';
import {UserDTO} from '../../_model/UserDTO';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {AuthenticationService} from '../../_services/authentication.service';
import {decode} from '../../_helper/json-util';
import {ApiService} from "../../_services/api.service";
import {GlobalService} from "../../_services/global.service";

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
    user: UserDTO;
    profileForm: FormGroup;

    displayresetPasswordModal = false;
    resetPasswordForm: FormGroup;
    blockedPanel3 = false;

    constructor(private authenticationService: AuthenticationService,
                private fb: FormBuilder,
                private apiService: ApiService,
                private globalService: GlobalService) {
    }

    ngOnInit(): void {
        this.profileForm = this.fb.group({
            firstName: new FormControl(),
            lastName: new FormControl(),
            personalNo: new FormControl(),
            mobileNo: new FormControl(),
            email: new FormControl(),
            address: new FormControl(),
            accountNumber: new FormControl(),
            userName: new FormControl(),
            birthDate: new FormControl(),
        });

        this.resetPasswordForm = this.fb.group({
            oldPassword: new FormControl(),
            password: new FormControl(),
            repeatPassword: new FormControl(),
        });

        this.user = decode(localStorage.getItem(this.authenticationService.LOCAL_STORAGE_KEY));

        this.profileForm.setValue({
            firstName: this.user.firstName,
            lastName: this.user.lastName,
            personalNo: this.user.personalNo,
            mobileNo: this.user.mobileNo,
            email: this.user.email,
            address: this.user.address,
            accountNumber: this.user.accountNumber,
            userName: this.user.userName,
            birthDate: this.user.birthDate
        });
    }

    openPasswordChangeModal() {
        this.displayresetPasswordModal = true;
    }

    changePassword() {
        this.blockedPanel3 = true;
        const formValues = this.resetPasswordForm.getRawValue();
        const request = {
            password: formValues.password,
            repeatPassword: formValues.repeatPassword,
            oldPassword: formValues.oldPassword,
        };
        this.apiService.changePassword(request).subscribe(response => {
            this.blockedPanel3 = false;
            this.displayresetPasswordModal = false;
            this.globalService.showSuccess('პაროლი წარმატებით შეიცვალა');
        }, error => {
            this.globalService.showError(error.message);
            this.blockedPanel3 = false;
        });
    }
}
