import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Table} from 'primeng/table';
import {ApiService} from '../../_services/api.service';
import {GlobalService} from '../../_services/global.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {LazyLoadEvent} from 'primeng/api';
import {PersonDto} from '../../_model/PersonDto';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-persons',
    templateUrl: './persons.component.html',
    styleUrls: ['./persons.component.scss']
})
export class PersonsComponent implements OnInit, AfterViewInit {
    @ViewChild('table', {static: false}) table: Table;
    persons: any = {count: 0, persons: []};
    searchRequest: any = {
        firstResult: 0,
        limit: 10,
        personalNo: '',
        firstName: '',
        lastName: '',
    };

    displayPersonModal: boolean;
    blockedPanel = false;
    curPerson: PersonDto;
    personAddForm: FormGroup;
    searchForm: FormGroup;


    constructor(private apiService: ApiService,
                private globalService: GlobalService,
                private fb: FormBuilder,
                private router: Router,
                private activatedRoute: ActivatedRoute) {
    }

    ngOnInit(): void {
        this.searchForm = this.fb.group({
            personalNo: new FormControl(),
            firstName: new FormControl(),
            lastName: new FormControl(),
        });

        this.personAddForm = this.fb.group({
            firstName: new FormControl('', [Validators.required]),
            lastName: new FormControl('', [Validators.required]),
            personalNo: new FormControl('', [Validators.required, Validators.minLength(11), Validators.maxLength(11)]),
            birthDate: new FormControl(),
            midName: new FormControl(),
            address: new FormControl(),
            addressFact: new FormControl(),
        });

        this.activatedRoute.queryParams.subscribe(params => {
            if (params.personalNo) {
                this.searchRequest.personalNo = params.personalNo;
                this.searchForm.patchValue({
                    personalNo: params.personalNo
                });
            }
            if (params.firstName) {
                this.searchRequest.firstName = params.firstName;
                this.searchForm.patchValue({
                    firstName: params.firstName
                });
            }
            if (params.lastName) {
                this.searchRequest.lastName = params.lastName;
                this.searchForm.patchValue({
                    lastName: params.lastName
                });
            }
            if (params.firstResult) {
                this.searchRequest.firstResult = params.firstResult;
            }
            if (params.limit) {
                this.searchRequest.limit = params.limit;
            }
        });
    }

    ngAfterViewInit() {
        this.search(null, this.searchRequest);
    }

    search(event: LazyLoadEvent, req) {
        const formValues = this.searchForm.getRawValue();
        if (event == null) {
            this.table._first = this.searchRequest.firstResult;
            if (req == null) {
                this.table._first = 0;
                this.searchRequest.firstResult = 0;

                this.searchRequest.personalNo = formValues.personalNo ? formValues.personalNo : '';
                this.searchRequest.firstName = formValues.firstName ? formValues.firstName : '';
                this.searchRequest.lastName = formValues.lastName ? formValues.lastName : '';
            }
        } else {
            this.searchRequest.firstResult = event.first;
        }

        const params = {
            firstResult: this.searchRequest.firstResult,
            limit: this.searchRequest.limit,
            personalNo: this.searchRequest.personalNo,
            firstName: this.searchRequest.firstName,
            lastName: this.searchRequest.lastName
        };
        this.globalService.createUrl(['/main/persons'], params);

        this.globalService.onBlockUI(true);
        this.apiService.searchPersons(this.searchRequest).subscribe(response => {
            this.persons = response;
            this.globalService.onBlockUI(false);
        }, error => {
            this.globalService.onBlockUI(false);
            this.globalService.showError(error.error.message);
        });
    }

    addNewPerson(person) {
        if (person) {
            this.curPerson = person;
            this.personAddForm.setValue({
                firstName: person.firstName,
                lastName: person.lastName,
                personalNo: person.personalNo,
                birthDate: person.birthDate ? new Date(person.birthDate) : null,
                midName: person.midName,
                address: person.address,
                addressFact: person.addressFact,
            });
        }
        this.displayPersonModal = true;
    }

    savePerson() {
        const valid = this.checkValidity();
        if (!valid) {
            this.globalService.showError('შეავსეთ აუცილებელი ველები');
            return;
        }

        this.globalService.onBlockUI(true);
        this.blockedPanel = true;
        const formValues = this.personAddForm.getRawValue();
        const request = {
            personTypeId: 1,
            person: {
                firstName: formValues.firstName,
                lastName: formValues.lastName,
                personalNo: formValues.personalNo,
                midName: formValues.midName,
                address: formValues.address,
                addressFact: formValues.addressFact,
                birthDate: formValues.birthDate,
            }
        };

        this.apiService.savePerson(request).subscribe(response => {
            this.displayPersonModal = false;
            this.globalService.onBlockUI(false);
            this.blockedPanel = false;
            this.globalService.showSuccess('პიროვნება წარმატებით დაემატა');
            this.search(null, this.searchRequest);
            this.personAddForm.reset();
        }, error => {
            this.globalService.onBlockUI(false);
            this.blockedPanel = false;
            this.globalService.showError(error.error.message);
        });
    }

    openPerson(person: any) {
        this.router.navigate(['/main/person/' + person.personId]);
    }

    public checkValidity(): boolean {
        let valid = true;
        Object.keys(this.personAddForm.controls).forEach((key) => {
            this.personAddForm.controls[key].markAsDirty();
            if (valid) {
                valid = this.personAddForm.controls[key].valid;
            }
        });
        return valid;
    }
}
