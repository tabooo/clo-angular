import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {Table} from 'primeng/table';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {ApiService} from '../../_services/api.service';
import {GlobalService} from '../../_services/global.service';
import {LazyLoadEvent} from 'primeng/api';

@Component({
    selector: 'app-search-persons-form',
    templateUrl: './search-persons-form.component.html',
    styleUrls: ['./search-persons-form.component.scss']
})
export class SearchPersonsFormComponent implements OnInit {
    @Input() personType = 1;
    @Output() onSelectPerson: EventEmitter<any> = new EventEmitter<any>();
    index = 0;
    blockedPanel = false;

    @ViewChild('tablePersons', {static: false}) tablePersons: Table;
    persons: any = {count: 0, persons: []};
    searchRequestPersons: any = {
        firstResult: 0,
        limit: 10,
        personalNo: ''
    };
    searchFormPersons: FormGroup;

    @ViewChild('tableOrganizations', {static: false}) tableOrganizations: Table;
    organizations: any = {count: 0, organizations: []};
    searchRequestOrganizations: any = {
        firstResult: 0,
        limit: 10,
        personalNo: ''
    };
    searchFormOrganizations: FormGroup;

    constructor(private apiService: ApiService,
                private globalService: GlobalService,
                private fb: FormBuilder) {
    }

    ngOnInit(): void {
        this.searchFormPersons = this.fb.group({
            personalNo: new FormControl(),
            firstName: new FormControl(),
            lastName: new FormControl(),
        });

        this.searchFormOrganizations = this.fb.group({
            organizationCode: new FormControl(),
            organizationName: new FormControl(),
            isLoaner: new FormControl(),
        });

        if (this.personType === 1) {
            this.index = 0;
        } else {
            this.index = 1;
        }
    }

    searchPersons(event: LazyLoadEvent, req) {
        const formValues = this.searchFormPersons.getRawValue();
        this.personType = 1;
        if (event == null) {
            if (req == null) {
                this.tablePersons._first = 0;
                this.searchRequestPersons.firstResult = 0;

                this.searchRequestPersons.personalNo = formValues.personalNo ? formValues.personalNo : '';
                this.searchRequestPersons.firstName = formValues.firstName ? formValues.firstName : '';
                this.searchRequestPersons.lastName = formValues.lastName ? formValues.lastName : '';
            }
        } else {
            this.searchRequestPersons.firstResult = event.first;
        }
        this.blockedPanel = true;
        this.apiService.searchPersons(this.searchRequestPersons).subscribe(response => {
            this.persons = response;
            this.blockedPanel = false;
        }, error => {
            this.blockedPanel = false;
            this.globalService.showError(error.error.message);
        });
    }

    searchOrganizations(event: LazyLoadEvent, req) {
        const formValues = this.searchFormOrganizations.getRawValue();
        this.personType = 2;
        if (event == null) {
            if (req == null) {
                this.tableOrganizations._first = 0;
                this.searchRequestOrganizations.firstResult = 0;

                this.searchRequestOrganizations.organizationCode = formValues.organizationCode ? formValues.organizationCode : '';
                this.searchRequestOrganizations.organizationName = formValues.organizationName ? formValues.organizationName : '';
                this.searchRequestOrganizations.isLoaner = formValues.isLoaner ? formValues.isLoaner : '';
            }
        } else {
            this.searchRequestOrganizations.firstResult = event.first;
        }
        this.blockedPanel = true;
        this.apiService.searchOrganizations(this.searchRequestOrganizations).subscribe(response => {
            this.organizations = response;
            this.blockedPanel = false;
        }, error => {
            this.blockedPanel = false;
            this.globalService.showError(error.error.message);
        });
    }

    setPersonType(personType) {
        this.personType = personType;
        if (this.personType === 1) {
            this.index = 0;
        } else if (this.personType === 2) {
            this.index = 1;
        }
    }

    selectPerson(person, personType) {
        this.onSelectPerson.emit({person, personType: this.personType});
    }

}
