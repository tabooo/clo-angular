import {Component, OnInit, ViewChild} from '@angular/core';
import {Table} from 'primeng/table';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {ApiService} from '../../_services/api.service';
import {GlobalService} from '../../_services/global.service';
import {ConfirmationService, LazyLoadEvent} from 'primeng/api';
import {DebtDto} from '../../_model/DebtDto';
import {ActivatedRoute, Router} from '@angular/router';
import {DebtComplexityTypeDto} from '../../_model/DebtComplexityTypeDto';
import {DateFormatPipe} from '../../_helper/dates/pipe/date-format.pipe';
import {AuthenticationService} from '../../_services/authentication.service';
import {decode} from '../../_helper/json-util';
import {UserDTO} from '../../_model/UserDTO';
import {hasRight} from '../../_helper/user-util';
import {getLibFromLibsById} from '../../_helper/utils';
import {LibDto} from '../../_model/LibDto';
import {FileUpload} from 'primeng/fileupload';
import {SearchPersonsFormComponent} from '../search-persons-form/search-persons-form.component';
import {resetTimeToDateToISOString} from '../../_helper/dates/date-util';
import {DebtStates} from '../../_libs/debtStates';

@Component({
    selector: 'app-debts',
    templateUrl: './debts.component.html',
    styleUrls: ['./debts.component.scss'],
    styles: [`
        :host ::ng-deep .row-danger {
            background-color: #FFCDD2 !important;
            color: #73000c;
            font-weight: bold;
        }
    `
    ]
})
export class DebtsComponent implements OnInit {
    user: UserDTO;
    @ViewChild('table', {static: false}) table: Table;
    searchForm: FormGroup;
    libs = 3;
    debts: any = {count: 0, debts: [], amount: 0};
    searchRequest: any = {
        firstResult: 0,
        limit: 10,
        borrowerPersonId: '',
        loanPersonId: '',
        debtDateFrom: '',
        commentDateFrom: '',
        commentDateTo: '',
        promiseDateFrom: '',
        promiseDateTo: '',
        nextContactDateFrom: '',
        nextContactDateTo: '',
        assignDateFrom: '',
        assignDateTo: '',
        statuteOfLimitationDateFrom: '',
        statuteOfLimitationDateTo: '',
        userId: '',
        debtComplexityTypeId: '',
        scheduleStatusId: '',
        unWorkedDebt: '',
        amountFrom: '',
        amountTo: '',
        debtOwner: '',
        refusesToPay: '',
        notInContact: '',
        fullyPaid: false,
        debtToConfirmPaid: false,
        states: '',
        debtId: '',
        lawyerStatusId: '',
    };
    curDebt: DebtDto;
    complexityTypes: DebtComplexityTypeDto[];
    scheduleStatuses: LibDto[];
    unWorkedDebtOptions: LibDto[] = [
        {
            id: 1,
            name: 'დამუშავებული'
        },
        {
            id: 2,
            name: 'დაუმუშავებელი'
        }
    ];
    debtOwnerOptions: LibDto[] = [
        {
            id: 1,
            name: 'ადმინისტრაციაზე'
        },
        {
            id: 2,
            name: 'იურისტზე'
        },
        {
            id: 3,
            name: 'კრედიტ ოფიცერზე'
        }
    ];
    assigneeDebtRequestOptions: LibDto[] = [
        {
            id: 1,
            name: 'ადმინისტრაციაზე'
        },
        {
            id: 2,
            name: 'იურისტზე'
        }
    ];
    refusesToPayOptions: any[] = [
        {
            id: true,
            name: 'კი'
        },
        {
            id: false,
            name: 'არა'
        }
    ];
    notInContactOptions: any[] = [
        {
            id: true,
            name: 'კი'
        },
        {
            id: false,
            name: 'არა'
        }
    ];
    statesOptions = DebtStates;
    debtLawyerStatuses: LibDto[] = [];

    selectedDebts: DebtDto[] = [];
    displaySearchUser: boolean;
    displaySearchUserForSearch: boolean;

    now;
    fullyPaid = false;
    removed = false;
    debtToConfirmPaid = false;
    scheduleStatusId = null;
    assigneeDebtRequestTypeId = null;

    displayExcelImport = false;
    blockedPanelExcelImport = false;
    uploadedFiles: any[] = [];

    displaySearchModal = false;
    importExcelForm: FormGroup;
    lastSearchPersonField: any;

    @ViewChild('SearchPersonsFormComponent', {static: false}) appSearchPersonsForm: SearchPersonsFormComponent;
    actionButtonItems: any;

    changeStateValue: any;
    displayChangeStateModal = false;
    blockedPanelChangeState = false;

    trClass = {};

    constructor(private apiService: ApiService,
                private globalService: GlobalService,
                private fb: FormBuilder,
                private router: Router,
                private activatedRoute: ActivatedRoute,
                private dateFormatPipe: DateFormatPipe,
                private authenticationService: AuthenticationService,
                private confirmationService: ConfirmationService) {
        if (activatedRoute.snapshot.url[1] && activatedRoute.snapshot.url[1].path === 'fullyPaid') {
            this.fullyPaid = true;
        } else {
            this.fullyPaid = false;
        }
        if (activatedRoute.snapshot.url[1] && activatedRoute.snapshot.url[1].path === 'debtToConfirmPaid') {
            this.debtToConfirmPaid = true;
        } else {
            this.debtToConfirmPaid = false;
        }
        if (activatedRoute.snapshot.url[1] && activatedRoute.snapshot.url[1].path === 'assigneeDebtRequest') {
            this.assigneeDebtRequestTypeId = activatedRoute.snapshot.url[2].path;
        } else {
            this.assigneeDebtRequestTypeId = null;
        }
        if (activatedRoute.snapshot.url[1] && activatedRoute.snapshot.url[1].path === 'removed') {
            this.removed = true;
        } else {
            this.removed = false;
        }
        if (activatedRoute.snapshot.url[1] && activatedRoute.snapshot.url[1].path === 'confirmedSchedules') {
            this.scheduleStatusId = 2;
        }
        if (activatedRoute.snapshot.url[1] && activatedRoute.snapshot.url[1].path === 'rejectSchedules') {
            this.scheduleStatusId = 3;
        }
    }

    ngOnInit(): void {
        this.user = decode(localStorage.getItem(this.authenticationService.LOCAL_STORAGE_KEY));

        this.searchForm = this.fb.group({
            debtDateFrom: new FormControl(),
            debtDateTo: new FormControl(),
            borrowerPerson: new FormControl(),
            borrowerPersonId: new FormControl(),
            loanPerson: new FormControl(),
            loanPersonId: new FormControl(),
            userId: new FormControl(),
            user: new FormControl(),
            complexityType: new FormControl(),
            commentDateFrom: new FormControl(),
            commentDateTo: new FormControl(),
            promiseDateFrom: new FormControl(),
            promiseDateTo: new FormControl(),
            nextContactDateFrom: new FormControl(),
            nextContactDateTo: new FormControl(),
            assignDateFrom: new FormControl(),
            assignDateTo: new FormControl(),
            debtComplexityType: new FormControl(),
            scheduleStatus: new FormControl(),
            unWorkedDebt: new FormControl(),
            debtOwner: new FormControl(),
            refusesToPay: new FormControl(),
            notInContact: new FormControl(),
            fullyPaid: new FormControl(),
            debtToConfirmPaid: new FormControl(),
            assigneeDebtRequestTypeId: new FormControl(),
            lawyerStatus: new FormControl(),
            amountFrom: new FormControl(),
            amountTo: new FormControl(),
            statuteOfLimitationDateFrom: new FormControl(),
            statuteOfLimitationDateTo: new FormControl(),
            debtId: new FormControl()
        });

        this.importExcelForm = this.fb.group({
            importExcelOrganizationName: new FormControl(),
            importExcelOrganizationId: new FormControl(),
        });

        if (hasRight(this.user, 'ROLE_მხოლოდ საკუთარი ვალების ნახვა')) {
            this.searchForm.patchValue({
                userId: this.user.userId,
                user: this.user.firstName + ' ' + this.user.lastName
            });
            this.searchRequest.userId = this.user.userId;
        }

        this.globalService.onBlockUI(true);
        this.apiService.getDebtComplexityTypes().subscribe(response => {
            this.complexityTypes = response;
            this.reloadLib();
        });

        this.globalService.onBlockUI(true);
        this.apiService.getDebtLawyerStatus().subscribe(response => {
            this.debtLawyerStatuses = response;
            this.reloadLib();
        });

        this.apiService.getDebtScheduleStatuses().subscribe(response => {
            this.scheduleStatuses = response;
            this.reloadLib();
        });
    }

    reloadLib() {
        this.libs -= 1;
        if (this.libs === 0) {
            if (this.assigneeDebtRequestTypeId != null) {
                this.searchRequest.assigneeDebtRequestTypeId = this.assigneeDebtRequestTypeId;
                this.searchForm.patchValue({
                    assigneeDebtRequestTypeId: getLibFromLibsById(this.assigneeDebtRequestTypeId, this.assigneeDebtRequestOptions)
                });
            }

            if (this.scheduleStatusId != null) {
                this.searchRequest.scheduleStatusId = this.scheduleStatusId;
                this.searchForm.patchValue({
                    scheduleStatus: getLibFromLibsById(this.scheduleStatusId, this.scheduleStatuses)
                });
            }

            this.activatedRoute.queryParams.subscribe(params => {

                if (params.borrowerPersonId) {
                    this.searchRequest.borrowerPersonId = params.borrowerPersonId;
                }
                if (params.borrowerPerson) {
                    this.searchRequest.borrowerPerson = params.borrowerPerson;
                    this.searchForm.patchValue({
                        borrowerPerson: params.borrowerPerson
                    });
                }
                if (params.loanPersonId) {
                    this.searchRequest.loanPersonId = params.loanPersonId;
                }
                if (params.loanPerson) {
                    this.searchRequest.loanPerson = params.loanPerson;
                    this.searchForm.patchValue({
                        loanPerson: params.loanPerson
                    });
                }
                if (params.userId) {
                    this.searchRequest.userId = params.userId;
                }
                if (params.user) {
                    this.searchRequest.user = params.user;
                    this.searchForm.patchValue({
                        user: params.user
                    });
                }
                if (params.debtComplexityTypeId) {
                    this.searchRequest.debtComplexityTypeId = params.debtComplexityTypeId;
                    this.searchForm.patchValue({
                        debtComplexityType: getLibFromLibsById(this.searchRequest.debtComplexityTypeId, this.complexityTypes)
                    });
                }
                if (params.lawyerStatusId) {
                    this.searchRequest.lawyerStatusId = params.lawyerStatusId;
                    this.searchForm.patchValue({
                        lawyerStatus: getLibFromLibsById(this.searchRequest.lawyerStatusId, this.debtLawyerStatuses)
                    });
                }
                if (params.scheduleStatusId) {
                    this.searchRequest.scheduleStatusId = params.scheduleStatusId;
                    this.searchForm.patchValue({
                        scheduleStatus: getLibFromLibsById(this.searchRequest.scheduleStatusId, this.scheduleStatuses)
                    });
                }
                if (params.unWorkedDebt) {
                    this.searchRequest.unWorkedDebt = params.unWorkedDebt;
                    this.searchForm.patchValue({
                        unWorkedDebt: getLibFromLibsById(this.searchRequest.unWorkedDebt, this.unWorkedDebtOptions)
                    });
                }
                if (params.debtOwner) {
                    this.searchRequest.debtOwner = params.debtOwner;
                    this.searchForm.patchValue({
                        debtOwner: getLibFromLibsById(this.searchRequest.debtOwner, this.debtOwnerOptions)
                    });
                }
                if (params.refusesToPay) {
                    this.searchRequest.refusesToPay = params.refusesToPay;
                    this.searchForm.patchValue({
                        refusesToPay: getLibFromLibsById(this.searchRequest.refusesToPay, this.refusesToPayOptions)
                    });
                }
                if (params.notInContact) {
                    this.searchRequest.notInContact = params.notInContact;
                    this.searchForm.patchValue({
                        notInContact: getLibFromLibsById(this.searchRequest.notInContact, this.notInContactOptions)
                    });
                }

                if (params.states) {
                    this.searchRequest.states = params.states;
                }

                if (this.removed) {
                    const tmpStates = [];
                    tmpStates.push(2);
                    tmpStates.push(3);
                    this.searchRequest.states = tmpStates;
                }

                if (params.fullyPaid === 'true') {
                    this.fullyPaid = true;
                }
                this.searchRequest.fullyPaid = this.fullyPaid;

                if (params.debtToConfirmPaid === 'true') {
                    this.debtToConfirmPaid = true;
                }
                this.searchRequest.debtToConfirmPaid = this.debtToConfirmPaid;

                if (params.assigneeDebtRequestTypeId) {
                    this.searchRequest.assigneeDebtRequestTypeId = params.assigneeDebtRequestTypeId;
                    this.searchForm.patchValue({
                        assigneeDebtRequestTypeId: getLibFromLibsById(this.searchRequest.assigneeDebtRequestTypeId, this.assigneeDebtRequestOptions)
                    });
                }

                if (params.amountFrom) {
                    this.searchRequest.amountFrom = params.amountFrom;
                    this.searchForm.patchValue({
                        amountFrom: params.amountFrom
                    });
                }

                if (params.amountTo) {
                    this.searchRequest.amountTo = params.amountTo;
                    this.searchForm.patchValue({
                        amountTo: params.amountTo
                    });
                }

                if (params.debtDateFrom) {
                    this.searchRequest.debtDateFrom = params.debtDateFrom;
                    this.searchForm.patchValue({
                        debtDateFrom: new Date(params.debtDateFrom)
                    });
                }
                if (params.debtDateTo) {
                    this.searchRequest.debtDateTo = params.debtDateTo;
                    this.searchForm.patchValue({
                        debtDateTo: new Date(params.debtDateTo)
                    });
                }

                if (params.commentDateFrom) {
                    this.searchRequest.commentDateFrom = params.commentDateFrom;
                    this.searchForm.patchValue({
                        commentDateFrom: new Date(params.commentDateFrom)
                    });
                }
                if (params.commentDateTo) {
                    this.searchRequest.commentDateTo = params.commentDateTo;
                    this.searchForm.patchValue({
                        commentDateTo: new Date(params.commentDateTo)
                    });
                }

                if (params.promiseDateFrom) {
                    this.searchRequest.promiseDateFrom = params.promiseDateFrom;
                    this.searchForm.patchValue({
                        promiseDateFrom: new Date(params.promiseDateFrom)
                    });
                }
                if (params.promiseDateTo) {
                    this.searchRequest.promiseDateTo = params.promiseDateTo;
                    this.searchForm.patchValue({
                        promiseDateTo: new Date(params.promiseDateTo)
                    });
                }

                if (params.nextContactDateFrom) {
                    this.searchRequest.nextContactDateFrom = params.nextContactDateFrom;
                    this.searchForm.patchValue({
                        nextContactDateFrom: new Date(params.nextContactDateFrom)
                    });
                }
                if (params.nextContactDateTo) {
                    this.searchRequest.nextContactDateTo = params.nextContactDateTo;
                    this.searchForm.patchValue({
                        nextContactDateTo: new Date(params.nextContactDateTo)
                    });
                }

                if (params.assignDateFrom) {
                    this.searchRequest.assignDateFrom = params.assignDateFrom;
                    this.searchForm.patchValue({
                        assignDateFrom: new Date(params.assignDateFrom)
                    });
                }
                if (params.assignDateTo) {
                    this.searchRequest.assignDateTo = params.assignDateTo;
                    this.searchForm.patchValue({
                        assignDateTo: new Date(params.assignDateTo)
                    });
                }

                if (params.statuteOfLimitationDateFrom) {
                    this.searchRequest.statuteOfLimitationDateFrom = params.statuteOfLimitationDateFrom;
                    this.searchForm.patchValue({
                        statuteOfLimitationDateFrom: new Date(params.statuteOfLimitationDateFrom)
                    });
                }
                if (params.statuteOfLimitationDateTo) {
                    this.searchRequest.statuteOfLimitationDateTo = params.statuteOfLimitationDateTo;
                    this.searchForm.patchValue({
                        statuteOfLimitationDateTo: new Date(params.statuteOfLimitationDateTo)
                    });
                }
                if (params.debtId) {
                    this.searchRequest.debtId = params.debtId;
                    this.searchForm.patchValue({
                        debtId: params.debtId
                    });
                }

                if (params.firstResult) {
                    this.searchRequest.firstResult = params.firstResult;
                }
                if (params.limit) {
                    this.searchRequest.limit = params.limit;
                }
                this.globalService.onBlockUI(false);
                this.search(null, this.searchRequest);
            });
        }
    }

    search(event: LazyLoadEvent, req) {
        this.globalService.onBlockUI(true);
        const formValues = this.searchForm.getRawValue();
        this.now = new Date();
        this.selectedDebts = [];
        if (event == null) {
            this.table._first = this.searchRequest.firstResult;
            if (req == null) {
                this.table._first = 0;
                this.searchRequest.firstResult = 0;

                this.searchRequest.borrowerPersonId = formValues.borrowerPersonId ? formValues.borrowerPersonId : '';
                this.searchRequest.borrowerPerson = formValues.borrowerPerson ? formValues.borrowerPerson : '';
                this.searchRequest.loanPersonId = formValues.loanPersonId ? formValues.loanPersonId : '';
                this.searchRequest.loanPerson = formValues.loanPerson ? formValues.loanPerson : '';
                this.searchRequest.userId = formValues.userId ? formValues.userId : '';
                this.searchRequest.user = formValues.user ? formValues.user : '';
                this.searchRequest.debtComplexityTypeId = formValues.debtComplexityType ? formValues.debtComplexityType.id : '';
                this.searchRequest.lawyerStatusId = formValues.lawyerStatus ? formValues.lawyerStatus.id : '';
                this.searchRequest.scheduleStatusId = formValues.scheduleStatus ? formValues.scheduleStatus.id : '';
                this.searchRequest.unWorkedDebt = formValues.unWorkedDebt ? formValues.unWorkedDebt.id : '';
                this.searchRequest.debtOwner = formValues.debtOwner ? formValues.debtOwner.id : '';
                this.searchRequest.refusesToPay = formValues.refusesToPay ? formValues.refusesToPay.id : '';
                this.searchRequest.notInContact = formValues.notInContact ? formValues.notInContact.id : '';
                /*if (formValues.states && formValues.states.length > 0) {
                    this.searchRequest.states = [];
                    formValues.states.forEach(item => {
                        this.searchRequest.states.push(item.id);
                    });
                }*/
                this.searchRequest.fullyPaid = this.fullyPaid;
                this.searchRequest.debtToConfirmPaid = this.debtToConfirmPaid;
                this.searchRequest.assigneeDebtRequestTypeId = formValues.assigneeDebtRequestTypeId ? formValues.assigneeDebtRequestTypeId.id : '';
                this.searchRequest.amountFrom = formValues.amountFrom ? formValues.amountFrom : '';
                this.searchRequest.amountTo = formValues.amountTo ? formValues.amountTo : '';
                this.searchRequest.debtId = formValues.debtId ? formValues.debtId : '';

                this.searchRequest.debtDateFrom = resetTimeToDateToISOString(formValues.debtDateFrom);
                this.searchRequest.debtDateTo = resetTimeToDateToISOString(formValues.debtDateTo);

                this.searchRequest.commentDateFrom = resetTimeToDateToISOString(formValues.commentDateFrom);
                this.searchRequest.commentDateTo = resetTimeToDateToISOString(formValues.commentDateTo);

                this.searchRequest.promiseDateFrom = resetTimeToDateToISOString(formValues.promiseDateFrom);
                this.searchRequest.promiseDateTo = resetTimeToDateToISOString(formValues.promiseDateTo);

                this.searchRequest.nextContactDateFrom = resetTimeToDateToISOString(formValues.nextContactDateFrom);
                this.searchRequest.nextContactDateTo = resetTimeToDateToISOString(formValues.nextContactDateTo);

                this.searchRequest.assignDateFrom = resetTimeToDateToISOString(formValues.assignDateFrom);
                this.searchRequest.assignDateTo = resetTimeToDateToISOString(formValues.assignDateTo);

                this.searchRequest.statuteOfLimitationDateFrom = resetTimeToDateToISOString(formValues.statuteOfLimitationDateFrom);
                this.searchRequest.statuteOfLimitationDateTo = resetTimeToDateToISOString(formValues.statuteOfLimitationDateTo);
            }
        } else {
            this.searchRequest.firstResult = event.first;
        }

        const params = {
            firstResult: this.searchRequest.firstResult,
            limit: this.searchRequest.limit,
            borrowerPersonId: this.searchRequest.borrowerPersonId,
            borrowerPerson: this.searchRequest.borrowerPerson,
            loanPersonId: this.searchRequest.loanPersonId,
            loanPerson: this.searchRequest.loanPerson,
            debtComplexityTypeId: this.searchRequest.debtComplexityTypeId,
            lawyerStatusId: this.searchRequest.lawyerStatusId,
            scheduleStatusId: this.searchRequest.scheduleStatusId,
            unWorkedDebt: this.searchRequest.unWorkedDebt,
            debtOwner: this.searchRequest.debtOwner,
            refusesToPay: this.searchRequest.refusesToPay,
            notInContact: this.searchRequest.notInContact,
            states: this.searchRequest.states,
            fullyPaid: this.searchRequest.fullyPaid,
            debtToConfirmPaid: this.searchRequest.debtToConfirmPaid,
            assigneeDebtRequestTypeId: this.searchRequest.assigneeDebtRequestTypeId,
            amountFrom: this.searchRequest.amountFrom,
            amountTo: this.searchRequest.amountTo,
            debtDateFrom: this.searchRequest.debtDateFrom,
            debtDateTo: this.searchRequest.debtDateTo,
            commentDateFrom: this.searchRequest.commentDateFrom,
            commentDateTo: this.searchRequest.commentDateTo,
            promiseDateFrom: this.searchRequest.promiseDateFrom,
            promiseDateTo: this.searchRequest.promiseDateTo,
            nextContactDateFrom: this.searchRequest.nextContactDateFrom,
            nextContactDateTo: this.searchRequest.nextContactDateTo,
            assignDateFrom: this.searchRequest.assignDateFrom,
            assignDateTo: this.searchRequest.assignDateTo,
            statuteOfLimitationDateFrom: this.searchRequest.statuteOfLimitationDateFrom,
            statuteOfLimitationDateTo: this.searchRequest.statuteOfLimitationDateTo,
            userId: this.searchRequest.userId,
            user: this.searchRequest.user,
            debtId: this.searchRequest.debtId,
        };
        this.globalService.createUrl(['/main/debts'], params);

        this.apiService.searchDebts(this.searchRequest).subscribe(response => {
            this.debts = response;
            this.globalService.onBlockUI(false);
        }, error => {
            this.debts.count = 0;
            this.debts.debts = [];
            this.globalService.onBlockUI(false);
            this.globalService.showError(error.error.message);
        });
    }

    addNewDebt() {
        this.router.navigate(['/main/add-debt']);
    }

    onRowSelect(event) {
        this.selectedDebts.push(event.data);
    }

    onRowUnselect(event) {
        const index = this.selectedDebts.indexOf(event.data, 0);
        if (index > -1) {
            this.selectedDebts.splice(index, 1);
        }
    }

    assigneeDebt(event) {
        const debtIds = [];
        this.selectedDebts.forEach(debt => {
            debtIds.push(debt.id);
        });

        const request = {
            debtId: debtIds,
            userId: event.userId
        };
        this.globalService.onBlockUI(true);
        this.apiService.assigneeDebtToUser(request).subscribe(response => {
            this.displaySearchUser = false;
            this.globalService.onBlockUI(false);
            this.search(null, this.searchRequest);
        }, error => {
            this.globalService.onBlockUI(false);
            this.globalService.showError(error.error.message);
        });
    }

    openSearchUserModal() {
        if (this.selectedDebts.length < 1) {
            this.globalService.showSuccess('აირჩიეთ ვალ(ებ)ი');
            return;
        }
        this.displaySearchUser = true;
    }

    openDebt(debt) {
        this.router.navigate(['/main/debt/' + debt.id]);
    }

    openPerson(personId) {
        this.router.navigate(['/main/person/' + personId]);
    }

    openOrganization(organizationId) {
        this.router.navigate(['/main/organization/' + organizationId]);
    }

    openSearchUserForSearch() {
        this.displaySearchUserForSearch = true;
    }

    selectUserForSearch(event) {
        this.displaySearchUserForSearch = false;
        this.searchForm.patchValue({
            user: event.firstName + ' ' + event.lastName,
            userId: event.userId
        });
    }

    hasRight(right) {
        return hasRight(this.user, right);
    }

    resetSearchForm() {
        this.searchForm.reset();
        // this.fullyPaid = false;
        if (hasRight(this.user, 'ROLE_მხოლოდ საკუთარი ვალების ნახვა')) {
            this.searchForm.patchValue({
                userId: this.user.userId,
                user: this.user.firstName + ' ' + this.user.lastName
            });
            this.searchRequest.userId = this.user.userId;
        }
    }

    stringToDate(lastNextContactDate) {
        return new Date(lastNextContactDate);
    }

    openImportExcelModal() {
        this.displayExcelImport = true;
    }

    uploadFile(event: any, fileUploadButton: FileUpload): void {
        const formValues = this.importExcelForm.getRawValue();
        if (!formValues.importExcelOrganizationId) {
            this.globalService.showError('აირჩიეთ ორგანიზაცია');
            return;
        }
        const file = event.files[0];

        const formData: FormData = new FormData();
        formData.append('file', file, file.name);
        formData.append('organizationId', formValues.importExcelOrganizationId);

        this.apiService.importDebtsFromExcel(formData).subscribe(response => {
            fileUploadButton.clear();
            fileUploadButton.disabled = true;
            if (!response.valid) {
                this.globalService.showError(response.description);
            }
        }, error => {
            fileUploadButton.clear();
            fileUploadButton.disabled = false;
            this.globalService.showError('ფაილის ატვირთვის დროს მოხდა შეცდომა');
        });
    }

    openSearchPersonModal(searchPersonType, field) {
        this.lastSearchPersonField = field;
        this.appSearchPersonsForm.setPersonType(searchPersonType);
        this.displaySearchModal = true;
    }

    selectPerson(obj) {
        if (this.lastSearchPersonField === 'importExcelOrganizationId') {
            this.importExcelForm.patchValue({
                importExcelOrganizationId: obj.personType === 1 ? obj.person.personId : obj.person.organizationId,
                importExcelOrganizationName: obj.personType === 1 ? obj.person.firstName + ' '
                    + obj.person.lastName : obj.person.organizationName
            });
        } else if (this.lastSearchPersonField === 'search-borrowerPersonId') {
            this.searchForm.patchValue({
                borrowerPersonId: obj.personType === 1 ? obj.person.personId : obj.person.organizationId,
                borrowerPerson: obj.personType === 1 ? obj.person.firstName + ' '
                    + obj.person.lastName : obj.person.organizationName
            });
        } else if (this.lastSearchPersonField === 'search-loanPersonId') {
            this.searchForm.patchValue({
                loanPersonId: obj.personType === 1 ? obj.person.personId : obj.person.organizationId,
                loanPerson: obj.personType === 1 ? obj.person.firstName + ' '
                    + obj.person.lastName : obj.person.organizationName
            });
        }

        this.displaySearchModal = false;
    }

    private assigneeDebtToAdministration() {
        this.confirmationService.confirm({
            message: 'დარწმუნებული ხართ რომ გსურთ მონიშნული ვალების ადმინისტრაციაზე გადაწერა?',
            header: 'დადასტურება',
            icon: 'pi pi-info-circle',
            acceptLabel: 'კი',
            rejectLabel: 'არა',
            accept: () => {
                const debtIds = [];
                this.selectedDebts.forEach(debt => {
                    debtIds.push(debt.id);
                });

                const request = {
                    debtIds,
                };
                this.globalService.onBlockUI(true);
                this.apiService.assigneeDebtToAdministration(request).subscribe(response => {
                    this.displaySearchUser = false;
                    this.globalService.onBlockUI(false);
                    this.search(null, this.searchRequest);
                }, error => {
                    this.globalService.onBlockUI(false);
                    this.globalService.showError(error.error.message);
                });
            }
        });
    }

    changeDebtStateModal(debt) {
        this.changeStateValue = null;
        this.curDebt = debt;
        this.displayChangeStateModal = true;
    }

    changeDebtState() {
        console.log(this.curDebt);
        console.log(this.changeStateValue);
        this.blockedPanelChangeState = true;
        this.apiService.removeDebt({
            debtId: this.curDebt.id,
            state: this.changeStateValue
        }).subscribe(response => {
            this.blockedPanelChangeState = false;
            if (response.valid) {
                this.displayChangeStateModal = false;
                this.search(null, this.searchRequest);
            } else {
                this.globalService.showError(response.description);
            }
        }, error => {
            this.blockedPanelChangeState = false;
            this.globalService.showError(error.error.message);
        });
    }

    getLibFromLibs(libId, libs) {
        return getLibFromLibsById(libId, libs);
    }

    getTrClass(debt) {
        if (debt.statuteOfLimitationDate
            && this.stringToDate(debt.statuteOfLimitationDate).getTime() > this.now.getTime()
            && this.stringToDate(debt.statuteOfLimitationDate).getTime() <= (this.now.getTime() + 1000 * 60 * 60 * 24 * 180)) {
            return {'row-warning': true};
        }
        if (debt.lastNextContactDate && this.stringToDate(debt.lastNextContactDate).getTime() <= this.now.getTime()) {
            return {'row-danger': true};
        }
        return {};
    }
}
