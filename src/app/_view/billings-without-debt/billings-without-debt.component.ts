import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Table} from 'primeng/table';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {LibDto} from '../../_model/LibDto';
import {ApiService} from '../../_services/api.service';
import {GlobalService} from '../../_services/global.service';
import {ActivatedRoute, Router} from '@angular/router';
import {getLibFromLibsById} from '../../_helper/utils';
import {ConfirmationService, LazyLoadEvent} from 'primeng/api';
import {resetTimeToDateToISOString, setDefaultTimeToDate} from '../../_helper/dates/date-util';
import {FileUpload} from 'primeng/fileupload';
import {DebtBillingDto} from '../../_model/DebtBillingDto';
import {hasRight} from '../../_helper/user-util';
import {UserDTO} from '../../_model/UserDTO';
import {decode} from "../../_helper/json-util";
import {AuthenticationService} from "../../_services/authentication.service";

@Component({
    selector: 'app-billings-without-debt',
    templateUrl: './billings-without-debt.component.html',
    styleUrls: ['./billings-without-debt.component.scss']
})
export class BillingsWithoutDebtComponent implements OnInit, AfterViewInit {
    @ViewChild('table', {static: false}) table: Table;
    user: UserDTO;
    billings: any = {
        content: [],
        totalElements: 0,
        totalPages: 0,
        size: 0,
    };
    searchRequest: any = {
        page: 0,
        size: 10,
        paymentStartDate: '',
        paymentEndDate: '',
        insertStartDate: '',
        insertEndDate: '',
        payByCheck: '',
        amount: '',
        userId: '',
        attachedToDebt: 0,
    };
    searchForm: FormGroup;

    payByCheckOptions: LibDto[] = [
        {
            id: 1,
            name: 'ჩეკით'
        },
        {
            id: 0,
            name: 'ჩეკის გარეშე'
        }];

    displayModal = false;
    blockedPanel = false;
    addForm: FormGroup;
    now = new Date();
    currentBilling: DebtBillingDto;
    uploadedFiles: any[] = [];
    @ViewChild('fileUploadButton', {static: false}) fileUploadButton: FileUpload;

    displayDebtSearchModal = false;

    constructor(private apiService: ApiService,
                private globalService: GlobalService,
                private fb: FormBuilder,
                private router: Router,
                private activatedRoute: ActivatedRoute,
                private confirmationService: ConfirmationService,
                private authenticationService: AuthenticationService) {
    }

    ngOnInit(): void {
        this.user = decode(localStorage.getItem(this.authenticationService.LOCAL_STORAGE_KEY));

        this.addForm = this.fb.group({
            paymentDate: new FormControl('', [Validators.required]),
            amount: new FormControl('', [Validators.required]),
            comment: new FormControl(),
            payByCheck: new FormControl(),
        });

        this.searchForm = this.fb.group({
            paymentStartDate: new FormControl(),
            paymentEndDate: new FormControl(),
            payByCheck: new FormControl(),
        });

        this.activatedRoute.queryParams.subscribe(params => {
            if (params.paymentStartDate) {
                this.searchRequest.paymentStartDate = params.paymentStartDate;
                this.searchForm.patchValue({
                    paymentStartDate: new Date(params.paymentStartDate)
                });
            }
            if (params.paymentEndDate) {
                this.searchRequest.paymentEndDate = params.paymentEndDate;
                this.searchForm.patchValue({
                    paymentEndDate: new Date(params.paymentEndDate)
                });
            }

            if (params.payByCheck) {
                this.searchRequest.payByCheck = params.payByCheck;
                this.searchForm.patchValue({
                    payByCheck: getLibFromLibsById(this.searchRequest.payByCheck, this.payByCheckOptions)
                });
            }

            if (params.firstResult) {
                this.searchRequest.firstResult = params.firstResult;
            }
            if (params.limit) {
                this.searchRequest.limit = params.limit;
            }
        });
    }

    ngAfterViewInit() {
        this.search(null, this.searchRequest);
    }

    search(event: LazyLoadEvent, req) {
        const formValues = this.searchForm.getRawValue();
        if (event == null) {
            // this.table._first = this.searchRequest.firstResult;
            if (req == null) {
                this.table._first = 0;
                this.searchRequest.page = 0;

                this.searchRequest.payByCheck = formValues.payByCheck ? formValues.payByCheck.id : '';

                this.searchRequest.paymentStartDate = resetTimeToDateToISOString(formValues.paymentStartDate);
                this.searchRequest.paymentEndDate = resetTimeToDateToISOString(formValues.paymentEndDate);
            }
        } else {
            this.searchRequest.page = (event.first / this.searchRequest.size);
        }

        const params = {
            page: this.searchRequest.page,
            limit: this.searchRequest.limit,
            paymentStartDate: this.searchRequest.paymentStartDate,
            paymentEndDate: this.searchRequest.paymentEndDate,
            payByCheck: this.searchRequest.payByCheck,
        };
        this.globalService.createUrl(['/main/billings-without-debt'], params);

        this.globalService.onBlockUI(true);
        this.apiService.searchBillingWithoutDebt(this.searchRequest).subscribe(response => {
            this.billings = response;
            this.globalService.onBlockUI(false);
        }, error => {
            this.globalService.onBlockUI(false);
            this.globalService.showError(error.error.message);
        });
    }

    downloadFile(file: any) {
        window.open('/clo-java/rest/api/file/files/' + file.id);
    }

    addNewBilling(billing) {
        this.addForm.reset();
        this.currentBilling = null;
        this.uploadedFiles = [];
        if (billing) {
            this.addForm.patchValue({
                paymentDate: new Date(billing.paymentDate),
                amount: billing.amount,
                comment: billing.comment,
                payByCheck: billing.payByCheck
            });
            if (billing.file) {
                this.uploadedFiles.push(billing.file);
            }
            this.currentBilling = billing;
        }
        this.displayModal = true;
    }

    removeBillingWithoutDebt(billing: any) {
        this.confirmationService.confirm({
            message: 'დარწმუნებული ხართ რომ გსურთ წაშლა?',
            header: 'დადასტურება',
            icon: 'pi pi-info-circle',
            acceptLabel: 'კი',
            rejectLabel: 'არა',
            accept: () => {
                this.globalService.onBlockUI(true);
                this.apiService.removeBillingWithoutDebt(billing.id).subscribe(response => {
                    this.globalService.onBlockUI(false);
                    this.search(null, this.searchRequest);
                }, error => {
                    this.globalService.onBlockUI(false);
                    this.globalService.showError('დაფიქსირდა შეცდომა');
                });
            }
        });
    }

    uploadFile(event: any, fileUploadButton: FileUpload): void {
        if (this.uploadedFiles.length > 0) {
            return;
        }
        const file = event.files[0];

        const formData: FormData = new FormData();
        formData.append('file', file, file.name);
        formData.append('fileTypeId', '2');

        this.apiService.fileUpload(formData).subscribe(response => {
            if (!response.valid) {
                this.globalService.showError(response.description);
            } else {
                this.uploadedFiles.push(response.added);
                fileUploadButton.clear();
                fileUploadButton.disabled = true;
            }
        }, error => {
            fileUploadButton.clear();
            fileUploadButton.disabled = false;
            this.globalService.showError('ფაილის ატვირთვის დროს მოხდა შეცდომა');
        });
    }

    removeDebtBillingFile(file: any) {
        this.uploadedFiles.forEach((item, index) => {
            if (item.id === file.id) {
                this.uploadedFiles.splice(index, 1);
            }
        });
    }

    addDebtBilling() {
        const valid = this.checkValidity(this.addForm);
        if (!valid) {
            this.globalService.showError('შეავსეთ აუცილებელი ველები');
            return;
        }

        this.blockedPanel = true;
        const formValues = this.addForm.getRawValue();

        const request = {
            id: this.currentBilling ? this.currentBilling.id : null,
            amount: formValues.amount,
            comment: formValues.comment,
            payByCheck: formValues.payByCheck ? 1 : 0,
            paymentDate: formValues.paymentDate ? setDefaultTimeToDate(formValues.paymentDate) : null,
            fileId: this.uploadedFiles.length > 0 ? this.uploadedFiles[0].id : null,
        };
        if (request.id) {
            this.apiService.updateBillingWithoutDebt(request).subscribe(response => {
                this.blockedPanel = false;
                if (response.valid) {
                    this.displayModal = false;
                    this.search(null, this.searchRequest);
                } else {
                    this.globalService.showError(response.description);
                }

            }, error => {
                this.blockedPanel = false;
                this.globalService.showError(error.error.message);
            });
        } else {
            this.apiService.addBillingWithoutDebt(request).subscribe(response => {
                this.blockedPanel = false;
                if (response.valid) {
                    this.displayModal = false;
                    this.search(null, this.searchRequest);
                } else {
                    this.globalService.showError(response.description);
                }

            }, error => {
                this.blockedPanel = false;
                this.globalService.showError(error.error.message);
            });
        }
    }

    onModalClose() {
        this.addForm.reset();
        this.uploadedFiles = [];
        this.fileUploadButton.clear();
        this.fileUploadButton.disabled = false;
    }

    public checkValidity(form): boolean {
        let valid = true;
        Object.keys(form.controls).forEach((key) => {
            form.controls[key].markAsDirty();
            if (valid) {
                valid = form.controls[key].valid;
            }
        });
        return valid;
    }

    openDebtsSearchModal(billing: any) {
        this.currentBilling = billing;
        this.displayDebtSearchModal = true;
    }

    selectDebt(debt: any) {
        this.confirmationService.confirm({
            message: 'დარწმუნებული ხართ რომ გსურთ ' + debt.id + ' ვალზე მიბმა?',
            header: 'დადასტურება',
            icon: 'pi pi-info-circle',
            acceptLabel: 'კი',
            rejectLabel: 'არა',
            accept: () => {
                this.globalService.onBlockUI(true);
                this.apiService.attacheBillingToDebt(this.currentBilling.id, debt.id).subscribe(response => {
                    this.globalService.onBlockUI(false);
                    this.currentBilling = null;
                    this.displayDebtSearchModal = false;
                    this.search(null, this.searchRequest);
                }, error => {
                    this.globalService.onBlockUI(false);
                    this.globalService.showError('დაფიქსირდა შეცდომა');
                });
            }
        });
    }

    hasRight(right) {
        return hasRight(this.user, right);
    }
}
