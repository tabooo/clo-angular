import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Table} from 'primeng/table';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {ApiService} from '../../_services/api.service';
import {GlobalService} from '../../_services/global.service';
import {ActivatedRoute, Router} from '@angular/router';
import {LazyLoadEvent} from 'primeng/api';
import {SearchPersonsFormComponent} from '../search-persons-form/search-persons-form.component';
import {resetTimeToDateToISOString} from '../../_helper/dates/date-util';
import {LibDto} from '../../_model/LibDto';
import {getLibFromLibsById} from '../../_helper/utils';

@Component({
    selector: 'app-billings-search',
    templateUrl: './billings-search.component.html',
    styleUrls: ['./billings-search.component.scss']
})
export class BillingsSearchComponent implements OnInit, AfterViewInit {
    @ViewChild('table', {static: false}) table: Table;
    billings: any = {count: 0, amount: 0, billings: []};
    searchRequest: any = {
        firstResult: 0,
        limit: 10,
        borrowerPersonId: '',
        loanPersonId: '',
        expertId: '',
        debtBillingPaymentDateFrom: '',
        debtBillingPaymentDateTo: '',
        debtBillingInsertDateFrom: '',
        debtBillingInsertDateTo: '',
        payByCheck: '',
    };
    searchForm: FormGroup;

    displaySearchUserForSearch: boolean;
    lastSearchPersonField: any;
    displaySearchModal = false;
    @ViewChild('SearchPersonsFormComponent', {static: false}) appSearchPersonsForm: SearchPersonsFormComponent;
    payByCheckOptions: LibDto[] = [
        {
            id: 1,
            name: 'ჩეკით'
        },
        {
            id: 0,
            name: 'ჩეკის გარეშე'
        }];

    constructor(private apiService: ApiService,
                private globalService: GlobalService,
                private fb: FormBuilder,
                private router: Router,
                private activatedRoute: ActivatedRoute) {
    }

    ngOnInit(): void {
        this.searchForm = this.fb.group({
            borrowerPerson: new FormControl(),
            borrowerPersonId: new FormControl(),
            loanPerson: new FormControl(),
            loanPersonId: new FormControl(),
            expertId: new FormControl(),
            expert: new FormControl(),
            debtBillingPaymentDateFrom: new FormControl(),
            debtBillingPaymentDateTo: new FormControl(),
            payByCheck: new FormControl(),
        });

        this.activatedRoute.queryParams.subscribe(params => {
            if (params.borrowerPersonId) {
                this.searchRequest.borrowerPersonId = params.borrowerPersonId;
            }
            if (params.borrowerPerson) {
                this.searchRequest.borrowerPerson = params.borrowerPerson;
                this.searchForm.patchValue({
                    borrowerPerson: params.borrowerPerson
                });
            }
            if (params.loanPersonId) {
                this.searchRequest.loanPersonId = params.loanPersonId;
            }
            if (params.loanPerson) {
                this.searchRequest.loanPerson = params.loanPerson;
                this.searchForm.patchValue({
                    loanPerson: params.loanPerson
                });
            }
            if (params.expertId) {
                this.searchRequest.expertId = params.expertId;
            }
            if (params.expert) {
                this.searchRequest.expert = params.expert;
                this.searchForm.patchValue({
                    expert: params.expert
                });
            }

            if (params.debtBillingPaymentDateFrom) {
                this.searchRequest.debtBillingPaymentDateFrom = params.debtBillingPaymentDateFrom;
                this.searchForm.patchValue({
                    debtBillingPaymentDateFrom: new Date(params.debtBillingPaymentDateFrom)
                });
            }
            if (params.debtBillingPaymentDateTo) {
                this.searchRequest.debtBillingPaymentDateTo = params.debtBillingPaymentDateTo;
                this.searchForm.patchValue({
                    debtBillingPaymentDateTo: new Date(params.debtBillingPaymentDateTo)
                });
            }

            if (params.payByCheck) {
                this.searchRequest.payByCheck = params.payByCheck;
                this.searchForm.patchValue({
                    payByCheck: getLibFromLibsById(this.searchRequest.payByCheck, this.payByCheckOptions)
                });
            }

            if (params.firstResult) {
                this.searchRequest.firstResult = params.firstResult;
            }
            if (params.limit) {
                this.searchRequest.limit = params.limit;
            }
        });
    }

    ngAfterViewInit() {
        this.search(null, this.searchRequest);
    }

    search(event: LazyLoadEvent, req) {
        const formValues = this.searchForm.getRawValue();
        if (event == null) {
            this.table._first = this.searchRequest.firstResult;
            if (req == null) {
                this.table._first = 0;
                this.searchRequest.firstResult = 0;

                this.searchRequest.borrowerPersonId = formValues.borrowerPersonId ? formValues.borrowerPersonId : '';
                this.searchRequest.borrowerPerson = formValues.borrowerPerson ? formValues.borrowerPerson : '';
                this.searchRequest.loanPersonId = formValues.loanPersonId ? formValues.loanPersonId : '';
                this.searchRequest.loanPerson = formValues.loanPerson ? formValues.loanPerson : '';
                this.searchRequest.expertId = formValues.expertId ? formValues.expertId : '';
                this.searchRequest.expert = formValues.expert ? formValues.expert : '';
                this.searchRequest.payByCheck = formValues.payByCheck ? formValues.payByCheck.id : '';

                this.searchRequest.debtBillingPaymentDateFrom = resetTimeToDateToISOString(formValues.debtBillingPaymentDateFrom);
                this.searchRequest.debtBillingPaymentDateTo = resetTimeToDateToISOString(formValues.debtBillingPaymentDateTo);
            }
        } else {
            this.searchRequest.firstResult = event.first;
        }

        const params = {
            firstResult: this.searchRequest.firstResult,
            limit: this.searchRequest.limit,
            borrowerPersonId: this.searchRequest.borrowerPersonId,
            borrowerPerson: this.searchRequest.borrowerPerson,
            loanPersonId: this.searchRequest.loanPersonId,
            loanPerson: this.searchRequest.loanPerson,
            expertId: this.searchRequest.expertId,
            expert: this.searchRequest.expert,
            debtBillingPaymentDateFrom: this.searchRequest.debtBillingPaymentDateFrom,
            debtBillingPaymentDateTo: this.searchRequest.debtBillingPaymentDateTo,
            payByCheck: this.searchRequest.payByCheck,
        };
        this.globalService.createUrl(['/main/billings'], params);

        this.globalService.onBlockUI(true);
        this.apiService.searchDebtBillings(this.searchRequest).subscribe(response => {
            this.billings = response;
            this.globalService.onBlockUI(false);
        }, error => {
            this.globalService.onBlockUI(false);
            this.globalService.showError(error.error.message);
        });
    }

    openDebt(billing: any) {
        this.router.navigate(['/main/debt/' + billing.debtId]);
    }

    downloadFile(file: any) {
        window.open('/clo-java/rest/api/file/files/' + file.id);
    }

    openSearchPersonModal(searchPersonType, field) {
        this.lastSearchPersonField = field;
        this.appSearchPersonsForm.setPersonType(searchPersonType);
        this.displaySearchModal = true;
    }

    selectPerson(obj) {
        if (this.lastSearchPersonField === 'search-borrowerPersonId') {
            this.searchForm.patchValue({
                borrowerPersonId: obj.personType === 1 ? obj.person.personId : obj.person.organizationId,
                borrowerPerson: obj.personType === 1 ? obj.person.firstName + ' '
                    + obj.person.lastName : obj.person.organizationName
            });
        } else if (this.lastSearchPersonField === 'search-loanPersonId') {
            this.searchForm.patchValue({
                loanPersonId: obj.personType === 1 ? obj.person.personId : obj.person.organizationId,
                loanPerson: obj.personType === 1 ? obj.person.firstName + ' '
                    + obj.person.lastName : obj.person.organizationName
            });
        }

        this.displaySearchModal = false;
    }

    openPerson(personId) {
        this.router.navigate(['/main/person/' + personId]);
    }

    openOrganization(organizationId) {
        this.router.navigate(['/main/organization/' + organizationId]);
    }

    openSearchUserForSearch() {
        this.displaySearchUserForSearch = true;
    }

    selectUserForSearch(event) {
        this.displaySearchUserForSearch = false;
        this.searchForm.patchValue({
            expert: event.firstName + ' ' + event.lastName,
            expertId: event.userId
        });
    }

}
