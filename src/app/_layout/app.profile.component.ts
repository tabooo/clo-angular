import {Component} from '@angular/core';
import {AppMainComponent} from '../app.main.component';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {AppComponent} from '../app.component';
import {AuthenticationService} from '../_services/authentication.service';
import {decode} from '../_helper/json-util';
import {UserDTO} from '../_model/UserDTO';
import {Router} from '@angular/router';

@Component({
    selector: 'app-inline-profile',
    template: `
        <div class="user-profile">
            <a href="#" (click)="onProfileClick($event)" id="sidebar-profile-button">
                <img src="assets/layout/images/icon-profile.png" alt="" style="margin-left: 80px;"/>
                <span class="sidebar-profile-name">{{user?.userName}}</span>
                <br>
                <span class="sidebar-profile-name">ბონუსი: {{countsObject?.userBonusAmount?.toFixed(2)}}</span>
                <span
                    class="sidebar-profile-role"><b>{{(user && user.roles) ? user.roles[0].displayName : ''}}</b></span>
            </a>

            <ul id="sidebar-usermenu" class="usermenu" [ngClass]="{'usermenu-active':app.usermenuActive}"
                [@menu]="app.isSlim()? app.usermenuActive ? 'visible' : 'hidden' :
                app.usermenuActive ? 'visibleAnimated' : 'hiddenAnimated'">
                <li #profile [ngClass]="{'menuitem-active':app.activeProfileItem === profile}">
                    <a href="#" (click)="onProfileItemClick($event,profile)">
                        <i class="pi pi-fw pi-user"></i>
                        <span class="topbar-item-name">პროფილი</span>
                    </a>
                </li>
                <!--<li #settings [ngClass]="{'menuitem-active':app.activeProfileItem === settings}">
                    <a href="#" (click)="onProfileItemClick($event,settings)">
                        <i class="pi pi-fw pi-cog"></i>
                        <span class="topbar-item-name">ენა</span>
                        <i class="layout-menuitem-toggler pi pi-fw pi-angle-down"></i>
                    </a>
                    <ul>
                        <li role="menuitem">
                            <a href="#" (click)="switchLanguage('ka')">
                                <i class="pi pi-fw pi-palette"></i>
                                <span>ქართული</span>
                            </a>
                        </li>
                        <li role="menuitem">
                            <a href="#" (click)="switchLanguage('en')">
                                <i class="pi pi-fw pi-star"></i>
                                <span>ინგლისური</span>
                            </a>
                        </li>
                        <li role="menuitem">
                            <a href="#" (click)="switchLanguage('ru')">
                                <i class="pi pi-fw pi-lock"></i>
                                <span>რუსული</span>
                            </a>
                        </li>
                    </ul>
                </li>-->
                <li #profile [ngClass]="{'menuitem-active':app.activeProfileItem === profile}">
                    <a href="#" (click)="logout()">
                        <i class="pi pi-fw pi-sign-out"></i>
                        <span class="topbar-item-name">{{ 'LOGOUT' | translate }}</span>
                    </a>
                </li>
            </ul>
        </div>
    `,
    animations: [
        trigger('menu', [
            state('hiddenAnimated', style({
                height: '0px'
            })),
            state('visibleAnimated', style({
                height: '*'
            })),
            state('visible', style({
                height: '*'
            })),
            state('hidden', style({
                height: '0px'
            })),
            transition('visibleAnimated => hiddenAnimated', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)')),
            transition('hiddenAnimated => visibleAnimated', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
        ])
    ]
})
export class AppProfileComponent {
    user: UserDTO;
    countsObject: any;

    constructor(public app: AppMainComponent,
                private appComponent: AppComponent,
                private authenticationService: AuthenticationService,
                private router: Router) {
        this.user = decode(localStorage.getItem(this.authenticationService.LOCAL_STORAGE_KEY));
        authenticationService.user.subscribe(value => {
            this.user = value;
        });
        authenticationService.countsObject.subscribe(value => {
            this.countsObject = value;
        });
    }

    onProfileClick(event) {
        this.app.usermenuClick = true;
        this.app.usermenuActive = !this.app.usermenuActive;
        event.preventDefault();
    }

    logout() {
        this.authenticationService.logout();
    }

    onProfileItemClick(event, item) {
        this.app.usermenuClick = true;
        if (this.app.activeProfileItem === item) {
            this.app.activeProfileItem = null;
        } else {
            this.app.activeProfileItem = item;
        }

        this.router.navigate(['/main/profile']);
        event.preventDefault();
    }

    onProfileSubItemClick(event) {
        event.preventDefault();
    }

    switchLanguage(lang) {
        this.appComponent.switchLang(lang);
    }
}
