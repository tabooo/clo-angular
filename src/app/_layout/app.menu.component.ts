import {Component, OnDestroy, OnInit} from '@angular/core';
import {AppMainComponent} from '../app.main.component';
import {hasRight} from '../_helper/user-util';
import {AuthenticationService} from '../_services/authentication.service';
import {decode} from '../_helper/json-util';
import {UserDTO} from '../_model/UserDTO';

@Component({
    selector: 'app-menu',
    template: `
        <ul class="layout-menu">
            <li app-menuitem *ngFor="let item of model; let i = index;" [item]="item" [index]="i" [root]="true">
            </li>
        </ul>
    `
})
export class AppMenuComponent implements OnInit, OnDestroy {
    user: UserDTO;
    model: any[] = [];
    countsObject: any = {count: 0};
    countsObjectSubscription;

    constructor(public app: AppMainComponent,
                private authenticationService: AuthenticationService) {
        this.countsObjectSubscription = authenticationService.countsObject.subscribe(value => {
            this.countsObject = value;
            this.model = [];
            this.drawMenu();
        });
    }

    ngOnInit() {
        this.user = decode(localStorage.getItem(this.authenticationService.LOCAL_STORAGE_KEY));
        this.drawMenu();
    }

    ngOnDestroy() {
        this.countsObjectSubscription.unsubscribe();
    }

    drawMenu() {
        if (hasRight(this.user, 'ROLE_პიროვნებების ნახვა')) {
            this.model.push({label: 'პიროვნებები', icon: 'pi pi-fw pi-users', routerLink: ['/main/persons']});
        }

        if (hasRight(this.user, 'ROLE_ორგანიზაციების ნახვა')) {
            this.model.push({label: 'ორგანიზაციები', icon: 'pi pi-fw pi-sitemap', routerLink: ['/main/organizations']});
        }

        if (hasRight(this.user, 'ROLE_ვალის დამატება')) {
            this.model.push({label: 'ვალის შექმნა', icon: 'pi pi-fw pi-plus-circle', routerLink: ['/main/add-debt']});
        }

        if (hasRight(this.user, 'ROLE_ვალების ნახვა')) {
            this.model.push({label: 'ვალები', icon: 'pi pi-fw pi-money-bill', routerLink: ['/main/debts']});
        }

        if (hasRight(this.user, 'ROLE_ადმინისტრაციაზე გადასაწერი ვალები')) {
            this.model.push({
                label: 'ადმინისტრაციაზე გადასაწერი ვალები',
                icon: 'pi pi-fw pi-book',
                routerLink: ['/main/debts/assigneeDebtRequest/1'],
            });
        }

        if (hasRight(this.user, 'ROLE_ადმინისტრაციაზე გადასაწერი ვალები')) {
            this.model.push({
                label: 'იურისტზე გადასაწერი ვალები',
                icon: 'pi pi-fw pi-book',
                routerLink: ['/main/debts/assigneeDebtRequest/2'],
            });
        }

        if (hasRight(this.user, 'ROLE_დადასტურებული გრაფიკების ნახვა')) {
            this.model.push({
                label: 'დადასტურებული გრაფიკები',
                icon: 'pi pi-fw pi-book',
                routerLink: ['/main/debts/confirmedSchedules'],
                badgeValue: this.countsObject.confirmedScheduleCount
            });
        }

        if (hasRight(this.user, 'ROLE_უარყოფილი გრაფიკების ნახვა')) {
            this.model.push({
                label: 'უარყოფილი გრაფიკები',
                icon: 'pi pi-fw pi-book',
                routerLink: ['/main/debts/rejectSchedules'],
                badgeValue: this.countsObject.rejectScheduleCount
            });
        }

        if (hasRight(this.user, 'ROLE_გასანულებელი ვალები')) {
            this.model.push({
                label: 'გასანულებელი ვალები',
                icon: 'pi pi-fw pi-book',
                routerLink: ['/main/debts/debtToConfirmPaid'],
                badgeValue: this.countsObject.unconfirmedPaidDebtCount
            });
        }

        if (hasRight(this.user, 'ROLE_განულებული ვალები')) {
            this.model.push({
                label: 'არქივი',
                icon: 'pi pi-fw pi-book',
                routerLink: ['/main/debts/fullyPaid'],
            });
        }

        if (hasRight(this.user, 'ROLE_წაშლილი ვალების ნახვა')) {
            this.model.push({
                label: 'წაშლილი ვალები',
                icon: 'pi pi-fw pi-trash',
                routerLink: ['/main/debts/removed'],
            });
        }

        if (hasRight(this.user, 'ROLE_ბონუსების ნახვა')) {
            this.model.push({label: 'ბონუსები', icon: 'pi pi-fw pi-money-bill', routerLink: ['/main/bonuses']});
        }

        if (hasRight(this.user, 'ROLE_გადახდების ნახვა')) {
            this.model.push({label: 'გადახდები', icon: 'pi pi-fw pi-money-bill', routerLink: ['/main/billings']});
        }

        if (hasRight(this.user, 'ROLE_გადახდების ნახვა (ვალის გარეშე)')) {
            this.model.push({
                label: 'გაურკვეველი ჩარიცხვები',
                icon: 'pi pi-fw pi-money-bill',
                routerLink: ['/main/billings-without-debt']
            });
        }

        if (hasRight(this.user, 'ROLE_როლების ნახვა')) {
            this.model.push({label: 'როლები', icon: 'pi pi-fw pi-lock', routerLink: ['/main/roles']});
        }
        if (hasRight(this.user, 'ROLE_მომხმარებლების ნახვა')) {
            this.model.push({label: 'მომხმარებლები', icon: 'pi pi-fw pi-users', routerLink: ['/main/users']});
        }
    }
}
