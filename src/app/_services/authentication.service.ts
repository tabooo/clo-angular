import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {environment} from '../../environments/environment';
import {decode, encode} from '../_helper/json-util';
import {ApiService} from './api.service';
import {MessageService} from 'primeng/api';
import {UserDTO} from '../_model/UserDTO';
import {hasRole} from '../_helper/user-util';
import {resetTimeToDateToISOString} from '../_helper/dates/date-util';

@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {
    readonly LOCAL_STORAGE_KEY = 'userData';

    private userSubject: BehaviorSubject<UserDTO>;
    public user: Observable<UserDTO>;
    public countsObject = new Subject();

    constructor(
        private http: HttpClient,
        private router: Router,
        private apiService: ApiService,
        private messageService: MessageService) {

        this.userSubject = new BehaviorSubject<UserDTO>(decode(localStorage.getItem(this.LOCAL_STORAGE_KEY)));
        this.user = this.userSubject.asObservable();
    }

    public get userValue(): UserDTO {
        return this.userSubject.value;
    }

    // TODO [I.G]
    updateUserData(userObj: any) {
        const userData: UserDTO = decode(localStorage.getItem(this.LOCAL_STORAGE_KEY));

        if (!userData) {
            return;
        }

        const updated: UserDTO = {
            ...userData,
            ...userObj
        };

        localStorage.setItem(this.LOCAL_STORAGE_KEY, encode(updated));
        this.userSubject.next(updated);
    }

    sendPin(phoneNumber: string) {
        return this.http.get<void>(`${environment.apiUrl}/otp/send?phoneNumber=${phoneNumber}`);
    }

    login(username: string, password: string) {
        const request = {
            username,
            password,
        };
        this.apiService.login(username, password).subscribe(response => {
            localStorage.setItem(this.LOCAL_STORAGE_KEY, encode(response));
            this.userSubject.next(response);
            this.getCounts();
            setInterval(() => {
                this.getCounts();
            }, 60 * 1000);

            if (hasRole(response, 1)) {
                this.router.navigate(['main/debts']);
            } else {
                this.router.navigate(['main/dashboard']);
            }
        });
        /*this.http.post<UserDTO>(`${environment.apiUrl}/rest/api/auth/login?username=${username}&password=${password}`, {})
            .subscribe(response => {
                localStorage.setItem(this.LOCAL_STORAGE_KEY, encode(response));
                this.userSubject.next(response);
                this.router.navigate(['main']);
            });*/
    }

    logout() {
        this.userSubject.next(null);
        localStorage.removeItem(this.LOCAL_STORAGE_KEY);
        localStorage.clear();
        // this.router.navigate(['/login']);

        this.apiService.logout().subscribe(response => {
            window.location.href = window.location.origin;
            // this.router.navigate(['/login']);
        }, error => {
            // this.router.navigate(['/login']);
        });
    }

    logout2() {
        this.userSubject.next(null);
        localStorage.removeItem(this.LOCAL_STORAGE_KEY);
        localStorage.clear();
        this.router.navigate(['/login']);
    }

    async getUser() {
        const userData: UserDTO = decode(localStorage.getItem(this.LOCAL_STORAGE_KEY));
        if (userData == null) {
            this.logout2();
            return;
        }
        await this.apiService.getSessionData().subscribe(response => {
            if (response) {
                // this.getBonus(response);
                this.getCounts();
                setInterval(() => {
                    this.getCounts();
                }, 60 * 1000);
            }
        });
    }

    getBonus(user) {
        const date = new Date();
        const firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
        const lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 1);
        const request = {
            expertId: user.userId,
            startDate: resetTimeToDateToISOString(firstDay),
            endDate: resetTimeToDateToISOString(lastDay),
            debtId: ''
        };

        this.apiService.bonusSearch(request).subscribe(bonuses => {
            let wholeAmount = 0;
            if (bonuses) {
                bonuses.forEach(bonus => {
                    wholeAmount += bonus.amount;
                });
            }
            user.bonus = wholeAmount;
            this.updateUserData(user);
        });
    }

    public getCounts() {
        this.apiService.getNotifications().subscribe(response => {
            if (response) {
                this.countsObject.next(response);
            }
        });
    }
}
