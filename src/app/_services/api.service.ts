import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ApiService {

    constructor(private http: HttpClient) {
    }

    // AUTH

    login(username, password): Observable<any> {
        return this.http.post<any>(`/clo-java/rest/api/auth/login?username=${username}&password=${password}`, {});
    }

    logout(): Observable<any> {
        return this.http.post<any>(`/clo-java/rest/api/auth/logout`, {});
    }

    getSessionData(): Observable<any> {
        return this.http.get<any>(`/clo-java/rest/api/auth/session-data`, {});
    }


    // PERSON

    savePerson(request): Observable<any> {
        return this.http.post(`/clo-java/rest/api/person/save-person`, request);
    }

    updatePerson(request): Observable<any> {
        return this.http.post(`/clo-java/rest/api/person/update-person`, request);
    }

    updateOrganization(request): Observable<any> {
        return this.http.post(`/clo-java/rest/api/person/update-organization`, request);
    }

    searchPersons(request): Observable<any> {
        return this.http.get(`/clo-java/rest/api/person/search-persons`, {params: request});
    }

    getPerson(personId): Observable<any> {
        return this.http.get(`/clo-java/rest/api/person/get-person?personId=${personId}`);
    }

    getOrganization(organizationId): Observable<any> {
        return this.http.get(`/clo-java/rest/api/person/get-organization?organizationId=${organizationId}`);
    }

    getPersonContacts(personId): Observable<any> {
        return this.http.get(`/clo-java/rest/api/person/contacts?personId=${personId}`);
    }

    addPersonContact(request): Observable<any> {
        return this.http.post(`/clo-java/rest/api/person/add-contact`, request);
    }

    updatePersonContact(request): Observable<any> {
        return this.http.post(`/clo-java/rest/api/person/update-contact`, request);
    }

    removePersonContact(contactId): Observable<any> {
        return this.http.get(`/clo-java/rest/api/person/remove-contact?id=${contactId}`);
    }

    getPersonProperties(personId): Observable<any> {
        return this.http.get(`/clo-java/rest/api/person/properties?personId=${personId}`);
    }

    addPersonProperty(request): Observable<any> {
        return this.http.post(`/clo-java/rest/api/person/add-property`, request);
    }

    updatePersonProperty(request): Observable<any> {
        return this.http.post(`/clo-java/rest/api/person/update-property`, request);
    }

    removePersonProperty(contactId): Observable<any> {
        return this.http.get(`/clo-java/rest/api/person/remove-property?id=${contactId}`);
    }

    searchOrganizations(request): Observable<any> {
        return this.http.get(`/clo-java/rest/api/person/search-organizations`, {params: request});
    }

    getPersonBankDetails(personId): Observable<any> {
        return this.http.get(`/clo-java/rest/api/person/bank-details?personId=${personId}`);
    }

    savePersonBankDetail(request): Observable<any> {
        return this.http.post(`/clo-java/rest/api/person/add-bank-detail`, request);
    }

    removePersonBankDetail(personBankDetailId): Observable<any> {
        return this.http.post(`/clo-java/rest/api/person/remove-bank-detail?personBankDetailId=${personBankDetailId}`, {});
    }

    getPersonConnections(personId): Observable<any> {
        return this.http.get(`/clo-java/rest/api/person/connections?personId=${personId}`);
    }

    addPersonConnection(request): Observable<any> {
        return this.http.post(`/clo-java/rest/api/person/add-connection`, request);
    }

    removePersonConnection(connectionId): Observable<any> {
        return this.http.get(`/clo-java/rest/api/person/remove-connection?id=${connectionId}`);
    }

    // DEBT

    searchDebts(request): Observable<any> {
        return this.http.get(`/clo-java/rest/api/debt/search-debts`, {params: request});
    }

    addDebt(request): Observable<any> {
        return this.http.post(`/clo-java/rest/api/debt/add-debt`, request);
    }

    updateDebt(request): Observable<any> {
        return this.http.post(`/clo-java/rest/api/debt/update-debt`, request);
    }

    assigneeDebtToUser(request): Observable<any> {
        return this.http.post(`/clo-java/rest/api/debt/assignee-debt`, request);
    }

    assigneeDebtToAdministration(request): Observable<any> {
        return this.http.post(`/clo-java/rest/api/debt/assignee-debt-to-administration`, {}, {params: request});
    }

    getDebt(debtId): Observable<any> {
        return this.http.get(`/clo-java/rest/api/debt/get?id=${debtId}`);
    }

    getDebtComments(debtId): Observable<any> {
        return this.http.get(`/clo-java/rest/api/debt/get-debt-comments?debtId=${debtId}`);
    }

    saveDebtComment(request): Observable<any> {
        return this.http.post(`/clo-java/rest/api/debt/save-debt-comment`, request);
    }

    updateDebtComment(request): Observable<any> {
        return this.http.post(`/clo-java/rest/api/debt/update-debt-comment`, request);
    }

    removeDebtComment(commentId): Observable<any> {
        return this.http.get(`/clo-java/rest/api/debt/remove-debt-comment/${commentId}`);
    }

    getDebtRsLinks(debtId): Observable<any> {
        return this.http.get(`/clo-java/rest/api/debt/rs-links?debtId=${debtId}`);
    }

    removeDebtRsLink(id): Observable<any> {
        return this.http.get(`/clo-java/rest/api/debt/remove-rs-link?id=${id}`);
    }

    saveDebtRsLink(request): Observable<any> {
        return this.http.post(`/clo-java/rest/api/debt/add-rs-link`, request);
    }

    addDebtSchedule(request): Observable<any> {
        return this.http.post(`/clo-java/rest/api/debt/add-debt-schedule`, request);
    }

    generateDebtScheduleForPreview(request): Observable<any> {
        return this.http.post(`/clo-java/rest/api/debt/generate-debt-schedule-for-preview`, request);
    }

    getActiveDebtSchedule(debtId): Observable<any> {
        return this.http.get(`/clo-java/rest/api/debt/get-active-debt-schedule?debt-id=${debtId}`);
    }

    changeDebtScheduleStatus(params): Observable<any> {
        return this.http.put(`/clo-java/rest/api/debt/change-debt-schedule-status?schedule-id=${params.scheduleId}&status-id=${params.statusId}&comment=${params.comment}`, {});
    }

    getDebtBillings(debtId): Observable<any> {
        return this.http.get(`/clo-java/rest/api/debt/get-debt-billings/${debtId}`);
    }

    checkIfNeedPaidDebtExecutionAmount(debtId): Observable<any> {
        return this.http.get(`/clo-java/rest/api/debt/check-if-need-paid-debt-court-fee-amount/${debtId}`);
    }

    addDebtBilling(request): Observable<any> {
        if (request.id) {
            return this.http.put(`/clo-java/rest/api/debt/update-debt-billing`, request);
        } else {
            return this.http.post(`/clo-java/rest/api/debt/add-debt-billing`, request);
        }
    }

    importDebtsFromExcel(request): Observable<any> {
        return this.http.post(`/clo-java/rest/api/debt/importDebtsFromExcel`, request
        );
    }

    removeDebtBilling(billingId): Observable<any> {
        return this.http.get(`/clo-java/rest/api/debt/remove-debt-billing/${billingId}`);
    }

    getDebtSchedules(debtId): Observable<any> {
        return this.http.get(`/clo-java/rest/api/debt/get-debt-schedules?debt-id=${debtId}`);
    }

    getDebtChanges(debtId): Observable<any> {
        return this.http.get(`/clo-java/rest/api/debt/get-debt-changes?debtId=${debtId}`);
    }

    discussDebtToConfirmResult(debtId, debtToConfirmPaidId, confirmedStatus): Observable<any> {
        return this.http.get(`/clo-java/rest/api/debt/discuss-debt-to-confirm-result/${debtId}/${debtToConfirmPaidId}/${confirmedStatus}`);
    }

    discussDebtToConfirmFullyPaid(debtId): Observable<any> {
        return this.http.get(`/clo-java/rest/api/debt/discuss-debt-to-confirm-fully-paid/${debtId}`);
    }

    requestAssigneeDebt(request): Observable<any> {
        return this.http.post(`/clo-java/rest/api/debt/request-assignee-debt`, request);
    }

    makeAssigneeDecision(request): Observable<any> {
        return this.http.post(`/clo-java/rest/api/debt/make-assignee-decision`, request);
    }

    makeAssigneeDecisionForLawyer(request): Observable<any> {
        return this.http.post(`/clo-java/rest/api/debt/make-assignee-decision-for-lawyer`, request);
    }

    addDebtFile(request): Observable<any> {
        return this.http.post(`/clo-java/rest/api/debt/add-file`, request);
    }

    getDebtFiles(debtId): Observable<any> {
        return this.http.get(`/clo-java/rest/api/debt/get-files?debtId=${debtId}`);
    }

    getDebtFilesByTypeId(debtId, fileTypeId): Observable<any> {
        return this.http.get(`/clo-java/rest/api/debt/get-files-by-type?debtId=${debtId}&fileTypeId=${fileTypeId}`);
    }

    removeDebtFile(fileId): Observable<any> {
        return this.http.get(`/clo-java/rest/api/debt/remove-file?fileId=${fileId}`);
    }

    refusesToPay(params): Observable<any> {
        return this.http.get(`/clo-java/rest/api/debt/refuses_to_pay`, {params});
    }

    contact(params): Observable<any> {
        return this.http.get(`/clo-java/rest/api/debt/contact`, {params});
    }

    isProcessed(params): Observable<any> {
        return this.http.get(`/clo-java/rest/api/debt/is_processed`, {params});
    }

    canSeeExpert(params): Observable<any> {
        return this.http.get(`/clo-java/rest/api/debt/can-see-expert`, {params});
    }

    searchDebtBillings(request): Observable<any> {
        return this.http.get(`/clo-java/rest/api/debt/search-debt-billings`, {params: request});
    }

    removeDebt(request): Observable<any> {
        return this.http.get(`/clo-java/rest/api/debt/remove-debt`, {params: request});
    }

    searchBillingWithoutDebt(request): Observable<any> {
        return this.http.get(`/clo-java/rest/api/debt/search-billing-without-debt`, {params: request});
    }

    removeBillingWithoutDebt(billingId): Observable<any> {
        return this.http.get(`/clo-java/rest/api/debt/remove-billing-without-debt/${billingId}`);
    }

    addBillingWithoutDebt(request): Observable<any> {
        return this.http.post(`/clo-java/rest/api/debt/add-billing-without-debt`, request);
    }

    updateBillingWithoutDebt(request): Observable<any> {
        return this.http.put(`/clo-java/rest/api/debt/update-billing-without-debt`, request);
    }

    attacheBillingToDebt(billingWithoutDebtId, debtId): Observable<any> {
        return this.http.post(`/clo-java/rest/api/debt/attache-billing-to-debt/${billingWithoutDebtId}/${debtId}`, {});
    }

    getLawyerSchedules(debtId): Observable<any> {
        return this.http.get(`/clo-java/rest/api/debt/lawyer-schedules?debtId=${debtId}`);
    }

    saveLawyerSchedule(request): Observable<any> {
        if (request.id == null) {
            return this.http.post(`/clo-java/rest/api/debt/add-lawyer-schedule`, request);
        } else {
            return this.http.post(`/clo-java/rest/api/debt/update-lawyer-schedule`, request);
        }
    }

    addDebtLawyerStatus(request): Observable<any> {
        return this.http.post(`/clo-java/rest/api/debt/add-debt-lawyer-status?debtId=${request.debtId}&debtLawyerStatusId=${request.statusId}`, {});
    }

    // USER/MANAGEMENT

    getRoleUsers(request): Observable<any> {
        return this.http.get(`/clo-java/rest/api/user/management/get-role-users/${request.roleId}`);
    }

    getRoles(): Observable<any> {
        return this.http.get(`/clo-java/rest/api/user/management/get-roles`);
    }

    getRolePrivileges(roleId): Observable<any> {
        return this.http.get(`/clo-java/rest/api/user/management/get-role-privileges/${roleId}`);
    }

    addPrivilegeToRole(roleId, privleges): Observable<any> {
        return this.http.post(`/clo-java/rest/api/user/management/add-privilege-to-role/${roleId}?privilege-ids`, privleges);
    }

    removePrivilegeFromRole(roleId, privleges): Observable<any> {
        return this.http.post(`/clo-java/rest/api/user/management/remove-privilege-from-role/${roleId}?privilege-ids`,
            privleges);
    }

    getPrivileges(): Observable<any> {
        return this.http.get(`/clo-java/rest/api/user/management/get-privileges`);
    }

    addRolesToUser(userId, roles): Observable<any> {
        return this.http.post(`/clo-java/rest/api/user/management/add-roles-to-user/${userId}?role-ids`,
            roles);
    }

    removeRolesFromUser(userId, roles): Observable<any> {
        return this.http.post(`/clo-java/rest/api/user/management/remove-roles-from-user/${userId}?role-ids`,
            roles);
    }

    // USER

    searchUsers(request): Observable<any> {
        return this.http.get(`/clo-java/rest/api/user`, {params: request});
    }

    addUser(request): Observable<any> {
        if (request.userId) {
            return this.http.put(`/clo-java/rest/api/user/update-user`, request);
        } else {
            return this.http.post(`/clo-java/rest/api/user/add-user`, request);
        }
    }

    addUserWithRole(request): Observable<any> {
        if (request.userId) {
            return this.http.put(`/clo-java/rest/api/user/update-user-with-roles`, request);
        } else {
            return this.http.post(`/clo-java/rest/api/user/add-user-with-roles`, request);
        }
    }

    getUserById(userId): Observable<any> {
        return this.http.get(`/clo-java/rest/api/user/${userId}`);
    }

    removeUser(userId): Observable<any> {
        return this.http.get(`/clo-java/rest/api/user/remove-user/${userId}`);
    }

    resetPassword(request): Observable<any> {
        return this.http.put(`/clo-java/rest/api/user/reset-password`, request);
    }

    changePassword(request): Observable<any> {
        return this.http.put(`/clo-java/rest/api/user/change-password`, request);
    }

    // LIB

    getDebtComplexityTypes(): Observable<any> {
        return this.http.get(`/clo-java/rest/api/lib/debt-complexity-types`);
    }

    getDebtPropertyTypes(): Observable<any> {
        return this.http.get(`/clo-java/rest/api/lib/property-types`);
    }

    getDebtComplexityTypeByDate(debtDate): Observable<any> {
        return this.http.get(`/clo-java/rest/api/lib/debt-complexity-type-by-date`, {params: {debtDate}});
    }

    getDebtScheduleTypes(): Observable<any> {
        return this.http.get(`/clo-java/rest/api/lib/debt-schedule-types`);
    }

    getDebtScheduleStatuses(): Observable<any> {
        return this.http.get(`/clo-java/rest/api/lib/debt-schedule-status`);
    }

    getDebtBillingTypes(): Observable<any> {
        return this.http.get(`/clo-java/rest/api/lib/debt-billing-types`);
    }

    getContactStatuses(): Observable<any> {
        return this.http.get(`/clo-java/rest/api/lib/contact-statuses`);
    }

    getPersonConnectionTypes(): Observable<any> {
        return this.http.get(`/clo-java/rest/api/lib/person-connection-types`);
    }

    getDebtLawyerStatus(): Observable<any> {
        return this.http.get(`/clo-java/rest/api/lib/debt-lawyer-status`);
    }

    // FILE

    fileUpload(request): Observable<any> {
        return this.http.post(`/clo-java/rest/api/file/upload`, request);
    }

    // BONUS

    bonusSearch(request): Observable<any> {
        return this.http.get(`/clo-java/rest/api/bonus`, {params: request});
    }

    // NOTIFICATION

    getNotifications(): Observable<any> {
        return this.http.get(`/clo-java/rest/api/notification`);
    }
}
