import {RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {AppMainComponent} from './app.main.component';
import {AppNotfoundComponent} from './pages/app.notfound.component';
import {AppAccessdeniedComponent} from './pages/app.accessdenied.component';
import {AppLoginComponent} from './pages/app.login.component';
import {AuthGuardService} from './_services/auth-guard.service';
import {LoginGuardService} from './_services/login-guard.service';
import {DashboardComponent} from './_view/dashboard/dashboard.component';
import {OrganizationsComponent} from './_view/organizations/organizations.component';
import {PersonsComponent} from './_view/persons/persons.component';
import {DebtsComponent} from './_view/debts/debts.component';
import {AddDebtComponent} from './_view/add-debt/add-debt.component';
import {PersonDetailComponent} from './_view/person-detail/person-detail.component';
import {OrganizationDetailComponent} from './_view/organization-detail/organization-detail.component';
import {RolesComponent} from './_view/user-management/roles/roles.component';
import {UsersComponent} from './_view/user-management/users/users.component';
import {DebtDetailComponent} from './_view/debt-detail/debt-detail.component';
import {ProfileComponent} from './_view/profile/profile.component';
import {BonusesComponent} from './_view/bonuses/bonuses.component';
import {BillingsSearchComponent} from './_view/billings-search/billings-search.component';
import {BillingsWithoutDebtComponent} from './_view/billings-without-debt/billings-without-debt.component';

@NgModule({
    imports: [
        RouterModule.forRoot([
            {
                path: '',
                redirectTo: '/main',
                pathMatch: 'full'
            },
            {
                path: 'main',
                component: AppMainComponent,
                canActivate: [AuthGuardService],
                children: [
                    {
                        path: 'dashboard',
                        component: DashboardComponent
                    },
                    {
                        path: 'organizations',
                        component: OrganizationsComponent
                    },
                    {
                        path: 'organization/:organizationId',
                        component: OrganizationDetailComponent
                    },
                    {
                        path: 'persons',
                        component: PersonsComponent
                    },
                    {
                        path: 'person/:personId',
                        component: PersonDetailComponent
                    },
                    {
                        path: 'debts',
                        component: DebtsComponent,
                    },
                    {
                        path: 'debts/fullyPaid',
                        component: DebtsComponent,
                    },
                    {
                        path: 'debts/removed',
                        component: DebtsComponent,
                    },
                    {
                        path: 'debts/debtToConfirmPaid',
                        component: DebtsComponent,
                    },
                    {
                        path: 'debts/confirmedSchedules',
                        component: DebtsComponent,
                    },
                    {
                        path: 'debts/rejectSchedules',
                        component: DebtsComponent,
                    },
                    {
                        path: 'debts/assigneeDebtRequest/:assigneeDebtRequestTypeId',
                        component: DebtsComponent,
                    },
                    {
                        path: 'debt/:debtId',
                        component: DebtDetailComponent
                    },
                    {
                        path: 'add-debt',
                        component: AddDebtComponent
                    },
                    {
                        path: 'roles',
                        component: RolesComponent
                    },
                    {
                        path: 'users',
                        component: UsersComponent
                    },
                    {
                        path: 'profile',
                        component: ProfileComponent
                    },
                    {
                        path: 'bonuses',
                        component: BonusesComponent
                    },
                    {
                        path: 'billings',
                        component: BillingsSearchComponent
                    },
                    {
                        path: 'billings-without-debt',
                        component: BillingsWithoutDebtComponent
                    }]
            },
            {path: 'access', component: AppAccessdeniedComponent},
            {path: 'notfound', component: AppNotfoundComponent},
            {path: 'login', component: AppLoginComponent, canActivate: [LoginGuardService]},
            {path: '**', redirectTo: '/notfound'},
        ], {scrollPositionRestoration: 'enabled'})
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
