import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HashLocationStrategy, LocationStrategy, registerLocaleData} from '@angular/common';
import {AppRoutingModule} from './app-routing.module';

import {AccordionModule} from 'primeng/accordion';
import {AutoCompleteModule} from 'primeng/autocomplete';
import {AvatarModule} from 'primeng/avatar';
import {AvatarGroupModule} from 'primeng/avatargroup';
import {BadgeModule} from 'primeng/badge';
import {BreadcrumbModule} from 'primeng/breadcrumb';
import {ButtonModule} from 'primeng/button';
import {CalendarModule} from 'primeng/calendar';
import {CardModule} from 'primeng/card';
import {CarouselModule} from 'primeng/carousel';
import {CascadeSelectModule} from 'primeng/cascadeselect';
import {ChartModule} from 'primeng/chart';
import {CheckboxModule} from 'primeng/checkbox';
import {ChipModule} from 'primeng/chip';
import {ChipsModule} from 'primeng/chips';
import {CodeHighlighterModule} from 'primeng/codehighlighter';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ConfirmPopupModule} from 'primeng/confirmpopup';
import {ColorPickerModule} from 'primeng/colorpicker';
import {ContextMenuModule} from 'primeng/contextmenu';
import {DataViewModule} from 'primeng/dataview';
import {DialogModule} from 'primeng/dialog';
import {DividerModule} from 'primeng/divider';
import {DropdownModule} from 'primeng/dropdown';
import {FieldsetModule} from 'primeng/fieldset';
import {FileUploadModule} from 'primeng/fileupload';
import {FullCalendarModule} from 'primeng/fullcalendar';
import {GalleriaModule} from 'primeng/galleria';
import {InplaceModule} from 'primeng/inplace';
import {InputNumberModule} from 'primeng/inputnumber';
import {InputMaskModule} from 'primeng/inputmask';
import {InputSwitchModule} from 'primeng/inputswitch';
import {InputTextModule} from 'primeng/inputtext';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {KnobModule} from 'primeng/knob';
import {LightboxModule} from 'primeng/lightbox';
import {ListboxModule} from 'primeng/listbox';
import {MegaMenuModule} from 'primeng/megamenu';
import {MenuModule} from 'primeng/menu';
import {MenubarModule} from 'primeng/menubar';
import {MessagesModule} from 'primeng/messages';
import {MessageModule} from 'primeng/message';
import {MultiSelectModule} from 'primeng/multiselect';
import {OrderListModule} from 'primeng/orderlist';
import {OrganizationChartModule} from 'primeng/organizationchart';
import {OverlayPanelModule} from 'primeng/overlaypanel';
import {PaginatorModule} from 'primeng/paginator';
import {PanelModule} from 'primeng/panel';
import {PanelMenuModule} from 'primeng/panelmenu';
import {PasswordModule} from 'primeng/password';
import {PickListModule} from 'primeng/picklist';
import {ProgressBarModule} from 'primeng/progressbar';
import {RadioButtonModule} from 'primeng/radiobutton';
import {RatingModule} from 'primeng/rating';
import {RippleModule} from 'primeng/ripple';
import {ScrollPanelModule} from 'primeng/scrollpanel';
import {ScrollTopModule} from 'primeng/scrolltop';
import {SelectButtonModule} from 'primeng/selectbutton';
import {SidebarModule} from 'primeng/sidebar';
import {SkeletonModule} from 'primeng/skeleton';
import {SlideMenuModule} from 'primeng/slidemenu';
import {SliderModule} from 'primeng/slider';
import {SplitButtonModule} from 'primeng/splitbutton';
import {SplitterModule} from 'primeng/splitter';
import {StepsModule} from 'primeng/steps';
import {TabMenuModule} from 'primeng/tabmenu';
import {TableModule} from 'primeng/table';
import {TabViewModule} from 'primeng/tabview';
import {TagModule} from 'primeng/tag';
import {TerminalModule} from 'primeng/terminal';
import {TieredMenuModule} from 'primeng/tieredmenu';
import {TimelineModule} from 'primeng/timeline';
import {ToastModule} from 'primeng/toast';
import {ToggleButtonModule} from 'primeng/togglebutton';
import {ToolbarModule} from 'primeng/toolbar';
import {TooltipModule} from 'primeng/tooltip';
import {TreeModule} from 'primeng/tree';
import {TreeTableModule} from 'primeng/treetable';
import {VirtualScrollerModule} from 'primeng/virtualscroller';

// Application Components
import {AppCodeModule} from './app.code.component';
import {AppComponent} from './app.component';
import {AppMainComponent} from './app.main.component';
import {AppConfigComponent} from './app.config.component';
import {AppMenuComponent} from './_layout/app.menu.component';
import {AppMenuitemComponent} from './_layout/app.menuitem.component';
import {AppBreadcrumbComponent} from './_layout/app.breadcrumb.component';
import {AppMegamenuComponent} from './_layout/app.megamenu.component';
import {AppProfileComponent} from './_layout/app.profile.component';
import {AppRightPanelComponent} from './_layout/app.rightpanel.component';
import {AppTopBarComponent} from './_layout/app.topbar.component';
import {AppFooterComponent} from './_layout/app.footer.component';

import {AppNotfoundComponent} from './pages/app.notfound.component';
import {AppAccessdeniedComponent} from './pages/app.accessdenied.component';
import {AppLoginComponent} from './pages/app.login.component';

// Application services
import {BreadcrumbService} from './_services/app.breadcrumb.service';
import {MenuService} from './_services/app.menu.service';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {NgxLocalStorageModule} from 'ngx-localstorage';
import {FormatTimePipe} from './_helper/FormatTimePipe';
import {ConfirmationService, MessageService} from 'primeng/api';
import {DashboardComponent} from './_view/dashboard/dashboard.component';
import {DateFormatPipe} from './_helper/dates/pipe/date-format.pipe';
import {LOCALE_CODE} from './_helper/utils';
import localeKa from '@angular/common/locales/ka';
import localeKaExtra from '@angular/common/locales/extra/ka';
import {HttpInterceptorService} from './_services/config/http-interceptor.service';
import {OrganizationsComponent} from './_view/organizations/organizations.component';
import {PersonsComponent} from './_view/persons/persons.component';
import {BlockUIModule} from 'primeng/blockui';
import {DateTimeFormatPipe} from './_helper/dates/pipe/date-time-format.pipe';
import {DebtsComponent} from './_view/debts/debts.component';
import {AddDebtComponent} from './_view/add-debt/add-debt.component';
import {SearchPersonsFormComponent} from './_view/search-persons-form/search-persons-form.component';
import {SearchUsersFormComponent} from './_view/search-users-form/search-users-form.component';
import {PersonDetailComponent} from './_view/person-detail/person-detail.component';
import {OrganizationDetailComponent} from './_view/organization-detail/organization-detail.component';
import {PersonContactsComponent} from './_view/_forms/person-contacts/person-contacts.component';
import {PersonPropertiesComponent} from './_view/_forms/person-properties/person-properties.component';
import {RolesComponent} from './_view/user-management/roles/roles.component';
import {UsersComponent} from './_view/user-management/users/users.component';
import {DebtDetailComponent} from './_view/debt-detail/debt-detail.component';
import {DebtCommentsComponent} from './_view/_forms/debt-comments/debt-comments.component';
import {DebtScheduleComponent} from './_view/_forms/debt-schedule/debt-schedule.component';
import {ProfileComponent} from './_view/profile/profile.component';
import {DebtBillingsComponent} from './_view/_forms/debt-billings/debt-billings.component';
import {DebtSchedulesComponent} from './_view/_forms/debt-schedules/debt-schedules.component';
import {BonusesComponent} from './_view/bonuses/bonuses.component';
import {DebtChangesComponent} from './_view/_forms/debt-changes/debt-changes.component';
import {DebtRsLinksComponent} from './_view/_forms/debt-rs-links/debt-rs-links.component';
import {DebtFilesComponent} from './_view/_forms/debt-files/debt-files.component';
import {PersonBankDetailsComponent} from './_view/_forms/person-bank-details/person-bank-details.component';
import {BillingsSearchComponent} from './_view/billings-search/billings-search.component';
import {PersonConnectionsComponent} from './_view/_forms/person-connections/person-connections.component';
import {DebtLawyerComponent} from './_view/_forms/debt-lawyer/debt-lawyer.component';
import {DebtLawyerFilesComponent} from './_view/_forms/debt-lawyer/debt-lawyer-files/debt-lawyer-files.component';
import {BillingsWithoutDebtComponent} from './_view/billings-without-debt/billings-without-debt.component';
import {DebtsSearchSimpleComponent} from './_view/debts-search-simple/debts-search-simple.component';
import { DebtLawyerScheduleComponent } from './_view/_forms/debt-lawyer/debt-lawyer-schedule/debt-lawyer-schedule.component';
import {EditorModule} from 'primeng/editor';
import { DebtLawyerStatusComponent } from './_view/_forms/debt-lawyer/debt-lawyer-status/debt-lawyer-status.component';

registerLocaleData(localeKa, LOCALE_CODE, localeKaExtra);

export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        AppRoutingModule,
        HttpClientModule,
        BrowserAnimationsModule,
        AccordionModule,
        AutoCompleteModule,
        AvatarGroupModule,
        AvatarModule,
        BadgeModule,
        BreadcrumbModule,
        ButtonModule,
        CalendarModule,
        CardModule,
        CarouselModule,
        CascadeSelectModule,
        ChartModule,
        CheckboxModule,
        ChipModule,
        ChipsModule,
        CodeHighlighterModule,
        ConfirmDialogModule,
        ConfirmPopupModule,
        ColorPickerModule,
        ContextMenuModule,
        DataViewModule,
        DialogModule,
        DividerModule,
        DropdownModule,
        FieldsetModule,
        FileUploadModule,
        FullCalendarModule,
        GalleriaModule,
        InplaceModule,
        InputNumberModule,
        InputMaskModule,
        InputSwitchModule,
        InputTextModule,
        InputTextareaModule,
        KnobModule,
        LightboxModule,
        ListboxModule,
        MegaMenuModule,
        MenuModule,
        MenubarModule,
        MessageModule,
        MessagesModule,
        MultiSelectModule,
        OrderListModule,
        OrganizationChartModule,
        OverlayPanelModule,
        PaginatorModule,
        PanelModule,
        PanelMenuModule,
        PasswordModule,
        PickListModule,
        ProgressBarModule,
        RadioButtonModule,
        RatingModule,
        RippleModule,
        ScrollPanelModule,
        ScrollTopModule,
        SelectButtonModule,
        SidebarModule,
        SkeletonModule,
        SlideMenuModule,
        SliderModule,
        SplitButtonModule,
        SplitterModule,
        StepsModule,
        TableModule,
        TabMenuModule,
        TabViewModule,
        TagModule,
        TerminalModule,
        TieredMenuModule,
        TimelineModule,
        ToastModule,
        ToggleButtonModule,
        ToolbarModule,
        TooltipModule,
        TreeModule,
        TreeTableModule,
        VirtualScrollerModule,
        AppCodeModule,
        NgxLocalStorageModule.forRoot(),
        TranslateModule.forRoot({
            defaultLanguage: 'ka',
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
        ReactiveFormsModule,
        BlockUIModule,
        EditorModule
    ],
    declarations: [
        FormatTimePipe,
        AppComponent,
        AppMainComponent,
        AppMenuComponent,
        AppMenuitemComponent,
        AppConfigComponent,
        AppTopBarComponent,
        AppFooterComponent,
        AppRightPanelComponent,
        AppMegamenuComponent,
        AppProfileComponent,
        AppBreadcrumbComponent,
        AppLoginComponent,
        AppNotfoundComponent,
        AppAccessdeniedComponent,
        DashboardComponent,
        DateFormatPipe,
        DateTimeFormatPipe,
        OrganizationsComponent,
        PersonsComponent,
        DebtsComponent,
        AddDebtComponent,
        SearchPersonsFormComponent,
        SearchUsersFormComponent,
        PersonDetailComponent,
        OrganizationDetailComponent,
        PersonContactsComponent,
        PersonPropertiesComponent,
        RolesComponent,
        UsersComponent,
        DebtDetailComponent,
        DebtCommentsComponent,
        DebtScheduleComponent,
        ProfileComponent,
        DebtBillingsComponent,
        DebtSchedulesComponent,
        BonusesComponent,
        DebtChangesComponent,
        DebtRsLinksComponent,
        DebtFilesComponent,
        PersonBankDetailsComponent,
        BillingsSearchComponent,
        PersonConnectionsComponent,
        DebtLawyerComponent,
        DebtLawyerFilesComponent,
        BillingsWithoutDebtComponent,
        DebtsSearchSimpleComponent,
        DebtLawyerScheduleComponent,
        DebtLawyerStatusComponent,
    ],
    providers: [
        {provide: LocationStrategy, useClass: HashLocationStrategy}, MenuService, BreadcrumbService,
        MessageService,
        ConfirmationService,
        DateFormatPipe,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: HttpInterceptorService,
            multi: true
        }
    ],
    bootstrap: [AppComponent],
})
export class AppModule {
}
