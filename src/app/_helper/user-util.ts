import {UserDTO} from '../_model/UserDTO';

export function hasRole(user: UserDTO, roleId) {
    let bool = false;
    if (user.roles && user.roles.length > 0) {
        user.roles.forEach(role => {
            if (role.id === roleId) {
                bool = true;
            }
        });
    }
    return bool;
}

export function hasRight(user: UserDTO, rightName) {
    let bool = false;
    if (user.authorities && user.authorities.length > 0) {
        user.authorities.forEach(right => {
            if (right === rightName) {
                bool = true;
            }
        });
    }
    return bool;
}
